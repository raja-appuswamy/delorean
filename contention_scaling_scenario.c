/*
 * contention_scaling_scenario.c
 *
 *  Created on: 8 Oct 2018
 *      Author: anadioti
 */

#if ENABLE_CPU_HOTPLUG

#include "headers.h"
#include "worker.h"

void *contention_monitoring(void *args) {
	double tps, tps_prev = -1;
	int shrink_count = 0;
	int expand_count = 0;
	int hotplug_socket = 1;
	int srv;
	int hotplug_srv_idx;
	int tps_log_size = 1000000;
	double *tps_log = (double *) calloc(tps_log_size, sizeof(double));
	int tps_cnt = 0;
	while (!hash_table->quitting) {
		tps = measure_current_throughput(hash_table);
		printf("%.9f %.9f\n", tps, tps_prev);
		tps_log[tps_cnt++] = tps;
		if (tps_prev != -1) {
			double tps_rate = tps / tps_prev;
			printf("rate = %.9f\n", tps_rate);
			if (tps_rate <= 0.7) {

				if (shrink_count > 1) {
					printf("Shrinking VM\n");
					expand_count = 0;
					shrink_count = 0;
					tps_prev = tps;
					// shrink the machine
					for (int i = 0; i < 2 * CPUS_PER_SOCKET; i++) {
						printf("Stopping srv %d from socket %d\n", i, hotplug_socket);
						stop_hotplugged_server_socket(hotplug_socket, hash_table);
					}
					while(nready != g_active_servers);
					usleep(5000000);
				} else {
					expand_count = 0;
					shrink_count++;
				}
			} else if (tps_rate >= 1.5) {
				if (expand_count > 2) {
					printf("Expanding VM\n");
					shrink_count = 0;
					expand_count = 0;
					tps_prev = tps;
					// expand the machine
					for (int i = 0; i < 2 * CPUS_PER_SOCKET; i++) {
						hotplug_srv_idx = hotplug_socket * 2 * CPUS_PER_SOCKET + i;
						hotplug_worker_to_socket(hotplug_srv_idx, hotplug_socket, hash_table, &srv);
						notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
						g_nservers_to_begin_txns = g_active_servers;
						printf("Active servers now are %d\n", g_active_servers);
						hash_table->partitions[hotplug_srv_idx].prev_sum_tps = 0;
						hash_table->partitions[hotplug_srv_idx].prev_time_tps = 0;
					}
					while(nready != g_active_servers);
					usleep(5000000);
				} else {
					shrink_count = 0;
					expand_count++;
				}
			} else {
				shrink_count = 0;
				expand_count = 0;
				tps_prev = tps;
			}
		} else {
			tps_prev = tps;
		}
		usleep(1000000);
	}

	printf("================= Throughput =================\n");
	for (int i = 0; i < tps_cnt; i++) {
		printf("%.9f\n", tps_log[i]);
	}
	printf("==============================================\n");

	return NULL;
}

void virt_contention_scaling_scenario(struct hash_table *hash_table) {
	printf("================= Contention Scaling =================\n");
	// we will use two sockets
	int sockets_count = 2;
	int srv;
	int hotplug_srv_idx = 0;
	int cur_active_srvs = g_active_servers;
	int hotplug_socket = 1;
	// hotplug and start all servers from both sockets
//	for (int i = 0; i < sockets_count; i++) {
//		for (int j = cur_active_srvs; j < 2 * CPUS_PER_SOCKET; j++) {
//			hotplug_srv_idx = i * 2 * CPUS_PER_SOCKET + j;
//			printf("Hotpluggin srv %d\n", hotplug_srv_idx);
//			hotplug_worker_to_socket(hotplug_srv_idx, i, hash_table, &srv);
//			notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
//			g_nservers_to_begin_txns = g_active_servers;
//			printf("Active servers now are %d\n", g_active_servers);
//			hash_table->partitions[hotplug_srv_idx].prev_sum_tps = 0;
//			hash_table->partitions[hotplug_srv_idx].prev_time_tps = 0;
//
//		}
//		cur_active_srvs = 0;
//	}

	while(nready != g_nservers);
	// now sleep for 5 seconds to stabilize performance
	usleep(5000000);
	// now start monitoring
//	pthread_t monitor;
//	pthread_create(&monitor, NULL, contention_monitoring, NULL);
	int tps_log_size = 1000000;
	double *tps_log = (double *) calloc(tps_log_size, sizeof(double));
	int tps_cnt = 0;
	double tps;
	for (int i = 0; i < 10; i++) {
		tps = measure_current_throughput(hash_table);
		printf("%.9f\n", tps);
		tps_log[tps_cnt++] = tps;
		usleep(1000000);
	}
	// now sleep for another 10 seconds to let monitor collect measurements
//	usleep(10000000);
	// now increase the contention
	printf("================= High contention =================\n");
#if YCSB_BENCHMARK_DYNAMIC
	for (int i = 0; i < g_active_servers; i++) {
		hash_table->partitions[i].rotate_theta = 1;
	}
#endif
	usleep(5000000);
	for (int i = 0; i < 5; i++) {
		tps = measure_current_throughput(hash_table);
		printf("%.9f\n", tps);
		tps_log[tps_cnt++] = tps;
		usleep(1000000);
	}
	for (int i = 0; i < 2 * CPUS_PER_SOCKET; i++) {
		printf("Stopping srv %d from socket %d\n", i, hotplug_socket);
		stop_hotplugged_server_socket(hotplug_socket, hash_table);
	}
	while(nready != g_active_servers);
	usleep(5000000);
	for (int i = 0; i < 10; i++) {
		tps = measure_current_throughput(hash_table);
		printf("%.9f\n", tps);
		tps_log[tps_cnt++] = tps;
		usleep(1000000);
	}
	// now sleep for 10 seconds
//	usleep(30000000);
	// now switch to low contention
	printf("================= Low contention =================\n");
#if YCSB_BENCHMARK_DYNAMIC
	for (int i = 0; i < g_active_servers; i++) {
		hash_table->partitions[i].rotate_theta = 0;
	}
#endif
	usleep(20000000);
	for (int i = 0; i < 2 * CPUS_PER_SOCKET; i++) {
		hotplug_srv_idx = hotplug_socket * 2 * CPUS_PER_SOCKET + i;
		hotplug_worker_to_socket(hotplug_srv_idx, hotplug_socket, hash_table, &srv);
		notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
		g_nservers_to_begin_txns = g_active_servers;
		printf("Active servers now are %d\n", g_active_servers);
		hash_table->partitions[hotplug_srv_idx].prev_sum_tps = 0;
		hash_table->partitions[hotplug_srv_idx].prev_time_tps = 0;
	}
	while(nready != g_active_servers);
	usleep(10000000);
	for (int i = 0; i < 10; i++) {
		tps = measure_current_throughput(hash_table);
		printf("%.9f\n", tps);
		tps_log[tps_cnt++] = tps;
		usleep(1000000);
	}
	// now sleep for 20 seconds
//	usleep(30000000);
	// done
	printf("================= Throughput =================\n");
	for (int i = 0; i < tps_cnt; i++) {
		printf("%.9f\n", tps_log[i]);
	}
	printf("==============================================\n");
	hash_table->quitting = 1;

//	pthread_join(monitor, NULL);
}

#endif
