/*
 * worker.h
 *
 *  Created on: Dec 1, 2017
 *      Author: anadioti
 */

#ifndef WORKER_H_
#define WORKER_H_

#define WORKER_ACTIONS_NUMBER 3

#define CPUS_PER_SOCKET 18
#define SOCKET_COUNT	4

#define WORKER_ACTION_LOAD_DATA 0
#define WORKER_ACTION_START_TXNS 1
#define WORKER_ACTION_STOP_TXNS 2

void init_worker_id_socket_map();
void start_worker(int id, struct hash_table *hash_table);
void start_worker_hotplug(int id, struct hash_table *hash_table);
void stop_worker(int id, struct hash_table *hash_table);
#if ENABLE_CPU_HOTPLUG
void notify_worker(int s, int id, int action, struct hash_table *hash_table);
void stop_hotplugged_server_socket(int socket_id, struct hash_table *hash_table);
void stop_all_hotplugged_servers(struct hash_table *hash_table);
int hotplug_worker_to_socket(int worker_id, int socket_id, struct hash_table *hash_table, int *started_srv);
double measure_current_throughput(struct hash_table *hash_table);
#else
void notify_worker(int id, int action, struct hash_table *hash_table);
#endif

#endif /* WORKER_H_ */

