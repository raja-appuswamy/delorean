/*
 * peak_traffic_scenario.c
 *
 *  Created on: 10 Oct 2018
 *      Author: anadioti
 */
#if ENABLE_CPU_HOTPLUG

#include "headers.h"
#include "hotplug/hotplug.h"
#include "worker.h"


void *peak_traffic_monitor(void *args) {
	volatile int *quit = (volatile int *) args;
	long tpt_measure_time = 10000000;	// 100ms
	double tps;
	int tps_log_size = 1000000;
	double *tps_log = (double *) calloc(tps_log_size, sizeof(double));
	int tps_cnt = 0;
	int reps = 100;

	while (!(*quit)) {
		usleep(tpt_measure_time);
		tps = measure_current_throughput(hash_table);
		printf("Throughput is %.9f\n", tps);
		tps_log[tps_cnt++] = tps;
	}

	printf("================= Throughput =================\n");
	for (int i = 0; i < tps_cnt; i++) {
		printf("%.9f\n", tps_log[i]);
	}
	printf("==============================================\n");

	return NULL;
}


void run_peak_traffic_scenario(struct hash_table *hash_table) {
	printf("Running peak traffic scenario\n");

//	memset(g_srv_wait, 0, g_nservers * sizeof(volatile int));

	long tpt_measure_time = 100000;	// 100ms
	double tps;
	int tps_log_size = 1000000;
	double *tps_log = (double *) calloc(tps_log_size, sizeof(double));
	int tps_cnt = 0;
	int reps = 10;
	int srv;
	if (g_rep_time != 0) {
		reps = g_rep_time;
	}
	volatile int quit = 0;
//	pthread_t monitor_thread;
//	pthread_create(&monitor_thread, NULL, peak_traffic_monitor, (void *) &quit);
	int socket_id = (g_master_socket + 1) % 2;
	for (int i = 0; i < reps; i++) {
//		usleep(5000000);
		for (int j = 0; j < 5; j ++) {
			int cnt = g_active_servers;
			if (i % 2 == 1) {
				printf("Freezing old servers\n");
				for (int s = 0; s < CPUS_PER_SOCKET; s++) {
					g_srv_wait[s] = 1;
//					hotplug_worker_to_socket(cnt, socket_id, hash_table, &srv);
//					notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
//					g_nservers_to_begin_txns = g_active_servers;
//					cnt++;
				}
			}



	//		for (int j = 0; j < CPUS_PER_SOCKET; j++) {
	////			printf("Hotplugging worker %d to socket %d\n", cnt, socket_id);
	//			hotplug_worker_to_socket(cnt, socket_id, hash_table, &srv);
	//			cnt++;
	//			notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
	//			g_nservers_to_begin_txns = g_active_servers;
	//		}
			usleep(5000000);
			printf("Freezing new servers\n");
			if (i % 2 == 1) {
				for (int s = 0; s < CPUS_PER_SOCKET; s++) {
		//			printf("Freezing new servers\n");
					g_srv_wait[s] = 0;
	//				printf("Hotremoving worker from socket %d\n", socket_id);
//					stop_hotplugged_server_socket(socket_id, hash_table);
		//			g_srv_wait[s + CPUS_PER_SOCKET] = 1;
				}
		//		for (int j = 0; j < CPUS_PER_SOCKET; j++) {
		//			printf("Hotremoving worker from socket %d\n", g_master_socket);
		//			stop_hotplugged_server_socket(socket_id, hash_table);
		//		}
			}
			usleep(5000000);
			tps = measure_current_throughput(hash_table);
			printf("Throughput is %.9f\n", tps);
			tps_log[tps_cnt++] = tps;

		}
	}

	printf("================= Throughput =================\n");
	for (int i = 0; i < tps_cnt; i++) {
		printf("%.9f\n", tps_log[i]);
	}
	printf("==============================================\n");


	quit = 1;
//	pthread_join(monitor_thread, NULL);
	hash_table->quitting = 1;
}

#endif
