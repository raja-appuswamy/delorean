/*
 * spot_scaling_scenario.c
 *
 *  Created on: 9 Oct 2018
 *      Author: anadioti
 */
#if ENABLE_CPU_HOTPLUG

#include "headers.h"
#include "worker.h"
#include "hotplug/hotplug.h"

#define VM_MASK 0x0FFFFFFFFFFFFFFF

#define PREAMBLE_MASK	0xF000000000000000
#define CONTENT_MASK 	0x0FFFFFFF00000000
#define VM_ID_MASK		0x00000000FFFFFFFF
#define NUM_CORES_MASK	0x0FFFFFFFFFFFFFFF

#define GET_PREAMBLE(msg)	((msg & PREAMBLE_MASK) >> 60)
#define GET_CONTENT(msg)	((msg & CONTENT_MASK) >> 32)
#define GET_VM_ID(msg)		(msg & VM_ID_MASK)


void shrink_machine(struct hash_table *hash_table, int target_thread_count, int socket_id) {
	printf("================= Shrinking VM =================\n");
//	int target_socket = g_master_socket;
	// first stop the servers
	int currently_active_servers = g_active_servers;
	int cnt = 0;
	while (cnt < target_thread_count) {
		stop_hotplugged_server_socket(socket_id, hash_table);
		cnt++;
	}
}

void expand_machine(struct hash_table *hash_table, int target_thread_count, int socket_id) {
	printf("================= Expanding VM =================\n");
//	int target_socket = (g_master_socket + 1) % 2;
	int currently_active_servers = g_active_servers;
	int cnt = currently_active_servers;
	int srv;
	while ((cnt < currently_active_servers + target_thread_count) && (cnt < g_nservers)) {
		hotplug_worker_to_socket(cnt, socket_id, hash_table, &srv);
//		printf("Expanding with worker %d\n", srv);
		g_nservers_to_begin_txns = g_active_servers;
		notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
//		printf("Waiting for nready (%d) == g_active_servers (%d)\n", nready, g_active_servers);
		while (nready != g_active_servers) ;
		hash_table->partitions[srv].prev_sum_tps = 0;
		hash_table->partitions[srv].prev_time_tps = 0;
		cnt++;
	}
}


void run_spot_scaling_scenario(struct hash_table *hash_table) {
	printf("Running spot instance shrinking scenario\n");
//	int target_socket = g_master_socket;
//	int available_socket_count = 2;
//	int srv;
//	for (int i = g_active_servers; i < g_nservers; i++) {
//		// switch among the sockets while adding srvs
//		printf("Hotplugging srv %d\n", i);
//		hotplug_worker_to_socket(i, target_socket, hash_table, &srv);
//		notify_worker(srv, srv, WORKER_ACTION_LOAD_DATA, hash_table);
//		hash_table->partitions[i].prev_sum_tps = 0;
//		hash_table->partitions[i].prev_time_tps = 0;
//		target_socket = (target_socket + 1) % available_socket_count;
//	}
//	for (int i = g_startup_servers; i < g_active_servers; i++) {
//		notify_worker(i, i, WORKER_ACTION_START_TXNS, hash_table);
//	}
//	g_nservers_to_begin_txns = g_active_servers;
	double tps;
	int tps_log_size = 1000000;
	double *tps_log = (double *) calloc(tps_log_size, sizeof(double));
	int tps_cnt = 0;
	int reps = 80;
	if (g_rep_time != 0) {
		reps = g_rep_time;
	}

	uint64_t action = 1;
	uint64_t vm_id = g_vm_id;
	uint64_t val = (action << 60) | (vm_id & VM_MASK);
	for (int i = 0; i < reps && !hash_table->quitting; i++) {
		usleep(1000000);
		tps = measure_current_throughput(hash_table);
		printf("Throughput is %.9f\n", tps);
		tps_log[tps_cnt++] = tps;
		uint64_t code = hotplug_check(val);
		uint64_t hv_action = GET_PREAMBLE(code);
		uint64_t socket = GET_CONTENT(code);
		uint64_t ncores = GET_VM_ID(code);
//		printf("HV instructed to take action %"PRIu64" with %"PRIu64" cores\n", hv_action, ncores);
		switch(hv_action) {
		case 1:
			// shrink
			shrink_machine(hash_table, ncores, socket);
			break;
		case 0:
			// expand
			if (ncores > 0) {
				expand_machine(hash_table, ncores, socket);
			}

			break;
		}
	}
	printf("Time is up; exiting\n");
	hash_table->quitting = 1;

	printf("================= Throughput =================\n");
	for (int i = 0; i < tps_cnt; i++) {
		printf("%.9f\n", tps_log[i]);
	}
	printf("==============================================\n");
	hash_table->quitting = 1;
}

#endif

