#pragma once

#if defined(__MAIN__)
#define EXTERN
#else
#define EXTERN extern
#endif

EXTERN int g_nservers;
EXTERN int g_nrecs;
EXTERN struct benchmark *g_benchmark;
EXTERN struct hash_table *hash_table;
EXTERN double g_alpha;
EXTERN int g_niters;
EXTERN int g_nhot_servers;
EXTERN int g_nhot_recs;
EXTERN int g_ops_per_txn;
EXTERN int g_nremote_ops;
EXTERN int g_dist_threshold;
EXTERN int g_write_threshold;
EXTERN double g_write_threshold_fraction;
EXTERN int g_nfibers;
EXTERN int g_verbosity;
EXTERN volatile int nready;
EXTERN int g_serv_part;
EXTERN int g_nremote_exec_ops;
EXTERN int g_nremote_exec_srv;
EXTERN int g_twopc_cur_srv;
EXTERN int g_netconn_port;

EXTERN struct task* g_tasks[144][16];

EXTERN int g_active_servers;
EXTERN int g_startup_servers;
EXTERN int g_max_started_servers;
EXTERN volatile int g_nservers_to_begin_txns;
EXTERN volatile int *g_srv_wait;

#if ENABLE_DL_DETECT_CC
#include "dl_detect.h"
EXTERN DL_detect dl_detector;
#endif

EXTERN int g_rep_time;
EXTERN int g_master_socket;
EXTERN int g_vm_id;
EXTERN int g_nsockets;
EXTERN int g_nht;
EXTERN volatile int lready;
EXTERN volatile int sready;

#if YCSB_BENCHMARK_DYNAMIC
  EXTERN struct drand48_data *rand_buffer;
  EXTERN double g_theta[2];
  EXTERN double g_zetan[2];
  EXTERN double g_zeta2[2];
  EXTERN double g_eta[2];
  EXTERN double g_alpha_half_pow[2];
#endif

#if MICRO_BENCHMARK_DYNAMIC
  EXTERN int g_use_ht;
#endif

#if YCSB_BENCHMARK
  EXTERN struct drand48_data *rand_buffer;
  EXTERN double g_theta;
  EXTERN double g_zetan;
  EXTERN double g_zeta2;
  EXTERN double g_eta;
  EXTERN double g_alpha_half_pow;
#endif

#if defined(DYNAMIC_SCALING) || defined(CONTENTION_SCALING)
  EXTERN volatile int g_start_monitoring;
#endif
