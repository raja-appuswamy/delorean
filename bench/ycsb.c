#include "headers.h"
#include "partition.h"
#include "smphashtable.h"
#include "benchmark.h"
#include "twopl.h"

#if ENABLE_2PC
#include "comm/twopc.h"
//#include "comm/remote_ops_q.h"
#endif

#define YCSB_NFIELDS 10
#define YCSB_FIELD_SZ 100
#define YCSB_REC_SZ (YCSB_NFIELDS * YCSB_FIELD_SZ)

struct drand48_data *rand_buffer;
#if YCSB_BENCHMARK_DYNAMIC
double g_theta[2];
double g_zetan[2];
double g_zeta2[2];
double g_eta[2];
double g_alpha_half_pow[2];
#endif

#if YCSB_BENCHMARK
double g_theta;
double g_zetan;
double g_zeta2;
double g_eta;
double g_alpha_half_pow;
#endif

double rand_off;

extern void *micro_alloc_query();
double *old_theta;

#if ENABLE_2PC
struct twopc_txn_ops **in_remote_ops;
#endif

double zeta(uint64_t n, double theta) {
	double sum = 0;
	for (uint64_t i = 1; i <= n; i++)
		sum += pow(1.0 / i, theta);
	return sum;
}

void ycsb_init() {
	printf("Initializing zipf\n");
	rand_buffer = (struct drand48_data *) calloc(g_nservers, sizeof(struct drand48_data));
	for (int i = 0; i < g_nservers; i ++) {
		srand48_r(i + 1, &rand_buffer[i]);
	}

	uint64_t n = g_nrecs - 1;
#if YCSB_BENCHMARK_DYNAMIC
	old_theta = (double *) calloc(g_nservers, sizeof(double));
	for (int i = 0; i < g_nservers; i++) {
		old_theta[i] = g_theta[0];
	}
	for (int i = 0; i < 2; i++) {
		g_zetan[i] = zeta(n, g_theta[i]);
		g_zeta2[i] = zeta(2, g_theta[i]);

		g_eta[i] = (1 - pow(2.0 / n, 1 - g_theta[i])) / (1 - g_zeta2[i] / g_zetan[i]);
		g_alpha_half_pow[i] = 1 + pow(0.5, g_theta[i]);
	}
#elif YCSB_BENCHMARK
	g_zetan = zeta(n, g_alpha);
	g_zeta2 = zeta(2, g_alpha);

	g_eta = (1 - pow(2.0 / n, 1 - g_alpha)) / (1 - g_zeta2 / g_zetan);
	g_alpha_half_pow = 1 + pow(0.5, g_alpha);
#endif

#if EMULATE_SQLVM
	n = g_nrecs / g_serv_part - 1;
#endif

//	g_zetan = zeta(n, g_alpha);
//	g_zeta2 = zeta(2, g_alpha);
//
//	g_eta = (1 - pow(2.0 / n, 1 - g_alpha)) / (1 - g_zeta2 / g_zetan);
//	g_alpha_half_pow = 1 + pow(0.5, g_alpha);
	printf("n = %ld\n", n);
	printf("theta = %.2f\n", g_alpha);
#if ENABLE_2PC
	if (g_nremote_exec_ops > 0) {
		in_remote_ops = (struct twopc_txn_ops **) calloc(g_nservers, sizeof(struct twopc_txn_ops *));
		for (int i = 0; i < g_nservers; i++) {
			in_remote_ops[i] = (struct twopc_txn_ops *) malloc(twopc_txn_ops_struct_size());
		}
	}
#endif
}

void zipf_val(int s, struct hash_op *op) {
	uint64_t n = g_nrecs - 1;
#if EMULATE_SQLVM
	int servers_per_partition = g_startup_servers / g_serv_part;
	int cur_srv_part = s / servers_per_partition;
	n = g_nrecs / g_serv_part - 1;
	uint64_t recs_per_partition = g_nrecs / g_serv_part;
	uint64_t recs_per_server_part = recs_per_partition / servers_per_partition;
#endif

#if YCSB_BENCHMARK_DYNAMIC
//	double cur_time = now();
	int cnt = hash_table->partitions[s].rotate_theta;
	if (g_theta[cnt] != old_theta[s]) {
		printf("srv %d switched from theta %.3f to theta %.3f\n", s, old_theta[s], g_theta[cnt]);
		old_theta[s] = g_theta[cnt];
	}
	double alpha = 1 / (1 - g_theta[cnt]);
	double u;
	drand48_r(&rand_buffer[s], &u);
//	u = drand48();
	double uz = u * g_zetan[cnt];
	if (uz < 1) {
		op->key = 0;
	} else if (uz < g_alpha_half_pow[cnt]) {
		op->key = 1;
	} else {
		op->key = (uint64_t)(n * pow(g_eta[cnt] * u - g_eta[cnt] + 1, alpha));
	}
#elif YCSB_BENCHMARK
	double alpha = 1 / (1 - g_alpha);

	double u;
	drand48_r(&rand_buffer[s], &u);
	double uz = u * g_zetan;
	uint64_t val = 0;
	if (uz < 1) {
		val = 0;
	} else if (uz < g_alpha_half_pow) {
		val = 1;
	} else {
		val = (n * pow(g_eta * u - g_eta + 1, alpha));
	}
	op->key = val % n;
#endif


//	double alpha = 1 / (1 - g_alpha);
//
//	double u;
//	drand48_r(&rand_buffer[s], &u);
//	double uz = u * g_zetan;
//	if (uz < 1) {
//		op->key = 0;
//	} else if (uz < g_alpha_half_pow) {
//		op->key = 1;
//	} else {
//		op->key = (uint64_t)(n * pow(g_eta * u - g_eta + 1, alpha));
//	}



#if EMULATE_SQLVM
	int tserver = op->key % servers_per_partition;
//	uint64_t key_cnt = op->key / servers_per_partition;
//	op->key = tserver * recs_per_server_part + key_cnt;

	op->key = cur_srv_part * recs_per_partition + op->key;
	tserver = cur_srv_part * servers_per_partition + tserver;

//	printf("src %d trg %d key %ld\n", s, tserver, op->key);

	int tserver_part = tserver / servers_per_partition;
	assert(cur_srv_part == tserver_part);
	int key_part = op->key / recs_per_partition;
	assert(cur_srv_part == key_part);
#else
	// get the server id for the key
	int tserver = op->key % g_nservers;
	// get the key count for the key
	uint64_t key_cnt = op->key / g_nservers;

	uint64_t recs_per_server = g_nrecs / g_nservers;
	op->key = tserver * recs_per_server + key_cnt;
#endif


	assert(op->key < g_nrecs);
}

void zipf_val_remote(int s, struct hash_op *op, int cur_op) {
	uint64_t n = g_nrecs - 1;
#if EMULATE_SQLVM
	int servers_per_partition = g_startup_servers / g_serv_part;
	int cur_srv_part = s / servers_per_partition;
	n = g_nrecs / g_serv_part - 1;
	uint64_t recs_per_partition = g_nrecs / g_serv_part;
	uint64_t recs_per_server_part = recs_per_partition / servers_per_partition;
#elif ENABLE_2PC
	n -= 2 * g_nremote_exec_srv;
#endif

#if YCSB_BENCHMARK_DYNAMIC
//	double cur_time = now();
	int cnt = hash_table->partitions[s].rotate_theta;
	if (g_theta[cnt] != old_theta[s]) {
		printf("srv %d switched from theta %.3f to theta %.3f\n", s, old_theta[s], g_theta[cnt]);
		old_theta[s] = g_theta[cnt];
	}
	double alpha = 1 / (1 - g_theta[cnt]);
	double u;
	drand48_r(&rand_buffer[s], &u);
//	u = drand48();
	double uz = u * g_zetan[cnt];
	if (uz < 1) {
		op->key = 0;
	} else if (uz < g_alpha_half_pow[cnt]) {
		op->key = 1;
	} else {
		op->key = (uint64_t)(n * pow(g_eta[cnt] * u - g_eta[cnt] + 1, alpha));
	}
#elif YCSB_BENCHMARK
	double alpha = 1 / (1 - g_alpha);

	double u;
	drand48_r(&rand_buffer[s], &u);
	double uz = u * g_zetan;
	uint64_t val = 0;
	if (uz < 1) {
		val = 0;
	} else if (uz < g_alpha_half_pow) {
		val = 1;
	} else {
		val = (n * pow(g_eta * u - g_eta + 1, alpha));
	}
	op->key = val % n;
#endif


//	double alpha = 1 / (1 - g_alpha);
//
//	double u;
//	drand48_r(&rand_buffer[s], &u);
//	double uz = u * g_zetan;
//	if (uz < 1) {
//		op->key = 0;
//	} else if (uz < g_alpha_half_pow) {
//		op->key = 1;
//	} else {
//		op->key = (uint64_t)(n * pow(g_eta * u - g_eta + 1, alpha));
//	}



#if EMULATE_SQLVM
	int tserver = op->key % servers_per_partition;
//	uint64_t key_cnt = op->key / servers_per_partition;
//	op->key = tserver * recs_per_server_part + key_cnt;

	op->key = cur_srv_part * recs_per_partition + op->key;
	tserver = cur_srv_part * servers_per_partition + tserver;

//	printf("src %d trg %d key %ld\n", s, tserver, op->key);

	int tserver_part = tserver / servers_per_partition;
	assert(cur_srv_part == tserver_part);
	int key_part = op->key / recs_per_partition;
	assert(cur_srv_part == key_part);
#elif ENABLE_2PC
	// get the server id for the key
	int tserver = op->key % g_nservers;
	// get the key count for the key
	uint64_t key_cnt = op->key / g_nservers;

	uint64_t recs_per_server = g_nrecs / g_nservers;
	op->key = tserver * recs_per_server + key_cnt;

	if (g_nremote_exec_srv > 0) {
		int remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
		if (cur_op < g_nremote_exec_ops) {
			if (remote_server == g_twopc_cur_srv) {
				op->key = op->key + 1;
			}
		} else {
				while (remote_server != g_twopc_cur_srv) {
	//				printf("Key before update was %ld\n", op->key);
					op->key = op->key + 1;
					remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
	//				printf("Updating the key to become %ld\n", op->key);
				}
	//			remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
	//			assert(remote_server == g_twopc_cur_srv);
		}
	}
#else
	// get the server id for the key
	int tserver = op->key % g_nservers;
	// get the key count for the key
	uint64_t key_cnt = op->key / g_nservers;

	uint64_t recs_per_server = g_nrecs / g_nservers;
	op->key = tserver * recs_per_server + key_cnt;
#endif


	assert(op->key < g_nrecs);
}


void ycsb_load_data(struct hash_table *hash_table, int id)
{
  struct elem *e;
  uint64_t *value;
  struct partition *p = &hash_table->partitions[id];

#if SHARED_EVERYTHING
    // quick hack to get SE to load data from all threads
  //  uint64_t nrecs_per_server = p->nrecs / g_startup_servers;
    uint64_t nrecs_per_server = p->nrecs / g_nservers;
    uint64_t qid = id * nrecs_per_server;
    uint64_t eqid = qid + nrecs_per_server;
#else
  uint64_t qid = id * p->nrecs;
  uint64_t eqid = qid + p->nrecs;
#endif

  printf("Partition %d loading start record %"PRIu64" to end"
          "record %"PRIu64"\n", id, qid, eqid);
  
  while (qid < eqid) {
    p->ninserts++;
    e = hash_insert(p, qid, YCSB_REC_SZ, NULL);
    assert(e);

    for (int i = 0; i < YCSB_NFIELDS; i++) {
        value = (uint64_t *)(e->value + i * YCSB_FIELD_SZ);
        value[0] = 0;
    }

    e->ref_count = 1;

    qid++;
  }
}

static void sn_make_operation(struct hash_table *hash_table, int s, 
    struct hash_op *op)
{
  struct partition *p = &hash_table->partitions[s];
  uint64_t nrecs_per_server = p->nrecs; 
  uint64_t nrecs = nrecs_per_server * g_nservers;

  op->size = 0;
  zipf_val(s, op);
}

static void se_make_operation(struct hash_table *hash_table, int s, 
    struct hash_op *op)
{
  struct partition *p = &hash_table->partitions[s];
  uint64_t nrecs = p->nrecs; 
  uint64_t nrecs_per_server = g_nrecs / g_nservers;

  op->size = 0;
  zipf_val(s, op);
}

static void se_make_operation_remote(struct hash_table *hash_table, int s,
    struct hash_op *op, int cur_op)
{
  struct partition *p = &hash_table->partitions[s];
  uint64_t nrecs = p->nrecs;
  uint64_t nrecs_per_server = g_nrecs / g_nservers;

  op->size = 0;
  zipf_val_remote(s, op, cur_op);
}


void ycsb_get_next_query(struct hash_table *hash_table, int s, void *arg)
{
//	printf("Getting next query for server %d\n", s);
  struct hash_query *query = (struct hash_query *) arg;
  struct partition *p = &hash_table->partitions[s];
  int r = URand(&p->seed, 1, 99);
  char is_duplicate = 0, is_ronly = 0;
  uint64_t nrecs_per_server = g_nrecs / g_nservers;
  
  query->nops = g_ops_per_txn;

  /* transaction is either all read or all update */
  /* in this mode, we devote a bunch of threads to reads and another set to
   * writes. Read threads do only reads, write only write. In this mode,
   * g_write_threshold indicates the number of writer threads.
   * g_write_threshold = 0 means all readers
   * g_write_threshold = g_nservers means all writers
   */
#if USE_WRITERS_AS_FRACTION
  if (s >= g_write_threshold_fraction * g_active_servers)
	  is_ronly = 1;
#else
  assert(g_write_threshold <= g_nservers);
  if (s >= g_write_threshold)
      is_ronly = 1;
#endif


//#if ENABLE_2PC
//  int remote_reqs = 0;
//#endif // ENABLE_2PC
  for (int i = 0; i < g_ops_per_txn; i++) {
    struct hash_op *op = &query->ops[i];

    op->optype = (is_ronly ? OPTYPE_LOOKUP : OPTYPE_UPDATE);

    do {
#if SHARED_EVERYTHING
#if ENABLE_2PC
    	se_make_operation_remote(hash_table, s, op, i);
#else
        se_make_operation(hash_table, s, op);
#endif //ENABLE_2PC
#else
        sn_make_operation(hash_table, s, op);
#endif
        is_duplicate = FALSE;

        for (int j = 0; j < i; j++) {
            if (op->key == query->ops[j].key) {
                is_duplicate = TRUE;
                break;
            }
        }

//#if ENABLE_2PC
//				int remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
//				if (remote_server != g_twopc_cur_srv) {
//					if (remote_reqs < g_nremote_exec_ops) {
////						printf("Added a remote request\n");
//						remote_reqs++;
//					} else {
//						is_duplicate = TRUE;
//					}
//				} else {
//					if (remote_reqs < g_nremote_exec_ops) {
//						is_duplicate = TRUE;
//					}
//				}
//#endif

    } while(is_duplicate == TRUE);
  }

#if ENABLE_KEY_SORTING
  // sort ops based on key
  for (int i = 0; i < g_ops_per_txn; i++) {
      for (int j = i + 1; j < g_ops_per_txn; j++) {
        if (query->ops[i].key > query->ops[j].key) {
            struct hash_op tmp = query->ops[i];
            query->ops[i] = query->ops[j];
            query->ops[j] = tmp;
        }
      }
  }
#endif

  p->q_idx++;
}

int ycsb_run_txn_remote_worker(struct hash_table *hash_table, int s, void *arg, struct task *ctask) {
  struct txn_ctx *txn_ctx = &ctask->txn_ctx;
  int i;
  void *value;
  int nrecs_per_partition = hash_table->partitions[0].nrecs;


#if ENABLE_2PC
  int remote_workers = 9;
  ctask->rtxn_ctx = txn_ctx;
  struct partition *p = &hash_table->partitions[s];

	struct txn_ctx *incoming_txn_ctx = (struct txn_ctx *) malloc(sizeof(struct txn_ctx));
// check for incoming remote ops
	int received_txns = 0;
	double avg_exec_time = 0;
	double avg_send_time = 0;
	double avg_store_time = 0;
	long counter = 0;
	while (!p->thread_termination_flag) {
		int in_rops_count = 0;
		in_rops_count = twopc_rtxn_have_space(s);
		if (in_rops_count != -1) {
			in_rops_count = twopc_pop_remote_ops(in_remote_ops[s]);
		}
		if (in_rops_count > 0) {
			// start a new txn
			struct txn_ctx remote_txn_ctx;

			txn_start(hash_table, s, &remote_txn_ctx);
			remote_txn_ctx.rtxn_id = in_remote_ops[s]->txn_id;

			int rr = TXN_COMMIT;
			struct hash_op *all_ops = (struct hash_op *) in_remote_ops[s]->hash_ops;
			for (i = 0; i < in_remote_ops[s]->ops_cnt; i++) {
				struct hash_op *op = (struct hash_op *) &all_ops[i];
				int tserver = ((int)(op->key)) / nrecs_per_partition;
				assert((tserver >= 0) && (tserver < g_nfibers * g_nservers));
				ctask->rtxn_ctx = &remote_txn_ctx;
				value = txn_op(ctask, hash_table, s, op, tserver);

				if (!value) {
					rr = TXN_ABORT;
					break;
				}

				// in both lookup and update, we just check the value
				uint64_t first_field = *(uint64_t *)value;

				for (int j = 1; j < YCSB_NFIELDS; j++) {
					uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
					assert(*next_field == first_field);
				}

				if (op->optype == OPTYPE_UPDATE) {
					for (int j = 0; j < YCSB_NFIELDS; j++) {
						uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
						next_field[0]++;
					}
				}
			}

//			remote_txn_ctx.rtxn_id = in_remote_ops[s]->txn_id;
			// store the txn context
			int ret = twopc_store_remotely_issued_txn_ctx(s, &remote_txn_ctx);
			while ((ret == -1) && (!p->thread_termination_flag)) {
//				assert(0);
				printf("Waiting...\n");
				ret = twopc_store_remotely_issued_txn_ctx(s, &remote_txn_ctx);
			}
			twopc_cli_send_txn_result(in_remote_ops[s]->src_srv, in_remote_ops[s]->txn_id, rr, s, in_remote_ops[s]->src_issue_srv, in_remote_ops[s]->ltxn_pos, ret);
		}


		int idx = 0;
		int incoming_result;

		// check for incoming final results
		idx = twopc_load_rtxn_final_results(s, &incoming_result, &incoming_txn_ctx, idx);
		if (idx != -1) {
			ctask->rtxn_ctx = incoming_txn_ctx;
			if (incoming_result == TXN_COMMIT) {
				txn_commit(ctask, hash_table, s, TXN_SINGLE);
			} else {
				txn_abort(ctask, hash_table, s, TXN_SINGLE);
			}
			twopc_invalidate_rtxn(s, idx);
		}
	}

#endif //ENABLE_2PC


	return TXN_COMMIT;

}

int ycsb_run_txn_local_worker(struct hash_table *hash_table, int s, void *arg, struct task *ctask) {
	struct hash_query *query = (struct hash_query *) arg;
  struct task *task_backup = ctask;
  struct txn_ctx *txn_ctx = &ctask->txn_ctx;
  int i, r;
  void *value;
  int nrecs_per_partition = hash_table->partitions[0].nrecs;

  r = TXN_COMMIT;
  int query_ops = query->nops;
#if ENABLE_2PC
  ctask->rtxn_ctx = txn_ctx;
  int ltxn_space = 0;
  if (g_nremote_exec_ops > 0) {
  	ltxn_space = twopc_ltxn_have_space(s);
  }
  if ((ltxn_space != -1) || (g_nremote_exec_ops == 0)) {

#endif
  	txn_start(hash_table, s, txn_ctx);
  for (i = g_nremote_exec_ops; i < query_ops; i++) {
    struct hash_op *op = &query->ops[i];

    int tserver = ((int)(op->key)) / nrecs_per_partition;
    assert((tserver >= 0) && (tserver < g_nfibers * g_nservers));

    value = txn_op(ctask, hash_table, s, op, tserver);
    assert(ctask == task_backup);
    if (!value) {
        r = TXN_ABORT;
        break;
    }

    // in both lookup and update, we just check the value
    uint64_t first_field = *(uint64_t *)value;

    for (int j = 1; j < YCSB_NFIELDS; j++) {
        uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
        assert(*next_field == first_field);
    }

    if (op->optype == OPTYPE_UPDATE) {
        for (int j = 0; j < YCSB_NFIELDS; j++) {
            uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
            next_field[0]++;
        }
    }
  }

  if (g_nremote_exec_ops == 0) {
  	if (r == TXN_COMMIT) {
  	    r = txn_commit(ctask, hash_table, s, TXN_SINGLE);
  	    query->state = HASH_QUERY_COMMITTED;
  	  } else {
  	    txn_abort(ctask, hash_table, s, TXN_SINGLE);
  	    query->state = HASH_QUERY_ABORTED;
  	//		uint64_t penalty = URand(&p->seed, 0, ABORT_PENALTY);
  	//		usleep(penalty / 1000);
  	  }
  }

#if ENABLE_2PC
  } else if (ltxn_space == - 1) {	// if (ltxn_space != -1)
//		printf("Not enough space... aborting\n");
  	r = TXN_ABORT;
	}
  if (g_nremote_exec_ops > 0) {
		int remote_requests = 0;
		int active_remote_servers[g_nremote_exec_srv + 1];
		memset(active_remote_servers, 0, (g_nremote_exec_srv + 1) * sizeof(int));

		if (r == TXN_COMMIT) {
			ctask->rtxn_ctx = txn_ctx;
			for (i = 0; i < g_nremote_exec_ops; i++) {
				struct hash_op *op = &query->ops[i];


				/*
				 *  Check whether the op should stay within this server or
				 *  it should stay within this context.
				 *  We consider hash partitioning of the records to avoid
				 *  overloading the first server that would keep the hot set
				 *  in the case of range partitioning.
				 */

				int remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
				if (remote_server != g_twopc_cur_srv) {
					active_remote_servers[remote_server] = 1;
					twopc_cli_insert_remote_op(s, remote_server, ctask->rtxn_ctx->ts, op);
				} else {
					assert(0);
				}
			}
			for (i = 0; i < g_nremote_exec_srv + 1; i++) {
				remote_requests += active_remote_servers[i];
				if (active_remote_servers[i] == 0) {
					active_remote_servers[i] = -1;
				}
			}
			// now flush everything
			if (remote_requests > 0) {
				int ret = twopc_store_locally_issued_txn_ctx(s, remote_requests, ctask->rtxn_ctx, active_remote_servers);
				if (ret != -1) {
					twopc_cli_flush_rops(s, ret);
				} else {
					printf("Not enough space... aborting -- SHOULD NOT NOW!!!\n");
					txn_abort(ctask, hash_table, s, TXN_SINGLE);
					r = TXN_ABORT;
				}

			}
		}

		// check for incoming results
		struct txn_ctx *incoming_txn_ctx = (struct txn_ctx *) malloc(sizeof(struct txn_ctx));
		int idx = 0;
		int incoming_result;
		int rtxn_pos = 0;
		int *remote_srvs = (int *) calloc(g_nremote_exec_srv + 1, sizeof(int));
		idx = twopc_load_ltxn_results(s, &incoming_result, &incoming_txn_ctx, &remote_srvs, idx, &rtxn_pos);
		if (idx != -1) {
			ctask->rtxn_ctx = incoming_txn_ctx;
			if (incoming_result == TXN_COMMIT) {
				txn_commit(ctask, hash_table, s, TXN_SINGLE);
				query->state = HASH_QUERY_COMMITTED;
			} else {
				txn_abort(ctask, hash_table, s, TXN_SINGLE);
				query->state = HASH_QUERY_ABORTED;
			}
			twopc_cli_send_final_result(s, ctask->rtxn_ctx->ts, incoming_result, remote_srvs, rtxn_pos);
			twopc_invalidate_ltxn(s, idx);
		}
  }
#endif //ENABLE_2PC

  return r;	// we handle abort locally here
}

int ycsb_run_txn_remote_support(struct hash_table *hash_table, int s, void *arg,
    struct task *ctask) {

	struct hash_query *query = (struct hash_query *) arg;
  struct task *task_backup = ctask;
  struct txn_ctx *txn_ctx = &ctask->txn_ctx;
  int i, r;
  void *value;
  int nrecs_per_partition = hash_table->partitions[0].nrecs;

  txn_start(hash_table, s, txn_ctx);

  r = TXN_COMMIT;
  int query_ops = query->nops;
#if ENABLE_2PC
  int remote_workers = 9;
  ctask->rtxn_ctx = txn_ctx;
  struct partition *p = &hash_table->partitions[s];
  int ltxn_space = 0;
  if (g_nremote_exec_ops > 0) {
  	ltxn_space = twopc_ltxn_have_space(s);
  }
  if (((ltxn_space != -1) && (s < (g_nservers - remote_workers))) || (g_nremote_exec_ops == 0)) {
//  	printf("srv %d executing txn using idx %d\n", s, ltxn_space);
#endif

  for (i = g_nremote_exec_ops; i < query_ops; i++) {
    struct hash_op *op = &query->ops[i];

    int tserver = ((int)(op->key)) / nrecs_per_partition;
    assert((tserver >= 0) && (tserver < g_nfibers * g_nservers));

    value = txn_op(ctask, hash_table, s, op, tserver);
    assert(ctask == task_backup);
    if (!value) {
        r = TXN_ABORT;
        break;
    }

    // in both lookup and update, we just check the value
    uint64_t first_field = *(uint64_t *)value;

    for (int j = 1; j < YCSB_NFIELDS; j++) {
        uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
        assert(*next_field == first_field);
    }

    if (op->optype == OPTYPE_UPDATE) {
        for (int j = 0; j < YCSB_NFIELDS; j++) {
            uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
            next_field[0]++;
        }
    }
  }

  if (g_nremote_exec_ops == 0) {
  	if (r == TXN_COMMIT) {
  	    r = txn_commit(ctask, hash_table, s, TXN_SINGLE);
  	    query->state = HASH_QUERY_COMMITTED;
  	  } else {
  	    txn_abort(ctask, hash_table, s, TXN_SINGLE);
  	    query->state = HASH_QUERY_ABORTED;
  	//		uint64_t penalty = URand(&p->seed, 0, ABORT_PENALTY);
  	//		usleep(penalty / 1000);
  	  }
  }

#if ENABLE_2PC
  } else if ((ltxn_space == - 1) && (s < (g_nservers - remote_workers))) {	// if (ltxn_space != -1)
		printf("Not enough space... aborting\n");
  	r = TXN_ABORT;
	}
//  if (g_nremote_exec_ops > 0) {
  if (__builtin_expect(g_nremote_ops == 0, 1)) {
  	if (s < (g_nservers - remote_workers)) {
//  		if (s == 0) {
//  			double cur_time = now();
//  			double dt = cur_time - p->txn_prev_time;
//  			printf("SRV %d issuing remote ops for txn after %.9f\n", s, dt);
//  			p->txn_prev_time = cur_time;
//  		}
			int remote_requests = 0;
			int active_remote_servers[g_nremote_exec_srv + 1];
			memset(active_remote_servers, 0, (g_nremote_exec_srv + 1) * sizeof(int));

			if (r == TXN_COMMIT) {
				ctask->rtxn_ctx = txn_ctx;
				for (i = 0; i < g_nremote_exec_ops; i++) {
					struct hash_op *op = &query->ops[i];


					/*
					 *  Check whether the op should stay within this server or
					 *  it should stay within this context.
					 *  We consider hash partitioning of the records to avoid
					 *  overloading the first server that would keep the hot set
					 *  in the case of range partitioning.
					 */

					int remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
					if (remote_server != g_twopc_cur_srv) {
						active_remote_servers[remote_server] = 1;
						twopc_cli_insert_remote_op(s, remote_server, ctask->rtxn_ctx->ts, op);
					} else {
						assert(0);
					}
				}
				for (i = 0; i < g_nremote_exec_srv + 1; i++) {
					remote_requests += active_remote_servers[i];
					if (active_remote_servers[i] == 0) {
						active_remote_servers[i] = -1;
					}
				}
				// now flush everything
				if (remote_requests > 0) {
					int ret = twopc_store_locally_issued_txn_ctx(s, remote_requests, ctask->rtxn_ctx, active_remote_servers);
		//			while (ret == -1) {
		////				assert(0);
		//				ret = twopc_store_locally_issued_txn_ctx(s, remote_requests, ctask->rtxn_ctx, active_remote_servers);
		//			}
					if (ret != -1) {
//						for (i = 0; i < g_nremote_exec_ops; i++) {
//							struct hash_op *op = &query->ops[i];
//							int remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
//							if (remote_server != g_twopc_cur_srv) {
//								twopc_cli_insert_remote_op(s, remote_server, ctask->rtxn_ctx->ts, op);
//							}
//						}
						twopc_cli_flush_rops(s, ret);
					} else {
						printf("Not enough space... aborting -- SHOULD NOT NOW!!!\n");
						txn_abort(ctask, hash_table, s, TXN_SINGLE);
						r = TXN_ABORT;
					}

				}
			}

			// check for incoming results
			struct txn_ctx *incoming_txn_ctx = (struct txn_ctx *) malloc(sizeof(struct txn_ctx));
			int idx = 0;
			int incoming_result;
			int rtxn_pos = 0;
			int *remote_srvs = (int *) calloc(g_nremote_exec_srv + 1, sizeof(int));
	//		printf("1 incoming ctx addr %p\n", (void *) incoming_txn_ctx);
			idx = twopc_load_ltxn_results(s, &incoming_result, &incoming_txn_ctx, &remote_srvs, idx, &rtxn_pos);
	//		printf("2 incoming ctx addr %p\n", (void *) incoming_txn_ctx);
			if (idx != -1) {
//				assert(incoming_txn_ctx);
				ctask->rtxn_ctx = incoming_txn_ctx;
	//			for (int j = 0; j < g_nremote_exec_srv + 1; j++) {
	//				if ((remote_srvs[j] != -1) && (g_twopc_cur_srv == 1)) {
	//					printf("SRV %d confirm: TXN %ld was executed on node %d by srv %d\n", s, incoming_txn_ctx->ts, j, remote_srvs[j]);
	//				}
	//			}
//				assert(ctask->rtxn_ctx);
	//			ctask->rtxn_ctx = txn_ctx;
//		  	printf("srv %d received remote results\n", s);
				if (incoming_result == TXN_COMMIT) {
					txn_commit(ctask, hash_table, s, TXN_SINGLE);
					query->state = HASH_QUERY_COMMITTED;
	//				if ((g_twopc_cur_srv == 1)) {
	//					printf("SRV %d commited txn %ld\n", s, ctask->rtxn_ctx->ts);
	//				}
				} else {
					txn_abort(ctask, hash_table, s, TXN_SINGLE);
					query->state = HASH_QUERY_ABORTED;
		//			uint64_t penalty = URand(&p->seed, 0, ABORT_PENALTY);
		//			usleep(penalty / 1000);
				}
				twopc_cli_send_final_result(s, ctask->rtxn_ctx->ts, incoming_result, remote_srvs, rtxn_pos);
				twopc_invalidate_ltxn(s, idx);
		//  	memset(active_remote_servers, 0, (g_nremote_exec_srv + 1) * sizeof(short));
//				idx = twopc_load_ltxn_results(s, &incoming_result, &incoming_txn_ctx, &remote_srvs, idx, &rtxn_pos);
			} //else {
//				if (s == 0) {
//					printf("SRV %d did not find any incoming results\n", s);
//				}
//			}

  	} else {
  		struct txn_ctx *incoming_txn_ctx = (struct txn_ctx *) malloc(sizeof(struct txn_ctx));
	//  printf("Sent local ops\n");
		// check for incoming remote ops
  		int received_txns = 0;
  		double avg_exec_time = 0;
  		double avg_send_time = 0;
  		double avg_store_time = 0;
  		long counter = 0;
				while (!p->thread_termination_flag) {
					int rops_space = 0;
					int in_rops_count = 0;
					rops_space = twopc_rtxn_have_space(s);
					if (rops_space != -1) {
						in_rops_count = twopc_pop_remote_ops(in_remote_ops[s]);
	//					printf("srv %d got a remote op\n", s);
					} else {
//						printf("srv %d ran out of space\n", s);
					}
					if (in_rops_count > 0) {
						received_txns++;
//				  	printf("srv %d executing remote ops of txn %ld\n", s, in_remote_ops[s]->txn_id);
						// start a new txn
						struct txn_ctx remote_txn_ctx;

//						double stime = now();
						txn_start(hash_table, s, &remote_txn_ctx);
						remote_txn_ctx.rtxn_id = in_remote_ops[s]->txn_id;
			//			remote_txn_ctx.nops = in_remote_ops[s]->ops_cnt;

						int rr = TXN_COMMIT;
						struct hash_op *all_ops = (struct hash_op *) in_remote_ops[s]->hash_ops;
						for (i = 0; i < in_remote_ops[s]->ops_cnt; i++) {
							struct hash_op *op = (struct hash_op *) &all_ops[i];
							int tserver = ((int)(op->key)) / nrecs_per_partition;
							assert((tserver >= 0) && (tserver < g_nfibers * g_nservers));
				//				printf("ctask addr is %p\n", (void *) ctask);
							ctask->rtxn_ctx = &remote_txn_ctx;
//							assert(hash_table);
//							assert(&hash_table->partitions[s]);
							value = txn_op(ctask, hash_table, s, op, tserver);

							if (!value) {
								rr = TXN_ABORT;
								break;
							}

							// in both lookup and update, we just check the value
							uint64_t first_field = *(uint64_t *)value;

							for (int j = 1; j < YCSB_NFIELDS; j++) {
								uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
								assert(*next_field == first_field);
							}

							if (op->optype == OPTYPE_UPDATE) {
								for (int j = 0; j < YCSB_NFIELDS; j++) {
									uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
									next_field[0]++;
								}
							}
						}

//						double etime = now();

//						double dt = (etime - stime) * 1000;
//						avg_exec_time += dt;
//						printf("Time for txn exec %.9f\n", (etime - stime) * 1000);
						remote_txn_ctx.rtxn_id = in_remote_ops[s]->txn_id;
						// store the txn context
						int ret = twopc_store_remotely_issued_txn_ctx(s, &remote_txn_ctx);
						while ((ret == -1) && (!p->thread_termination_flag)) {
			//				assert(0);
							printf("Waiting...\n");
							ret = twopc_store_remotely_issued_txn_ctx(s, &remote_txn_ctx);
						}
//						stime = now();
//						dt = (stime - etime) * 1000;
//						avg_store_time += dt;
//						printf("Time for txn store %.9f\n", (stime - etime) * 1000);
						twopc_cli_send_txn_result(in_remote_ops[s]->src_srv, in_remote_ops[s]->txn_id, rr, s, in_remote_ops[s]->src_issue_srv, in_remote_ops[s]->ltxn_pos, ret);
//						etime = now();
//						dt = (etime - stime) * 1000;
//						avg_send_time += dt;
//						counter++;
//							printf("Time for txn send %.9f\n", (etime - stime) * 1000);
					} else {
//						printf("srv %d does not have anything in its queue\n", s);
					}


//					struct txn_ctx *incoming_txn_ctx = (struct txn_ctx *) malloc(sizeof(struct txn_ctx));
					int idx = 0;
					int incoming_result;

					// check for incoming final results
					idx = twopc_load_rtxn_final_results(s, &incoming_result, &incoming_txn_ctx, idx);
					if (idx != -1) {
						ctask->rtxn_ctx = incoming_txn_ctx;
//						printf("Received final results\n");
						if (incoming_result == TXN_COMMIT) {
							txn_commit(ctask, hash_table, s, TXN_SINGLE);
							query->state = HASH_QUERY_COMMITTED;
						} else {
							txn_abort(ctask, hash_table, s, TXN_SINGLE);
							query->state = HASH_QUERY_ABORTED;
				//			uint64_t penalty = URand(&p->seed, 0, ABORT_PENALTY);
				//			usleep(penalty / 1000);
						}
						twopc_invalidate_rtxn(s, idx);
			//			idx = twopc_load_rtxn_final_results(s, &incoming_result, &incoming_txn_ctx, idx);
					}
				}
				printf("srv %d received %d txns\n", s, received_txns);
//				avg_exec_time = avg_exec_time / counter;
//				avg_send_time = avg_send_time / counter;
//				avg_store_time = avg_store_time / counter;
//				printf("Exec time = %.9f\n", avg_exec_time);
//				printf("Store time = %.9f\n", avg_store_time);
//				printf("Send time = %.9f\n", avg_send_time);
  		}

//		free(remote_srvs);
//		free(incoming_txn_ctx);
  }


#else
  if (r == TXN_COMMIT) {
    r = txn_commit(ctask, hash_table, s, TXN_SINGLE);
    query->state = HASH_QUERY_COMMITTED;
  } else {
    txn_abort(ctask, hash_table, s, TXN_SINGLE);
    query->state = HASH_QUERY_ABORTED;
//		uint64_t penalty = URand(&p->seed, 0, ABORT_PENALTY);
//		usleep(penalty / 1000);
  }
#endif //ENABLE_2PC



  return r;	// we handle abort locally here
}

//int ycsb_run_txn(struct hash_table *hash_table, int s, void *arg,
//    struct task *ctask)
//{
////	assert(ctask);
////	printf("ctask addr is %p\n", (void *) ctask);
//  struct hash_query *query = (struct hash_query *) arg;
//  struct task *task_backup = ctask;
//  struct txn_ctx *txn_ctx = &ctask->txn_ctx;
//  int i, r;
//  void *value;
//  int nrecs_per_partition = hash_table->partitions[0].nrecs;
//
//  txn_start(hash_table, s, txn_ctx);
////	printf("Running txn %ld\n", txn_ctx->ts);
//
//#if SHARED_NOTHING
//  /* we have to acquire all partition locks in partition order. This protocol
//   * is similar to the partitioned store design used in silo.
//   * We do this only if we know there are remote transactions
//   */
//
//  int tserver, alock_state, partitions[MAX_SERVERS], npartitions;
//  char bits[BITNSLOTS(MAX_SERVERS)], nbits;
//
//  memset(bits, 0, sizeof(bits));
//  npartitions = nbits = 0;
//
//  for (i = 0; i < query->nops; i++) {
//    tserver = query->ops[i].key / nrecs_per_partition;
//    if (BITTEST(bits, tserver)) {
//      ;
//    } else {
//      BITSET(bits, tserver);
//      nbits++;
//    }
//
//#if GATHER_STATS
//    if (tserver != s)
//      hash_table->partitions[s].nlookups_remote++;
//#endif
//  }
//
//  for (i = 0; i < g_nservers; i++) {
//    if (BITTEST(bits, i)) {
//      dprint("srv %d latching lock %d\n", s, i);
//      LATCH_ACQUIRE(&hash_table->partitions[i].latch, &alock_state);
//      dprint("srv %d got lock %d\n", s, i);
//      partitions[npartitions++] = i;
//      nbits--;
//    }
//
//    if (nbits == 0)
//      break;
//  }
//
//#endif
//
//  r = TXN_COMMIT;
//  int query_ops = query->nops;
////#if ENABLE_2PC
////  int tserver;
////  for (i = 0; i < g_nremote_exec_ops; i++) {
////  	int tserver = ((int)(op->key)) / nrecs_per_partition;
////  	struct hash_op *op = &query->ops[i];
////  	insert_remote_op(s, tserver, txn_ctx->ts, op);
////  }
////  flush_remote_ops(s);
////#endif
//#if ENABLE_2PC
//  struct partition *p = &hash_table->partitions[s];
//  int remote_requests = 0;
//  short active_remote_servers[g_nremote_exec_srv + 1];
//  memset(active_remote_servers, 0, (g_nremote_exec_srv + 1) * sizeof(short));
//#endif //ENABLE_PC
//  for (i = 0; i < query_ops; i++) {
//    struct hash_op *op = &query->ops[i];
//
//#if ENABLE_2PC
//    ctask->rtxn_ctx = txn_ctx;
////    assert(ctask == task_backup);
////    printf("op %d begin ctask addr is %p\n", i, (void *) ctask);
//
//    /*
//     *  Check whether the op should stay within this server or
//     *  it should stay within this context.
//     *  We consider hash partitioning of the records to avoid
//     *  overloading the first server that would keep the hot set
//     *  in the case of range partitioning.
//     */
//
//  	int remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
////  	if (i < g_nremote_exec_ops) {
////  		if (remote_server == g_twopc_cur_srv) {
////  			op->key = op->key + 1;
////  		}
////  	} else {
////  			while (remote_server != g_twopc_cur_srv) {
//////  				printf("Key before update was %ld\n", op->key);
////  				op->key = op->key + 1;
////  				remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
//////  				printf("Updating the key to become %ld\n", op->key);
////  			}
////  			remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
////  			assert(remote_server == g_twopc_cur_srv);
////  	}
////  	remote_server = (int)(op->key % (g_nremote_exec_srv + 1));
//    if (remote_server != g_twopc_cur_srv) {
////    	printf("Got a remote request with key %ld\n", op->key);
////    	if (i >= g_nremote_exec_ops) {
////    		assert(0);
////    	}
//    	active_remote_servers[remote_server] = 1;
//    	twopc_cli_insert_remote_op(s, remote_server, txn_ctx->ts, op);
////    	remote_requests++;
//    } else {
//#endif //ENABLE_2PC
//
//#if defined(MIGRATION)
//    int tserver = (int)(op->key / (g_nrecs/g_startup_servers));
//#else
//    int tserver = ((int)(op->key)) / nrecs_per_partition;
//#endif
//    assert((tserver >= 0) && (tserver < g_nfibers * g_nservers));
//
//    value = txn_op(ctask, hash_table, s, op, tserver);
//    assert(ctask == task_backup);
////    printf("op %d ctask addr is %p\n", i, (void *) ctask);
//#if defined(MIGRATION)
//    s = ctask->s;
//#endif
//    if (!value) {
//        r = TXN_ABORT;
//        break;
//    }
//
//    // in both lookup and update, we just check the value
//    uint64_t first_field = *(uint64_t *)value;
//
//    for (int j = 1; j < YCSB_NFIELDS; j++) {
//        uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
//        assert(*next_field == first_field);
//    }
//
//    if (op->optype == OPTYPE_UPDATE) {
//        for (int j = 0; j < YCSB_NFIELDS; j++) {
//            uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
//            next_field[0]++;
//        }
//    }
//#if ENABLE_2PC
//		}	// closing the else from the above #if
//#endif //ENABLE_2PC
//  }
//
//#if ENABLE_2PC
//  for (i = 0; i < g_nremote_exec_srv + 1; i++) {
//		remote_requests += active_remote_servers[i];
//	}
////  printf("Have %d remote requests\n", remote_requests);
//	// now flush everything
//  if (remote_requests > 0) {
//  	twopc_cli_flush_rops(s);
//  }
//
//#endif
//
//#if SHARED_NOTHING
//  /* in shared nothing case, there is now way a transaction will ever be
//   * aborted. We always get all partition locks before we start. So txn
//   * execution is completely serial. Likewise, there is no need to reset
//   * reference counts in records for logical locks as we don't need them.
//   *
//   * So just unlatch the partitions and be done
//   */
//
//  assert (r == TXN_COMMIT);
//
//  for (i = 0; i < npartitions; i++) {
//    int t = partitions[i];
//    assert (BITTEST(bits, t));
//    LATCH_RELEASE(&hash_table->partitions[t].latch, &alock_state);
//    dprint("srv %d releasing lock %d\n", s, i);
//  }
//#else
//#if !ENABLE_2PC
//  if (r == TXN_COMMIT) {
//    r = txn_commit(ctask, hash_table, s, TXN_SINGLE);
////    printf("commit: ctask addr is %p\n", (void *) ctask);
//  } else {
//    txn_abort(ctask, hash_table, s, TXN_SINGLE);
////    printf("abort: ctask addr is %p\n", (void *) ctask);
//  }
//#endif //!ENABLE_2PC
//#endif
//
//#if ENABLE_2PC
//  // check for incoming requests
//  if (g_nremote_exec_ops > 0) {
//    int in_rops_count = twopc_pop_remote_ops(in_remote_ops);
//    int rcv_results = 0;
//    int rcv_reqs = 0;
//    int rcv_final_results = 0;
//
//    struct txn_ctx remote_txn_ctx[g_nremote_exec_srv];
//
//  	while ((rcv_results < remote_requests) || (in_rops_count > 0) || (rcv_final_results < rcv_reqs)) {
//  		if (in_rops_count > 0) {
//  //			printf("Got remote txn %ld\n", in_remote_ops->txn_id);
//  //			txn_start(hash_table, s, txn_ctx);
//  			txn_start(hash_table, s, &remote_txn_ctx[rcv_reqs]);
//  //			txn_ctx->rtxn_id = in_remote_ops->txn_id;
//  			remote_txn_ctx[rcv_reqs].rtxn_id = in_remote_ops->txn_id;
//  			int rr = TXN_COMMIT;
//  			struct hash_op *all_ops = (struct hash_op *) in_remote_ops->hash_ops;
//  			for (i = 0; i < in_remote_ops->ops_cnt; i++) {
//  //				struct hash_op *op = &(((struct hash_op *) in_remote_ops->hash_ops)[i]);
//  //				printf("Executing remote op %d\n", i);
//  				struct hash_op *op = (struct hash_op *) &all_ops[i];
//  				int tserver = ((int)(op->key)) / nrecs_per_partition;
//  				assert((tserver >= 0) && (tserver < g_nfibers * g_nservers));
//  //				printf("ctask addr is %p\n", (void *) ctask);
//  				ctask->rtxn_ctx = &remote_txn_ctx[rcv_reqs];
//  				assert(hash_table);
//  				assert(&hash_table->partitions[s]);
//  				value = txn_op(ctask, hash_table, s, op, tserver);
//
//  				if (!value) {
//  					rr = TXN_ABORT;
//  					break;
//  				}
//
//  				// in both lookup and update, we just check the value
//  				uint64_t first_field = *(uint64_t *)value;
//
//  				for (int j = 1; j < YCSB_NFIELDS; j++) {
//  					uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
//  					assert(*next_field == first_field);
//  				}
//
//  				if (op->optype == OPTYPE_UPDATE) {
//  					for (int j = 0; j < YCSB_NFIELDS; j++) {
//  						uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
//  						next_field[0]++;
//  					}
//  				}
//  			}
//  			rcv_reqs++;
//  //			if (rcv_reqs < g_nremote_exec_srv) {
//  				twopc_cli_send_txn_result(s, in_remote_ops->src_srv, in_remote_ops->txn_id, rr);
//  //			}
//  		}
//  		if (rcv_results < remote_requests) {
//  			int result_data_count = twopc_pop_result(result_data);
//  			if (result_data_count > 0) {
//  				rcv_results++;
//  				if (result_data->result == TXN_ABORT) {
//  					r = TXN_ABORT;
//  				}
//  //				printf("Have received %d results\n", rcv_results);
//  				if (rcv_results == remote_requests) {
//  					// this means that we have received all the results and we can end the txn
//  					if (r == TXN_COMMIT) {
//  //						printf("Committing locally issued txn %ld\n", result_data->txn_id);
//  						r = txn_commit(ctask, hash_table, s, TXN_SINGLE);
//  					} else {
//  //						printf("Aborting locally issued txn %ld\n", result_data->txn_id);
//  						txn_abort(ctask, hash_table, s, TXN_SINGLE);
//  					}
//  					// send the final result to all the remote servers
//  //					printf("Sending final results\n");
//  					twopc_cli_send_final_result(s, result_data->txn_id, r);
//  				}
//  			}
//  		}
//  		if (rcv_final_results < rcv_reqs) {
//  			int fin_results_count = twopc_pop_fin_result(fin_result_data);
//  			if (fin_results_count > 0) {
//  				// find the transaction context
//  				int txn_found = 0;
//  				for (i = 0; i < g_nremote_exec_srv; i++) {
//  					if (remote_txn_ctx[i].rtxn_id == fin_result_data->txn_id) {
//  						txn_found = 1;
//  						ctask->rtxn_ctx = &remote_txn_ctx[i];
//  						if (fin_result_data->result == TXN_COMMIT) {
//  //							printf("Committing remotely issued txn %ld\n", fin_result_data->txn_id);
//  							txn_commit(ctask, hash_table, s, TXN_SINGLE);
//  						} else {
//  //							printf("Aborting remotely issued txn %ld\n", fin_result_data->txn_id);
//  							txn_abort(ctask, hash_table, s, TXN_SINGLE);
//  						}
//  						break;
//  					}
//  				}
//
//  //				assert(txn_found > 0);
//  				rcv_final_results++;
//  			}
//  		}
//
//  		if (rcv_reqs < g_nremote_exec_srv) {
//  			in_rops_count = twopc_pop_remote_ops(in_remote_ops);
//  		} else {
//  			in_rops_count = 0;
//  		}
//
//  		if (twopc_finished_srvs_count() == g_nremote_exec_srv) {
//  			break;
//  		}
//
//  		if (p->thread_termination_flag) {
//  			break;
//  		}
//  	}
//
//  }
////	printf("Exited the big while loop\n");
//#endif
//
//  return r;
//}
//
//int ycsb_run_remote_txn(struct hash_table *hash_table, int s, void *arg,
//    struct task *ctask) {
//	int r;
//
//#if ENABLE_2PC
//	struct partition *p = &hash_table->partitions[s];
//	int nrecs_per_partition = hash_table->partitions[0].nrecs;
//	void *value;
////	struct twopc_txn_ops *in_remote_ops = (struct twopc_txn_ops *) malloc(twopc_txn_ops_struct_size());
////	struct txn_result_data *result_data = (struct txn_result_data *) malloc(sizeof(struct txn_result_data));
////	struct txn_result_data *fin_result_data = (struct txn_result_data *) malloc(sizeof(struct txn_result_data));
//
//	int in_rops_count = twopc_pop_remote_ops(in_remote_ops);
//	int rcv_results = 0;
//	int rcv_reqs = 0;
//	int rcv_final_results = 0;
//
//	struct txn_ctx remote_txn_ctx[g_nremote_exec_srv];
//	int i;
//	while ((in_rops_count > 0) || (rcv_final_results < rcv_reqs)) {
//		if (in_rops_count > 0) {
////			printf("Got remote txn %ld\n", in_remote_ops->txn_id);
////			txn_start(hash_table, s, txn_ctx);
//			txn_start(hash_table, s, &remote_txn_ctx[rcv_reqs]);
////			txn_ctx->rtxn_id = in_remote_ops->txn_id;
//			remote_txn_ctx[rcv_reqs].rtxn_id = in_remote_ops->txn_id;
//			int rr = TXN_COMMIT;
//			struct hash_op *all_ops = (struct hash_op *) in_remote_ops->hash_ops;
//			for (i = 0; i < in_remote_ops->ops_cnt; i++) {
////				struct hash_op *op = &(((struct hash_op *) in_remote_ops->hash_ops)[i]);
////				printf("Executing remote op %d\n", i);
//				struct hash_op *op = (struct hash_op *) &all_ops[i];
//				int tserver = ((int)(op->key)) / nrecs_per_partition;
//				assert((tserver >= 0) && (tserver < g_nfibers * g_nservers));
////				printf("ctask addr is %p\n", (void *) ctask);
//				ctask->rtxn_ctx = &remote_txn_ctx[rcv_reqs];
//				assert(hash_table);
//				assert(&hash_table->partitions[s]);
//				value = txn_op(ctask, hash_table, s, op, tserver);
//
//				if (!value) {
//					rr = TXN_ABORT;
//					break;
//				}
//
//				// in both lookup and update, we just check the value
//				uint64_t first_field = *(uint64_t *)value;
//
//				for (int j = 1; j < YCSB_NFIELDS; j++) {
//					uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
//					assert(*next_field == first_field);
//				}
//
//				if (op->optype == OPTYPE_UPDATE) {
//					for (int j = 0; j < YCSB_NFIELDS; j++) {
//						uint64_t *next_field = (uint64_t *)(value + j * YCSB_FIELD_SZ);
//						next_field[0]++;
//					}
//				}
//			}
//			rcv_reqs++;
////			if (rcv_reqs < g_nremote_exec_srv) {
//			int nr = twopc_cli_send_txn_result(s, in_remote_ops->src_srv, in_remote_ops->txn_id, rr);
//			if (nr == -1) {
//				return TXN_ABORT;
//			}
////			}
//		}
////		if (rcv_results < remote_requests) {
////			int result_data_count = twopc_pop_result(result_data);
////			if (result_data_count > 0) {
////				rcv_results++;
////				if (result_data->result == TXN_ABORT) {
////					r = TXN_ABORT;
////				}
////				printf("Have received %d results\n", rcv_results);
////				if (rcv_results == remote_requests) {
////					// this means that we have received all the results and we can end the txn
////					if (r == TXN_COMMIT) {
////						printf("Committing locally issued txn %ld\n", result_data->txn_id);
////						r = txn_commit(ctask, hash_table, s, TXN_SINGLE);
////					} else {
////						printf("Aborting locally issued txn %ld\n", result_data->txn_id);
////						txn_abort(ctask, hash_table, s, TXN_SINGLE);
////					}
////					// send the final result to all the remote servers
////					printf("Sending final results\n");
////					twopc_cli_send_final_result(s, result_data->txn_id, r);
////				}
////			}
////		}
//		if (rcv_final_results < rcv_reqs) {
//			int fin_results_count = twopc_pop_fin_result(fin_result_data);
//			if (fin_results_count > 0) {
//				// find the transaction context
//				int txn_found = 0;
//				for (i = 0; i < g_nremote_exec_srv; i++) {
//					if (remote_txn_ctx[i].rtxn_id == fin_result_data->txn_id) {
//						txn_found = 1;
//						ctask->rtxn_ctx = &remote_txn_ctx[i];
//						if (fin_result_data->result == TXN_COMMIT) {
////							printf("Committing remotely issued txn %ld\n", fin_result_data->txn_id);
//							r = txn_commit(ctask, hash_table, s, TXN_SINGLE);
//						} else {
////							printf("Aborting remotely issued txn %ld\n", fin_result_data->txn_id);
//							r = TXN_ABORT;
//							txn_abort(ctask, hash_table, s, TXN_SINGLE);
//						}
//						break;
//					}
//				}
//
////				assert(txn_found > 0);
//				rcv_final_results++;
//			}
//		}
//
//		if (rcv_reqs < g_nremote_exec_srv) {
//			in_rops_count = twopc_pop_remote_ops(in_remote_ops);
//		} else {
//			in_rops_count = 0;
//		}
//
//		if (twopc_finished_srvs_count() == g_nremote_exec_srv) {
//			break;
//		}
//
//		if (p->thread_termination_flag) {
//			break;
//		}
//
//	}
////	printf("Exited the big while loop\n");
//#endif //ENABLE_2PC
//	return r;
//}

struct benchmark ycsb_bench = {
  .init = ycsb_init,
  .load_data = ycsb_load_data,
  .alloc_query = micro_alloc_query,
  .get_next_query = ycsb_get_next_query,
  .run_txn = ycsb_run_txn_remote_support,
	.run_txn_local = ycsb_run_txn_local_worker,
	.run_txn_remote = ycsb_run_txn_remote_worker,
//	.run_remote_txn = ycsb_run_txn_remote_support,
  .verify_txn = NULL,
};
