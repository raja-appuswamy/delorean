
/*
 * master.c
 *
 *  Created on: Nov 28, 2017
 *      Author: anadioti
 */

#include "headers.h"
#include "benchmark.h"
#include "worker.h"
#include "smphashtable.h"
#include "se_dl_detect_graph.h"
#include "partition.h"
#if ENABLE_2PC
#include "comm/twopc.h"
#endif
#if ENABLE_CPU_HOTPLUG
#include "hotplug/hotplug.h"
#include "hotplug_scenarios.h"
#endif


void *hash_table_server(void* args)
{
	int i, r;
	const int s = ((struct thread_args *) args)->id;
	const int c = ((struct thread_args *) args)->core;
	struct hash_table *hash_table = ((struct thread_args *) args)->hash_table;
	struct partition *p = &hash_table->partitions[s];
	void *query;
	__attribute__((unused)) int pct = 10;

	set_affinity(c);
#if defined(MIGRATION)
	fix_affinity(s);
#endif

	double tstart = now();

#if SHARED_EVERYTHING
	/* load only one partition in case of shared everything */
	//if (s == 0)
	g_benchmark->load_data(hash_table, s);

#else

#if ENABLE_ASYMMETRIC_MESSAGING
	/* load only few partitions in case of asym msg. */
	if (s < g_nhot_servers)
		g_benchmark->load_data(hash_table, s);
#else
	/* always load for sn/trireme */
	g_benchmark->load_data(hash_table, s);

#endif //ENABLE_ASYMMETRIC_MESSAGING

#endif //SHARED_EVERYTHING

	double tend = now();

	printf("srv %d load time %.3f\n", s, tend - tstart);
	printf("srv %d rec count: %d partition sz %lu-KB "
			"tx count: %d, per_txn_op cnt: %d\n", s, p->ninserts, p->size / 1024,
			g_niters, g_ops_per_txn);

	pthread_mutex_lock(&hash_table->create_client_lock);

	nready++;

	pthread_mutex_unlock(&hash_table->create_client_lock);

	while (nready != g_active_servers) ;

	printf("srv %d starting txns\n", s);
	fflush(stdout);

	query = g_benchmark->alloc_query();
	assert(query);

	tstart = now();

#if ENABLE_ASYMMETRIC_MESSAGING

#if defined(SHARED_EVERYTHING) || defined(SHARED_NOTHING)
#error "Asymmetric messaging valid only in msgpassing mode\n"
#endif

	if (s >= g_nhot_servers)
		task_libinit(s);
#else
	task_libinit(s);
#endif

	tend = now();

	printf("srv %d query time %.3f\n", s, tend - tstart);
	printf("srv %d total txns %ld \n", s, p->q_idx);
	printf("srv %d commited txns %d aborted %d\n", s, p->ncommits, p->naborts);

	fflush(stdout);

#if ENABLE_ASYMMETRIC_MESSAGING
	if (s < g_nhot_servers)
		p->tps = 0;
	else
		p->tps = p->q_idx / (tend - tstart);
#else
	p->tps = p->q_idx / (p->txn_end_time - tstart);
#endif

	pthread_mutex_lock(&hash_table->create_client_lock);

	nready--;

	pthread_mutex_unlock(&hash_table->create_client_lock);

	while (nready != 0)
#if !defined (SHARED_EVERYTHING) && !defined (SHARED_NOTHING)
		process_requests(hash_table, s);
#else
	;
#endif

	printf("srv %d quitting \n", s);
	fflush(stdout);

	if (g_benchmark->verify_txn)
		g_benchmark->verify_txn(hash_table, s);

	// destory out dses
#if !defined (SHARED_EVERYTHING)
	/* in shared nothing and trireme case, we can safely delete
	 * the partition now as no one else will be directly accessing
	 * data from the partition. In shared everything case, some other
	 * thread might still be doing verification. So we don't destroy now.
	 */
	destroy_hash_partition(p);
#endif

	return NULL;
}

void start_hash_table_servers_hotplug(struct hash_table *hash_table, int hotplugged_servers)
{
	int r;
	void *value;
	nready = 0;

	int old_g_active_servers = g_active_servers;
	g_active_servers += hotplugged_servers;
	assert(g_active_servers <= g_nservers);
	assert(g_active_servers <= NCORES);


	for (int i = old_g_active_servers; i < g_active_servers; i++) {
		hash_table->thread_data[i].id = i;
		hash_table->thread_data[i].core = i;
		hash_table->thread_data[i].hash_table = hash_table;
		printf("Assinging core %d to srv %d\n", hash_table->thread_data[i].core, i);

		r = pthread_create(&hash_table->threads[i], NULL, hash_table_server, (void *) (&hash_table->thread_data[i]));
		assert(r == 0);
	}

	for (int i = 0; i < g_active_servers; i++) {
		r = pthread_join(hash_table->threads[i], &value);
		assert(r == 0);
	}
}


void *hotplug_master(void *args)
{
	struct hash_table *hash_table = (struct hash_table *) args;

	set_affinity(0);

	for (int i = g_startup_servers; i < g_active_servers; i++) {
#if ENABLE_CPU_HOTPLUG
		notify_worker(i, i, WORKER_ACTION_START_TXNS, hash_table);
#else
		notify_worker(i, WORKER_ACTION_START_TXNS, hash_table);
#endif
	}
	g_nservers_to_begin_txns = g_active_servers;


#if ENABLE_CPU_HOTPLUG

//	printf("Currently have %d\n", g_active_servers);
	while (nready != g_active_servers) ;

	usleep(10000000);

#if CONTENTION_SCALING
	virt_contention_scaling_scenario(hash_table);
//	usleep(20000000);
#elif DYNAMIC_SCALING
	run_dynamic_scaling_scenario(hash_table);
#elif SPOT_SCALING
	run_spot_scaling_scenario(hash_table);
#elif SPOT_PADDING
	run_spot_padding_scenario(hash_table);
#elif PEAK_TRAFFIC_SCALING
	run_peak_traffic_scenario(hash_table);
#else
	printf("Running normal execution\n");
	double tps;
	int tps_log_size = 1000000;
	double *tps_log = (double *) calloc(tps_log_size, sizeof(double));
	int tps_cnt = 0;
	int reps = 10;
	if (g_rep_time != 0) {
		reps = g_rep_time;
	}
	for (int i = 0; i < reps; i++) {
		tps = measure_current_throughput(hash_table);
		printf("Throughput is %.9f\n", tps);
		tps_log[tps_cnt++] = tps;
	}
	printf("Time is up; exiting\n");
	hash_table->quitting = 1;

	printf("================= Throughput =================\n");
	for (int i = 0; i < tps_cnt; i++) {
		printf("%.9f\n", tps_log[i]);
	}
	printf("==============================================\n");

#endif

	stop_all_hotplugged_servers(hash_table);

	hotplug_app_exit(g_vm_id);

	printf("Hotplug master thread now exiting...\n");

	printf("== results ==\n");
	printf("Total tps: %0.9fM\n", stats_get_tps(hash_table));
	stats_get_naborts(hash_table);
	stats_get_ncommits(hash_table);
#else
	// periodically start a bunch of servers
	int cnt = g_active_servers;
	while (cnt < g_nservers) {
		usleep(HOTPLUG_WAIT_TIME);
		printf("Total tps until this point: %0.9fM\n", stats_get_tps(hash_table));
		start_worker_hotplug(cnt, hash_table);
		notify_worker(cnt++, WORKER_ACTION_START_TXNS, hash_table);
	}
#endif // ENABLE_CPU_HOTPLUG

	return NULL;
}

void start_hash_table_servers(struct hash_table *hash_table)
{
#if ENABLE_DL_DETECT_CC
	se_dl_detect_init_dependency_graph();
#if !defined(SHARED_EVERYTHING)
#include "twopl.h"
	dl_detect_init_data_structures();
#endif
#endif
#if ENABLE_CPU_HOTPLUG
	hotplug_cpu_reg_master_socket(g_vm_id, g_master_socket);
	init_worker_id_socket_map();
#endif


	int r;
	void *value;
	nready = hash_table->quitting = 0;
	pthread_t cur_thread;

	assert(NCORES >= g_active_servers);

	printf("Active servers = %d\n", g_active_servers);

#if ENABLE_2PC
	pthread_t twopc_thread;
	if (g_nremote_exec_ops > 0) {
		twopc_config_init();
		twopc_util_init();
		twopc_cli_init();

		pthread_create(&twopc_thread, NULL, twopc_daemon_start, (int *) &g_twopc_cur_srv);
		twopc_clients_start(g_twopc_cur_srv);
		while (twopc_connected_srvs_count() != 3 * g_nremote_exec_srv) ;
		printf("Now the connected servers are %d\n", twopc_connected_srvs_count());
	}
#endif

#if ENABLE_CPU_HOTPLUG
	for (int i = 0; i < g_startup_servers; i++) {
		hash_table->partitions[i].rotate_theta = 0;

		start_worker(i, hash_table);
		notify_worker(i, i, WORKER_ACTION_LOAD_DATA, hash_table);
	}
#else
	for (int i = 0; i < g_active_servers; i++) {
		start_worker(i, hash_table);

		// worker load data
		notify_worker(i, WORKER_ACTION_LOAD_DATA, hash_table);
		// worker start transactions
		notify_worker(i, WORKER_ACTION_START_TXNS, hash_table);
	}

#endif

#if ENABLE_CPU_HOTPLUG
#if DYNAMIC_SCALING
	while (lready != 1) ;
	for (int i = 1; i < g_nservers; i++) {
		notify_worker(0, i, WORKER_ACTION_LOAD_DATA, hash_table);
		printf("lready = %d\n", lready);
		while (lready != (i + 1)) ;
	}
#elif VIRTSQLVM
	int sockets_count = g_nsockets;
	int srv;
	int hotplug_srv_idx = 0;
	int mul = g_nht + 1;
	int cur_active_srvs = g_active_servers;
	// hotplug and start all servers from both sockets
	for (int i = 0; i < sockets_count; i++) {
		for (int j = cur_active_srvs; j < mul * CPUS_PER_SOCKET; j++) {
			hotplug_srv_idx = i * mul * CPUS_PER_SOCKET + j;
			printf("Hotpluggin srv %d\n", hotplug_srv_idx);
			hotplug_worker_to_socket(hotplug_srv_idx, i, hash_table, &srv);
			notify_worker(srv, srv, WORKER_ACTION_LOAD_DATA, hash_table);
			hash_table->partitions[hotplug_srv_idx].prev_sum_tps = 0;
			hash_table->partitions[hotplug_srv_idx].prev_time_tps = 0;
		}
		cur_active_srvs = 0;
	}
	while(lready != g_active_servers) ;
#elif CONTENTION_SCALING
	int sockets_count = g_nsockets;
	int srv;
	int hotplug_srv_idx = 0;
	int mul = g_nht + 1;
	int cur_active_srvs = g_active_servers;
	// hotplug and start all servers from both sockets
	for (int i = 0; i < sockets_count; i++) {
		for (int j = cur_active_srvs; j < mul * CPUS_PER_SOCKET; j++) {
			hotplug_srv_idx = i * mul * CPUS_PER_SOCKET + j;
			printf("Hotpluggin srv %d\n", hotplug_srv_idx);
			hotplug_worker_to_socket(hotplug_srv_idx, i, hash_table, &srv);
			notify_worker(srv, srv, WORKER_ACTION_LOAD_DATA, hash_table);
			hash_table->partitions[hotplug_srv_idx].prev_sum_tps = 0;
			hash_table->partitions[hotplug_srv_idx].prev_time_tps = 0;

		}
		cur_active_srvs = 0;
	}
	while(lready != g_active_servers) ;
#elif SPOT_SCALING
	int available_socket_count = 2;
	int target_socket = (g_master_socket + 1) % available_socket_count;
	int srv;
	for (int i = g_active_servers; i < g_nservers; i++) {
		// switch among the sockets while adding srvs
		printf("Hotplugging srv %d to socket %d\n", i, target_socket);
		hotplug_worker_to_socket(i, target_socket, hash_table, &srv);
		notify_worker(srv, srv, WORKER_ACTION_LOAD_DATA, hash_table);
		hash_table->partitions[i].prev_sum_tps = 0;
		hash_table->partitions[i].prev_time_tps = 0;
		target_socket = (target_socket + 1) % available_socket_count;
	}
	while(lready != g_active_servers) ;
#elif SPOT_PADDING
	int sockets_count = 2;
	int srv;
	int hotplug_srv_idx = 0;
	int mul = 1;
	int cur_active_srvs = g_active_servers;
	int target_socket = g_master_socket;
	for (int i = 0; i < sockets_count; i++) {
		for (int j = cur_active_srvs; j < mul * CPUS_PER_SOCKET; j++) {
			hotplug_srv_idx = mul * i * CPUS_PER_SOCKET + j;
			printf("Hotpluggling srv %d\n", hotplug_srv_idx);
			hotplug_worker_to_socket(hotplug_srv_idx, target_socket, hash_table, &srv);
			notify_worker(srv, srv, WORKER_ACTION_LOAD_DATA, hash_table);
			hash_table->partitions[hotplug_srv_idx].prev_sum_tps = 0;
			hash_table->partitions[hotplug_srv_idx].prev_time_tps = 0;
		}
#if SPOT_PADDING_MOTIVATE
		target_socket = (target_socket + 1) % sockets_count;
#else
		if (g_vm_id == 0) {
			target_socket = (target_socket + 1) % sockets_count;
		}
#endif	//SPOT_PADDING_MOTIVATE
		cur_active_srvs = 0;
	}
#elif PEAK_TRAFFIC_SCALING
	g_srv_wait = (volatile int *) calloc(g_nservers, sizeof(volatile int));
	int sockets_count = 1;
	int target_socket = g_master_socket;
	int srv;
	int mul = 1;
	int cur_active_srvs = g_active_servers;
	int cnt = g_active_servers;
	for (int i = 0; i < sockets_count; i++) {
		for (int j = cur_active_srvs; j < mul * CPUS_PER_SOCKET; j++) {
			printf("Hotpluggin srv %d\n", cnt);
			hotplug_worker_to_socket(cnt, i, hash_table, &srv);
			notify_worker(srv, srv, WORKER_ACTION_LOAD_DATA, hash_table);
//			g_nservers_to_begin_txns = g_active_servers;
//			printf("Active servers now are %d\n", g_active_servers);
			hash_table->partitions[cnt].prev_sum_tps = 0;
			hash_table->partitions[cnt].prev_time_tps = 0;
			cnt++;
		}
		cur_active_srvs = 0;
	}
//	for (int i = g_active_servers; i < CPUS_PER_SOCKET; i++) {
//		// switch among the sockets while adding srvs
//		printf("Hotplugging srv %d to socket %d\n", i, target_socket);
//		hotplug_worker_to_socket(i, target_socket, hash_table, &srv);
//		notify_worker(srv, srv, WORKER_ACTION_LOAD_DATA, hash_table);
//		hash_table->partitions[i].prev_sum_tps = 0;
//		hash_table->partitions[i].prev_time_tps = 0;
//		target_socket = (target_socket + 1) % available_socket_count;
//	}
//	while(lready != CPUS_PER_SOCKET) ;
//	for (int i = 0; i < CPUS_PER_SOCKET; i++) {
//		notify_worker(i, i + CPUS_PER_SOCKET, WORKER_ACTION_LOAD_DATA, hash_table);
//	}
	while(lready != g_active_servers) ;
	printf("Loading completed\n");
	// now shutting down workers of the second socket
	if (sockets_count > 1) {
		for (int i = 0; i < CPUS_PER_SOCKET; i++) {
			stop_hotplugged_server_socket(g_master_socket + 1, hash_table);
		}
	}
#else
	int sockets_count = g_nsockets;
	int srv;
	int mul = g_nht + 1;
	int cur_active_srvs = g_active_servers;
	int cnt = g_active_servers;
	// hotplug and start all servers from both sockets
	for (int i = 0; i < sockets_count; i++) {
		for (int j = cur_active_srvs; j < mul * CPUS_PER_SOCKET; j++) {
			printf("Hotpluggin srv %d\n", cnt);
			hotplug_worker_to_socket(cnt, i, hash_table, &srv);
			notify_worker(srv, srv, WORKER_ACTION_LOAD_DATA, hash_table);
//			g_nservers_to_begin_txns = g_active_servers;
//			printf("Active servers now are %d\n", g_active_servers);
			hash_table->partitions[cnt].prev_sum_tps = 0;
			hash_table->partitions[cnt].prev_time_tps = 0;
			cnt++;
		}
		cur_active_srvs = 0;
	}
	while(lready != g_active_servers) ;
#endif

	for (int i = 0; i < g_startup_servers; i++) {
		notify_worker(i, i, WORKER_ACTION_START_TXNS, hash_table);
	}
#endif //ENABLE_CPU_HOTPLUG

	r = pthread_create(&cur_thread, NULL, hotplug_master, (void *) hash_table);
	assert(r == 0);

	/* wait for everybody to start */
	while (nready != g_active_servers) ;

	/* sleep for preconfigured time */
	usleep(RUN_TIME);

#if ENABLE_2PC
	if (g_nremote_exec_ops > 0) {
		for (int i = 0; i < g_nservers; i++) {
			struct partition *p = &hash_table->partitions[i];
			p->thread_termination_flag = 1;
		}
	//  twopc_cli_send_txn_exit();

		volatile int finished_remote_srv = twopc_finished_srvs_count();
		while (finished_remote_srv < g_nremote_exec_srv) {
			finished_remote_srv = twopc_finished_srvs_count();
		}
	}

#endif	// ENABLE_2PC

	hash_table->quitting = 1;

	pthread_join(cur_thread, NULL);

	for (int i = 0; i < g_active_servers; i++) {
		stop_worker(i, hash_table);
	}

#if ENABLE_2PC
	if (g_nremote_exec_ops > 0) {
		pthread_join(twopc_thread, NULL);
	}
	printf("MAX_REMOTE_IN_REQS_PER_SRV = %d\n", MAX_REMOTE_IN_REQS_PER_SRV);
	printf("MAX_REMOTE_OUT_REQS_PER_SRV = %d\n", MAX_REMOTE_OUT_REQS_PER_SRV);
#endif
}
