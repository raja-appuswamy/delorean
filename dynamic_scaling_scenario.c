/*
 * dynamic_scaling_scenario.c
 *
 *  Created on: 9 Oct 2018
 *      Author: anadioti
 */
#if ENABLE_CPU_HOTPLUG

#include "headers.h"
#include "worker.h"

void run_dynamic_scaling_scenario(struct hash_table *hash_table) {
	printf("================= Dynamic Scaling =================\n");
	int socket_id = 0;
	int cpus_upd_count = 1;
#if DIASCLD33
	cpus_upd_count = 4;
#elif DIASSRV8
	cpus_upd_count = 5;
#endif
	int srv;
	for (int i = g_startup_servers; i < cpus_upd_count; i++) {
		printf("Hotplugging %d\n", i);
		hotplug_worker_to_socket(i, 0, hash_table, &srv);
		notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
		g_nservers_to_begin_txns = g_active_servers;
//		while (nready != g_active_servers) ;
		printf("Active servers now are %d and nready %d\n", g_active_servers, nready);
		hash_table->partitions[i].prev_sum_tps = 0;
		hash_table->partitions[i].prev_time_tps = 0;
	}
	int cnt = g_active_servers;
	int startup_servers = g_active_servers;

	int tps_log_size = 1000000;
	double *tps_log = (double *) calloc(tps_log_size, sizeof(double));
	int tps_cnt = 0;
	double tps;

#if DYNAMIC_SCALING
		usleep(5000000);
#endif
		while (cnt < CPUS_PER_SOCKET * SOCKET_COUNT) {
#if DIASCLD33
			if (cpus_upd_count == 5) {
				cpus_upd_count = 4;
			} else {
				cpus_upd_count = 5;
			}
#endif
//			usleep(DS_WAIT_TIME);
			for (int i = 0; i < 5; i++) {
				usleep(1000000);
				tps = measure_current_throughput(hash_table);
				printf("Throughput is %.9f\n", tps);
				tps_log[tps_cnt++] = tps;
			}
			for (int i = 0; i < cpus_upd_count; i++) {
				if (cnt % CPUS_PER_SOCKET == 0) {
					socket_id++;
				}
				hotplug_worker_to_socket(cnt, socket_id, hash_table, &srv);
				notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
				g_nservers_to_begin_txns = g_active_servers;
//				while (nready != g_active_servers) ;
				printf("Active servers now are %d\n", g_active_servers);
				hash_table->partitions[cnt].prev_sum_tps = 0;
				hash_table->partitions[cnt].prev_time_tps = 0;
				cnt++;
			}
		}
		cnt--;
		while (cnt >= startup_servers) {
//			usleep(DS_WAIT_TIME);
			for (int i = 0; i < 5; i++) {
				usleep(1000000);
				tps = measure_current_throughput(hash_table);
				tps_log[tps_cnt++] = tps;
				printf("Throughput is %.9f\n", tps);
			}
			for (int i = 0; i < cpus_upd_count; i++) {
				printf("Hotremoving worker %d from socket %d\n", cnt, socket_id);
				stop_hotplugged_server_socket(socket_id, hash_table);
				if (cnt % CPUS_PER_SOCKET == 0) {
					socket_id--;
				}
				cnt--;
			}
#if DIASCLD33
			if (cpus_upd_count == 5) {
				cpus_upd_count = 4;
			} else {
				cpus_upd_count = 5;
			}
#endif
		}
		for (int i = 0; i < 5; i++) {
			usleep(1000000);
			tps = measure_current_throughput(hash_table);
			tps_log[tps_cnt++] = tps;
			printf("Throughput is %.9f\n", tps);
		}

		for (int i = 0; i < 3; i++) {
			stop_hotplugged_server_socket(0, hash_table);
		}

		printf("================= Throughput =================\n");
		for (int i = 0; i < tps_cnt; i++) {
			printf("%.9f\n", tps_log[i]);
		}
		printf("==============================================\n");
		hash_table->quitting = 1;
}

#endif

