/*
 * spot_padding_scenario.c
 *
 *  Created on: 10 Oct 2018
 *      Author: anadioti
 */
#if ENABLE_CPU_HOTPLUG

#include "headers.h"
#include "worker.h"
#include "hotplug/hotplug.h"

#define VM_MASK 0x0FFFFFFFFFFFFFFF

#define PREAMBLE_MASK	0xF000000000000000
#define CONTENT_MASK 	0x0FFFFFFF00000000
#define VM_ID_MASK		0x00000000FFFFFFFF
#define NUM_CORES_MASK	0x0FFFFFFFFFFFFFFF

#define GET_PREAMBLE(msg)	((msg & PREAMBLE_MASK) >> 60)
#define GET_CONTENT(msg)	((msg & CONTENT_MASK) >> 32)
#define GET_VM_ID(msg)		(msg & VM_ID_MASK)


void pad_shrink_machine(struct hash_table *hash_table, int target_thread_count, int socket_id) {
	printf("================= Shrinking VM =================\n");
//	int target_socket = g_master_socket;
	// first stop the servers
	int currently_active_servers = g_active_servers;
	int cnt = 0;
	while (cnt < target_thread_count) {
		stop_hotplugged_server_socket(socket_id, hash_table);
		cnt++;
	}
}

void pad_expand_machine(struct hash_table *hash_table, int target_thread_count, int socket_id) {
	printf("================= Expanding VM =================\n");
//	int target_socket = (g_master_socket + 1) % 2;
	int currently_active_servers = g_active_servers;
	int cnt = currently_active_servers;
	int srv;
	while ((cnt < currently_active_servers + target_thread_count) && (cnt < g_nservers)) {
		hotplug_worker_to_socket(cnt, socket_id, hash_table, &srv);
//		printf("Expanding with worker %d\n", srv);
		g_nservers_to_begin_txns = g_active_servers;
		notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
//		printf("Waiting for nready (%d) == g_active_servers (%d)\n", nready, g_active_servers);
		while (nready != g_active_servers) ;
		hash_table->partitions[srv].prev_sum_tps = 0;
		hash_table->partitions[srv].prev_time_tps = 0;
		cnt++;
	}
}


void run_spot_padding_scenario(struct hash_table *hash_table) {
	printf("Running spot instance padding scenario\n");
	double tps;
	int tps_log_size = 1000000;
	double *tps_log = (double *) calloc(tps_log_size, sizeof(double));
	int tps_cnt = 0;
	int reps = 80;
	int srv;
	if (g_rep_time != 0) {
		reps = g_rep_time;
	}

#if SPOT_PADDING_MOTIVATE
	for (int i = 0; i < reps && !hash_table->quitting; i++) {
		usleep(1000000);
		tps = measure_current_throughput(hash_table);
		printf("Throughput is %.9f\n", tps);
		tps_log[tps_cnt++] = tps;
	}
#else
	if (g_vm_id == 0) {
		for (int i = 0; i < reps && !hash_table->quitting; i++) {
			usleep(1000000);
			tps = measure_current_throughput(hash_table);
			printf("Throughput is %.9f\n", tps);
			tps_log[tps_cnt++] = tps;

			// after 20 seconds remove threads from second socket and use only the first
			if (i == 20) {
				int socket_id = (g_master_socket + 1) % 2;
				for (int j = 0; j < CPUS_PER_SOCKET; j++) {
					printf("Hotremoving worker from socket %d\n", socket_id);
					stop_hotplugged_server_socket(socket_id, hash_table);
				}
				usleep(1000000);
				int cnt = g_active_servers;
				for (int j = 0; j < CPUS_PER_SOCKET; j++) {
					printf("Hotplugging worker %d to socket %d\n", cnt, g_master_socket);
					hotplug_worker_to_socket(cnt, g_master_socket, hash_table, &srv);
					cnt++;
					notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
					g_nservers_to_begin_txns = g_active_servers;
				}

			}

			// leave it running for 100 more seconds and then expand again
			if (i == 200) {
				int socket_id = (g_master_socket + 1) % 2;
				for (int j = 0; j < CPUS_PER_SOCKET; j++) {
					printf("Hotremoving worker from socket %d\n", g_master_socket);
					stop_hotplugged_server_socket(g_master_socket, hash_table);
				}
				usleep(1000000);
				int cnt = g_active_servers;
				for (int j = 0; j < CPUS_PER_SOCKET; j++) {
					printf("Hotplugging worker %d to socket %d\n", cnt, socket_id);
					hotplug_worker_to_socket(cnt, socket_id, hash_table, &srv);
					cnt++;
					notify_worker(srv, srv, WORKER_ACTION_START_TXNS, hash_table);
					g_nservers_to_begin_txns = g_active_servers;
				}
			}
		}
	} else {
		for (int i = 0; i < reps && !hash_table->quitting; i++) {
			usleep(1000000);
			tps = measure_current_throughput(hash_table);
			printf("Throughput is %.9f\n", tps);
			tps_log[tps_cnt++] = tps;
		}
	}
#endif

	printf("Time is up; exiting\n");
	hash_table->quitting = 1;

	printf("================= Throughput =================\n");
	for (int i = 0; i < tps_cnt; i++) {
		printf("%.9f\n", tps_log[i]);
	}
	printf("==============================================\n");
	hash_table->quitting = 1;
}

#endif
