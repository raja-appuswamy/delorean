/*
 * hotplug_scenarios.h
 *
 *  Created on: 9 Oct 2018
 *      Author: anadioti
 */

#ifndef HOTPLUG_SCENARIOS_H_
#define HOTPLUG_SCENARIOS_H_

void virt_contention_scaling_scenario(struct hash_table *hash_table);
void run_dynamic_scaling_scenario(struct hash_table *hash_table);
void run_spot_scaling_scenario(struct hash_table *hash_table);
void run_spot_padding_scenario(struct hash_table *hash_table);
void run_peak_traffic_scenario(struct hash_table *hash_table);

#endif /* HOTPLUG_SCENARIOS_H_ */
