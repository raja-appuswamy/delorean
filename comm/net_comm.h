#if 0
/*
 * net_comm.h
 *
 *  Created on: 20 Aug 2018
 *      Author: anadioti
 */

#ifndef COMM_NET_COMM_H_
#define COMM_NET_COMM_H_

#include "headers.h"

#define COMM_PORT 60000

struct comm_msg {
	int src_srv;
	uint64_t txn_id;
	int ops_cnt;	// -1: PREPARE for 2PC
	struct hash_op ops[g_ops_per_txn];
};


struct rtxn_op {
	uint64_t txn_id;
	struct hash_op *ops;
	uint8_t ops_cnt;
	uint8_t res;
	int client_sock;
};

void remote_ops_q_init();
void remote_ops_q_insert(const uint64_t *txn_id, const struct hash_op *ops,
		const uint8_t *ops_cnt, const int *client_socket);
void remote_ops_q_pop(struct rtxn_op *remote_op);
void remote_ops_q_get(const uint64_t *txn_id, struct hash_op *ops, void *pointer_hint);
void remote_ops_q_upd(const uint64_t *txn_id, const uint8_t *res, const void *pointer_hint);
void remote_ops_q_2pc_reply(const uint64_t *txn_id, const void *pointer_hint);
void remote_ops_q_rmv(const uint64_t *txn_id, const void *pointer_hint);

void two_pc_reply(const int *client_socket, const int *r);

void register_remote_server(int srv, const char *ip_address, const short *port);
void insert_remote_op(int src, int trg, uint64_t txn_id, struct hash_op *hash_op);
int flush_remote_ops(int src);
int send_remote_ops(uint64_t txn_id, int nops, const struct hash_op *hash_ops, int srv);

#endif /* COMM_NET_COMM_H_ */
#endif	//0
