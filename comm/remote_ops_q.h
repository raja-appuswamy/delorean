#if 0

/*
 * remote_ops_q.h
 *
 *  Created on: 19 Sep 2018
 *      Author: anadioti
 */

#ifndef COMM_REMOTE_OPS_Q_H_
#define COMM_REMOTE_OPS_Q_H_

#include "../headers.h"

struct rtxn_op {
	uint64_t txn_id;
	struct hash_op *ops;
	uint8_t ops_cnt;
	uint8_t res;
	int remote_client;
};

void remote_ops_q_init();
void remote_ops_q_insert(const uint64_t *txn_id, const struct hash_op *ops,
		const uint8_t *ops_cnt, const int *client_socket);
void remote_ops_q_pop(struct rtxn_op *remote_op);
/*
void remote_ops_q_get(const uint64_t *txn_id, struct hash_op *ops, void *pointer_hint);
void remote_ops_q_upd(const uint64_t *txn_id, const uint8_t *res, const void *pointer_hint);
void remote_ops_q_2pc_reply(const uint64_t *txn_id, const void *pointer_hint);
void remote_ops_q_rmv(const uint64_t *txn_id, const void *pointer_hint);
void two_pc_reply(const int *client_socket, const int *r);
*/

#endif /* COMM_REMOTE_OPS_Q_H_ */
#endif	//0
