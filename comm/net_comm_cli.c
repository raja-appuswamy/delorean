#if 0
/*
 * net_comm_cli.c
 *
 *  Created on: 11 Sep 2018
 *      Author: anadioti
 */

#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>

#include "net_comm.h"
#include "../glo.h"

// data structure holding information about remote servers
struct remote_srv {
	char ip_addr[16];
	short port;
};

// associative map holding mappings between target servers and their IP addresses and ports
struct remote_srv remote_servers[g_nremote_exec_srv];

struct comm_msg comm_msg_buffer[g_nremote_exec_srv][g_nremote_exec_srv];

void register_remote_server(int srv, const char *ip_address, const short *port) {
	strcpy(remote_servers[srv].ip_addr, ip_address);
	remote_servers[srv].port = *port;
}

void insert_remote_op(int src, int trg, uint64_t txn_id, struct hash_op *hash_op) {
	struct comm_msg *msg = &comm_msg_buffer[src][trg];
	memcpy(msg->ops[msg->ops_cnt], *hash_op, sizeof(struct hash_op));
	msg->ops_cnt++;
	msg->src_srv = src;
	msg->txn_id = txn_id;
}

int flush_remote_ops(int src) {
	int t;
	for (t = 0; t < g_nremote_exec_srv; t++) {
		struct comm_msg *msg = &comm_msg_buffer[src][t];
		if (msg->ops_cnt > 0) {
			struct sockaddr_in cli_addr, srv_addr;
			int sock = 0;

			if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
				printf("Error creating socket\n");
				return -1;
			}

			memset(&srv_addr, '0', sizeof(struct sockaddr_in));

			srv_addr.sin_family = AF_INET;
			srv_addr.sin_port = htons(COMM_PORT);

			if (inet_pton(AF_INET, remote_servers[t].ip_addr, &srv_addr.sin_addr) <= 0) {
				printf("Address %s not supported\n", remote_servers[t].ip_addr);
				return -1;
			}

			if (connect(sock, (struct sockaddr *)&srv_addr, sizeof(struct sockaddr)) < 0) {
				printf("Connection to server %s failed \n", remote_servers[t].ip_addr);
				return -1;
			}

			send(sock, &msg, sizeof(struct comm_msg), 0);
			msg->ops_cnt = 0;
		}
	}

	return 0;
}

int send_remote_ops(uint64_t txn_id, int nops, const struct hash_op *hash_ops, int srv) {
	char *dst_srv_ip_addr = remote_servers[srv].ip_addr;
	// first build the msg to send to the other side
	struct comm_msg msg;
	memcpy(msg.ops, hash_ops, nops * sizeof(struct hash_op));
	msg.ops_cnt = nops;
	msg.txn_id = txn_id;

	// then establish the connection
	struct sockaddr_in cli_addr, srv_addr;
	int sock = 0;

	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Error creating socket\n");
		return -1;
	}

	memset(&srv_addr, '0', sizeof(struct sockaddr_in));

	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(COMM_PORT);

	if (inet_pton(AF_INET, dst_srv_ip_addr, &srv_addr.sin_addr) <= 0) {
		printf("Address %s not supported\n", dst_srv_ip_addr);
		return -1;
	}

	if (connect(sock, (struct sockaddr *)&srv_addr, sizeof(struct sockaddr)) < 0) {
		printf("Connection to server %s failed \n", dst_srv_ip_addr);
		return -1;
	}

	send(sock, &msg, sizeof(struct comm_msg), 0);

	return 0;
}
#endif	//0



