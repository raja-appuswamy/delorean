#if 0
/*
 * remote_ops_q.c
 *
 *  Created on: 22 Aug 2018
 *      Author: anadioti
 */

#include "headers.h"
#include "twopc.h"
#include <sys/queue.h>
#include <stdint.h>

LIST_HEAD(remote_txn_op, remote_txn_op_entry) remoteq_head =
		LIST_HEAD_INITIALIZER(remoteq_head);
struct remote_txn_op_entry {
	struct rtxn_op rop;
	LIST_ENTRY(remote_txn_op_entry) remote_entries;
};

pthread_mutex_t qmtx;

void remote_ops_q_init() {
	pthread_mutex_int(&qmtx, NULL);
	LIST_INIT(&remoteq_head);
}

void remote_ops_q_insert(const uint64_t *txn_id, const struct hash_op *ops,
		const uint8_t *ops_cnt, const int *client_socket) {
	struct remote_txn_op_entry *entry = (struct remote_txn_op_entry *)
			malloc(sizeof (struct remote_txn_op_entry));
	entry->rop.ops_cnt = *ops_cnt;
	entry->rop.ops = (struct hash_op *) calloc(*ops_cnt, sizeof (struct hash_op));
	memcpy(entry->rop.ops, ops, entry->rop.ops_cnt * sizeof(struct hash_op));
	entry->rop.txn_id = txn_id;
	entry->rop.remote_client = *client_socket;
	pthread_mutex_lock(&qmtx);
	LIST_INSERT_HEAD(&remoteq_head, entry, remote_entries);
	pthread_mutex_unlock(&qmtx);
}

void remote_ops_q_pop(struct rtxn_op *remote_op) {
	pthread_mutex_lock(&qmtx);
	struct remote_txn_op_entry *entry = LIST_FIRST(&remoteq_head);
	if (entry != NULL) {
		memcpy(remote_op, &entry->rop, sizeof(struct txn_op));
		LIST_REMOVE(entry, remote_entries);
		free(entry);
	}
	pthread_mutex_unlock(&qmtx);
}
/*
void remote_ops_q_get(const uint64_t *txn_id, struct hash_op *ops, void *pointer_hint) {
	struct remote_txn_op_entry *entry;
	pthread_mutex_lock(&qmtx);
	LIST_FOREACH(entry, &remoteq_head, remote_entries) {
		if (entry->rop.txn_id == *txn_id) {
			ops = entry->rop.ops;
			pointer_hint = (void *) entry;
			break;
		}
	}
	pthread_mutex_unlock(&qmtx);
}

void remote_ops_q_upd(const uint64_t *txn_id, const uint8_t *res, const void *pointer_hint) {
	struct remote_txn_op_entry *entry = (struct remote_txn_op_entry *) pointer_hint;
	pthread_mutex_lock(&qmtx);
	if (entry->rop.txn_id == *txn_id) {
		entry->rop.res = res;
	} else {
		LIST_FOREACH(entry, &remoteq_head, remote_entries) {
			if (entry->rop.txn_id == *txn_id) {
				entry->rop.res = res;
				break;
			}
		}
	}
	pthread_mutex_unlock(&qmtx);
}

void remote_ops_q_2pc_reply(const uint64_t *txn_id, const void *pointer_hint) {
	struct remote_txn_op_entry *entry = (struct remote_txn_op_entry *) pointer_hint;
	pthread_mutex_lock(&qmtx);
	if (entry->rop.txn_id == *txn_id) {
		send(entry->rop.client_sock, &entry->rop.res, sizeof(uint8_t), 0);
		break;
	} else {
		LIST_FOREACH(entry, &remoteq_head, remote_entries) {
			if (entry->rop.txn_id == *txn_id) {
				send(entry->rop.client_sock, &entry->rop.res, sizeof(uint8_t), 0);
				break;
			}
		}
	}
	pthread_mutex_unlock(&qmtx);
}

void remote_ops_q_rmv(const uint64_t *txn_id, const void *pointer_hint) {
	struct remote_txn_op_entry *entry = (struct remote_txn_op_entry *) pointer_hint;
	pthread_mutex_lock(&qmtx);
	if (entry->rop.txn_id == *txn_id) {
		free(entry->rop.ops);
		LIST_REMOVE(entry, remote_entries);
		free(entry);
	} else {
		entry = LIST_FIRST(&remoteq_head);
		while (entry != NULL) {
			if (entry->rop.txn_id == *txn_id) {
				free(entry->rop.ops);
				LIST_REMOVE(entry, remote_entries);
				free(entry);
				break;
			}
			entry = LIST_NEXT(entry, remote_entries);
		}
	}
	pthread_mutex_unlock(&qmtx);
}

void two_pc_reply(const int *client_socket, const int *r) {
	send(*client_socket, r, sizeof(int), 0);
}
*/
#endif	//0
