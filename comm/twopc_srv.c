/*
 * twopc_srv.c
 *
 *  Created on: 17 Sep 2018
 *      Author: anadioti
 */

#include "../headers.h"
#include "twopc.h"
#include "remote_ops_q.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>

//int client_sock_map[g_nremote_exec_srv];
ssize_t max_msg_size;

//void *twopc_process_txn_requests(void *args) {
//	struct registration_data *reg_data = (struct registration_data *) args;
//	// send back an ACK
//	int resp = 1;
//	send(reg_data->cli_socket, &resp, sizeof(resp), 0);
//	// now wait to receive remote txn ops
//	struct comm_msg in;
//	while (!hash_table->quitting) {
//	ssize_t bytes_recv = recv(reg_data->cli_socket, &in, sizeof(struct comm_msg), 0);
//		if (bytes_recv != -1) {
//			if (in.ops_cnt != -1) {
//				remote_ops_q_insert(&in.txn_id, in.ops, &in.ops_cnt, &reg_data->src);
//			} else {
//				/* Do nothing for now; we assume that all ops
//				* are sent together with the PREPARE msg */
//			}
//		}
//	}
//	return NULL;
//}

void twopc_max_msg_size() {
	ssize_t max = 0;
	if (max < sizeof(struct registration_data)) {
		max = sizeof(struct registration_data);
	}
	if (max < sizeof(struct twopc_txn_ops)) {
		max = sizeof(struct twopc_txn_ops);
	}
	if (max < sizeof(struct txn_result_data)) {
		max = sizeof(struct txn_result_data);
	}

	max_msg_size = max;
}


//void twopc_send_txn_result(uint64_t txn_id, int res, int remote_client) {
//	struct twopc_msg msg;
//	msg.twopc_code = TWOPC_OPS_RESULT;
//	msg.body = malloc(sizeof(struct txn_result_data));
//	struct txn_result_data *result_data = (struct txn_result_data *) msg.body;
//	result_data->result = res;
//	result_data->txn_id = txn_id;
//	send(client_sock_map[remote_client], &msg, sizeof(msg), 0);
//}

int twopc_srv_handle_incoming(int *accept_fd, struct twopc_msg *msg) {
//	struct twopc_msg *msg = (struct twopc_msg *) malloc(sizeof(*in_msg));
//	memcpy(msg, in_msg, sizeof(*in_msg));
//	printf("Received msg from remote client with opcode %d\n", msg->twopc_code);
	int ret = 0;
	switch (msg->twopc_code) {
	case TWOPC_OPS:
	{
		struct twopc_txn_ops *txn_ops = (struct twopc_txn_ops *) msg->body;
		if (txn_ops->ops_cnt > 0) {
			twopc_push_remote_ops(txn_ops);
		}
		break;
	}
	case TWOPC_OPS_RESULT:
	{
//		printf("Received result\n");
//		twopc_push_result((struct txn_result_data *) msg->body);
		twopc_upd_result_locally_issued_txn_ctx((struct txn_result_data *) msg->body);
		break;
	}
	case TWOPC_REGISTER:
	{
		struct registration_data *reg_data = (struct registration_data *) msg->body;
		printf("Srv %d received TWOPC_REGISTER from %d\n", g_twopc_cur_srv, reg_data->src);
		assert(0);
//		printf("Msg came from %d\n", reg_data->src);
//		twopc_register_remote_client(reg_data);
		break;
	}
	case TWOPC_REGISTER_OPS:
	{
		struct registration_data *reg_data = (struct registration_data *) msg->body;
		printf("Srv %d received TWOPC_REGISTER_OPS from %d\n", g_twopc_cur_srv, reg_data->src);
//		printf("Msg came from %d\n", reg_data->src);
		twopc_register_remote_client_ops(reg_data);
		break;
	}
	case TWOPC_REGISTER_RES:
	{
		struct registration_data *reg_data = (struct registration_data *) msg->body;
		printf("Srv %d received TWOPC_REGISTER_RES from %d\n", g_twopc_cur_srv, reg_data->src);
//		printf("Msg came from %d\n", reg_data->src);
		twopc_register_remote_client_res(reg_data);
		break;
	}
	case TWOPC_REGISTER_FINAL_RES:
	{
		struct registration_data *reg_data = (struct registration_data *) msg->body;
		printf("Srv %d received TWOPC_REGISTER_FINAL_RES from %d\n", g_twopc_cur_srv, reg_data->src);
//		printf("Msg came from %d\n", reg_data->src);
		twopc_register_remote_client_final_res(reg_data);
		break;
	}
	case TWOPC_REGISTER_ACK:
	{
		struct registration_data *reg_data = (struct registration_data *) msg->body;
		printf("Srv %d received TWOPC_REGISTER_ACK from %d\n", g_twopc_cur_srv, reg_data->src);
		reg_data->cli_socket = *accept_fd;
		twopc_ack_rcv(reg_data->src);
		break;
	}
	case TWOPC_FINAL_RESULT:
	{
//		printf("Received final result\n");
//		twopc_push_fin_result((struct txn_result_data *) msg->body);
		twopc_upd_result_remotely_issued_txn_ctx((struct txn_result_data *) msg->body);
		break;
	}
	case TWOPC_EXIT:
	{
		printf("Received exit message\n");
		ret = 1;
		struct twopc_exit_data *exit_data = (struct twopc_exit_data *) msg->body;
		twopc_exit_rcv(exit_data->src);
		break;
	}
	default:
		printf("SRV could not identify msg code %d\n", msg->twopc_code);
	}

	return ret;
}

//void twopc_srv_handle_incoming(int *accept_fd, struct msghdr *msg) {
////	struct twopc_msg *msg = (struct twopc_msg *) malloc(sizeof(*in_msg));
////	memcpy(msg, in_msg, sizeof(*in_msg));
////	printf("Received msg from remote client with opcode %d\n", msg->twopc_code);
//	enum twopc_proto_codes code = *((enum twopc_proto_codes *)msg->msg_name);
//	switch (code) {
//	case TWOPC_OPS:
//	{
////		struct twopc_txn_ops *txn_ops = (struct twopc_txn_ops *) msg->body;
////		if (txn_ops->ops_cnt > 0) {
////			twopc_push_remote_ops(txn_ops);
////		}
//		break;
//	}
//	case TWOPC_OPS_RESULT:
////		twopc_push_result((struct txn_result_data *) msg->body);
//		break;
//	case TWOPC_REGISTER:
//	{
//		struct registration_data *reg_data = (struct registration_data *) msg->msg_iov->iov_base;
//		printf("Srv %d received TWOPC_REGISTER from %d\n", g_twopc_cur_srv, reg_data->src);
////		printf("Msg came from %d\n", reg_data->src);
//		twopc_register_remote_client(reg_data);
//		break;
//	}
//	case TWOPC_REGISTER_ACK:
//	{
//		struct registration_data *reg_data = (struct registration_data *) msg->msg_iov->iov_base;
//		printf("Srv %d received TWOPC_REGISTER_ACK from %d\n", g_twopc_cur_srv, reg_data->src);
//		reg_data->cli_socket = *accept_fd;
//		twopc_ack_rcv(reg_data->src);
//		break;
//	}
//	case TWOPC_FINAL_RESULT:
//		twopc_push_fin_result((struct txn_result_data *) msg->msg_iov);
//		break;
//	default:
//		printf("SRV could not identify msg code %d\n", *(int *) msg->msg_name);
//	}
//}


void *twopc_process_txn_requests(void *args) {
	int r, r_sum = 0;
	int accept_fd = *(int *) args;
	ssize_t txn_ops_msg_size = twopc_txn_ops_struct_size(); //sizeof(int) + sizeof(uint64_t) + sizeof(int) + g_ops_per_txn * sizeof(struct hash_op);
	ssize_t msg_size = 256;//sizeof(struct twopc_msg) + txn_ops_msg_size;
	struct twopc_msg *msg = (struct twopc_msg *) malloc(msg_size);
	printf("Server waiting for data on socket %d\n", accept_fd);

	while (!hash_table->quitting) {
		memset(msg, 0, msg_size);
		ssize_t accept_msg_size;
		ssize_t bytes_received = recv(accept_fd , &accept_msg_size, sizeof(ssize_t), 0);
		bytes_received = recv(accept_fd , msg, accept_msg_size, 0);
//		printf("Received %ld bytes with accept msg size %ld max msg size %ld\n", bytes_received, accept_msg_size, msg_size);
//		ssize_t bytes_received = recvmsg(accept_fd, &msg, 0);
		r = twopc_srv_handle_incoming(&accept_fd, msg);
		r_sum += r;
		if (r_sum == g_nremote_exec_srv) {
			break;
		}
	}
	printf("Remote txn listener exiting...\n");
	close(accept_fd);
	return NULL;
}

int twopc_server_daemon(short port) {
//	twopc_max_msg_size();
	// we have one receiver thread per remote server
	pthread_t *twopc_receivers = (pthread_t *) calloc(g_nremote_exec_srv * 3, sizeof(pthread_t));
	int thread_cnt = 0;
	int srv_fd, accept_fd, readval;
	int sockopt = 1;

	struct sockaddr_in addr;
	int addrlen = sizeof(addr);

	if ((srv_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket");
		return -1;
	}

	if (setsockopt(srv_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &sockopt, sizeof(sockopt))) {
		perror("setsockopt");
		return -1;
	}

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);

	if (bind(srv_fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		perror("bind");
		return -1;
	}

	if (listen(srv_fd, g_nremote_exec_srv) < 0) {
		perror("listen");
		return -1;
	}

	printf("2PC Server listening for remote connections on socket %d\n", srv_fd);

	int accepted_fd_arr[3 * g_nremote_exec_srv];
	while ((!hash_table->quitting) && (thread_cnt < 3 * g_nremote_exec_srv)) {
		if ((accept_fd = accept(srv_fd, (struct sockaddr *) &addr, (socklen_t *) &addrlen)) < 0) {
			perror("accept");
			return -1;
		}
		accepted_fd_arr[thread_cnt] = accept_fd;
		printf("2PC Server: Accepted remote connection with fd %d\n", accept_fd);
//		struct twopc_msg inc_msg;
//
//		readval = recv(accept_fd , &inc_msg, sizeof(inc_msg), 0);
//		assert(inc_msg.twopc_code == TWOPC_REGISTER);
//		struct registration_data *reg_data = (struct registration_data *) inc_msg.body;
//		reg_data->cli_socket = accept_fd;
//		client_sock_map[reg_data->src] = accept_fd;
//		pthread_create(&twopc_receivers[thread_cnt], NULL, twopc_process_txn_requests, (void *) &reg_data);
		pthread_create(&twopc_receivers[thread_cnt], NULL, twopc_process_txn_requests, (void *) &accepted_fd_arr[thread_cnt]);
		thread_cnt++;
	}

//	close(srv_fd);

	int i;
	for (i = 0; i < thread_cnt; i++) {
		pthread_join(twopc_receivers[i], NULL);
	}

//	for (i = 0; i < thread_cnt; i++) {
//		close(client_sock_map[i]);
//	}

	close(srv_fd);

	printf("2PC server now exiting...\n");

	return 0;
}


