/*
 * twopc_cli.c
 *
 *  Created on: 17 Sep 2018
 *      Author: anadioti
 */

#include "../headers.h"
#include "twopc.h"
#include "net_comm.h"

#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

volatile int *server_sock_map;	//[g_nremote_exec_srv];
volatile int *server_ops_sock_map;
volatile int *server_res_sock_map;
volatile int *server_final_res_sock_map;
struct twopc_msg **comm_msg_buffer;	//[g_nremote_exec_srv][g_nremote_exec_srv];
pthread_mutex_t *snd_mtx;
pthread_mutex_t *snd_ops_mtx;
pthread_mutex_t *snd_res_mtx;
pthread_spinlock_t *ssnd_res_mtx;
pthread_mutex_t *snd_final_res_mtx;

struct twopc_msg **txn_result_msg;
struct twopc_msg **txn_final_result_msg;

void twopc_cli_init() {
	ssize_t txn_ops_msg_size = twopc_txn_ops_struct_size(); //sizeof(int) + sizeof(uint64_t) + sizeof(int) + g_ops_per_txn * sizeof(struct hash_op);
	ssize_t msg_size = sizeof(struct twopc_msg) + txn_ops_msg_size;
	server_sock_map = (volatile int *) calloc(g_nremote_exec_srv + 1, sizeof(volatile int));
	server_ops_sock_map = (volatile int *) calloc(g_nremote_exec_srv + 1, sizeof(volatile int));
	server_res_sock_map = (volatile int *) calloc(g_nremote_exec_srv + 1, sizeof(volatile int));
	server_final_res_sock_map = (volatile int *) calloc(g_nremote_exec_srv + 1, sizeof(volatile int));
	comm_msg_buffer = (struct twopc_msg **) calloc(g_nservers, sizeof(struct twopc_msg *));
	snd_mtx = (pthread_mutex_t *) calloc(g_nremote_exec_srv + 1, sizeof(pthread_mutex_t));
	snd_ops_mtx = (pthread_mutex_t *) calloc(g_nremote_exec_srv + 1, sizeof(pthread_mutex_t));
	snd_res_mtx = (pthread_mutex_t *) calloc(g_nremote_exec_srv + 1, sizeof(pthread_mutex_t));
	ssnd_res_mtx = (pthread_spinlock_t *) calloc(g_nremote_exec_srv + 1, sizeof(pthread_spinlock_t));
	snd_final_res_mtx = (pthread_mutex_t *) calloc(g_nremote_exec_srv + 1, sizeof(pthread_mutex_t));
	ssize_t txn_result_msg_size = sizeof(struct twopc_msg) + sizeof(struct txn_result_data);
	txn_result_msg = (struct twopc_msg **) calloc(g_nservers, sizeof(struct twopc_msg *));
	txn_final_result_msg = (struct twopc_msg **) calloc(g_nservers, sizeof(struct twopc_msg *));

	int i, j;
	for (i = 0; i < g_nremote_exec_srv + 1; i++) {
		pthread_mutex_init(&snd_mtx[i], NULL);
		pthread_mutex_init(&snd_ops_mtx[i], NULL);
		pthread_mutex_init(&snd_res_mtx[i], NULL);
		pthread_mutex_init(&snd_final_res_mtx[i], NULL);
		pthread_spin_init(&ssnd_res_mtx[i], PTHREAD_PROCESS_PRIVATE);
		server_sock_map[i] = -1;
		server_ops_sock_map[i] = -1;
		server_res_sock_map[i] = -1;
		server_final_res_sock_map[i] = -1;
	}

	for (i = 0; i < g_nservers; i++) {
		txn_result_msg[i] = (struct twopc_msg *) malloc(txn_result_msg_size);
		txn_final_result_msg[i] = (struct twopc_msg *) malloc(txn_result_msg_size);
		comm_msg_buffer[i] = (struct twopc_msg *) calloc(g_nremote_exec_srv + 1, msg_size);
		for (j = 0; j < g_nremote_exec_srv + 1; j++) {
			comm_msg_buffer[i][j].twopc_code = TWOPC_OPS;
			struct twopc_msg *msg = (struct twopc_msg *) &comm_msg_buffer[i][j];
			struct twopc_txn_ops *txn_ops = (struct twopc_txn_ops *) msg->body;
			txn_ops->ops_cnt = 0;
//			((struct twopc_txn_ops *) comm_msg_buffer[i][j].body)->ops =
//					(struct hash_op *) calloc(g_ops_per_txn, sizeof(struct hash_op));
		}
	}
}

void twopc_cli_insert_remote_op(int src, int trg, uint64_t txn_id, struct hash_op *hash_op) {
	struct twopc_msg *msg = (struct twopc_msg *) &comm_msg_buffer[src][trg];
	struct twopc_txn_ops *txn_ops = (struct twopc_txn_ops *) msg->body;
	struct hash_op *hash_ops = (struct hash_op *) txn_ops->hash_ops;

//	printf("Inserting op (%d --> %d)\n", src, trg);
//	printf("Msg type is %d\n", msg->twopc_code);
//	printf("Ops count is %d\n", txn_ops->ops_cnt);
//	printf("Size of hash ops is %ld\n", sizeof(txn_ops->hash_ops));
//	struct twopc_txn_ops *msg = (struct twopc_txn_ops *) comm_msg_buffer[src][trg].body;
//	struct hash_op *msg_ops = (struct hash_op *) msg->hash_ops;
	memcpy(&hash_ops[txn_ops->ops_cnt], hash_op, sizeof(struct hash_op));
//	memcpy(&msg->hash_ops[msg->ops_cnt], hash_op, sizeof(struct hash_op));
	txn_ops->ops_cnt++;
//	printf("1 Ops count is %d\n", txn_ops->ops_cnt);
	txn_ops->src_srv = g_twopc_cur_srv;
	txn_ops->src_issue_srv = src;
	txn_ops->txn_id = txn_id;
//	printf("2 Ops count is %d\n", txn_ops->ops_cnt);
}

void twopc_cli_upd_remote_op(int src, int trg, uint64_t txn_id, int remote_exec_srv) {
	struct twopc_msg *msg = (struct twopc_msg *) &comm_msg_buffer[src][trg];
	struct twopc_txn_ops *txn_ops = (struct twopc_txn_ops *) msg->body;
//	struct hash_op *hash_ops = (struct hash_op *) txn_ops->hash_ops;
	txn_ops->dst_exec_src = remote_exec_srv;	// ugly hack but we keep one variable
//	printf("Inserting op (%d --> %d)\n", src, trg);
//	printf("Msg type is %d\n", msg->twopc_code);
//	printf("Ops count is %d\n", txn_ops->ops_cnt);
//	printf("Size of hash ops is %ld\n", sizeof(txn_ops->hash_ops));
//	struct twopc_txn_ops *msg = (struct twopc_txn_ops *) comm_msg_buffer[src][trg].body;
//	struct hash_op *msg_ops = (struct hash_op *) msg->hash_ops;
//	memcpy(&hash_ops[txn_ops->ops_cnt], hash_op, sizeof(struct hash_op));
//	memcpy(&msg->hash_ops[msg->ops_cnt], hash_op, sizeof(struct hash_op));
//	txn_ops->ops_cnt++;
//	txn_ops->src_srv = g_twopc_cur_srv;
//	txn_ops->txn_id = txn_id;
}


int twopc_cli_flush_rops(int src, int ltxn_pos) {
//	printf("srv %d flushing remote ops\n", src);
	ssize_t txn_ops_msg_size = twopc_txn_ops_struct_size(); //sizeof(int) + sizeof(uint64_t) + sizeof(int) + g_ops_per_txn * sizeof(struct hash_op);
	ssize_t msg_size = sizeof(struct twopc_msg) + txn_ops_msg_size;
	int t;
	for (t = 0; t < g_nremote_exec_srv + 1; t++) {
		if (t == g_twopc_cur_srv) {
			continue;
		}
		struct twopc_msg *msg = &comm_msg_buffer[src][t];
		struct twopc_txn_ops *msg_ops = (struct twopc_txn_ops *) msg->body;
		if (msg_ops->ops_cnt > 0) {
//			printf("Have %d ops to send\n", msg_ops->ops_cnt);
//			for (int i = 0; i < msg_ops->ops_cnt; i++) {
//				struct twopc_msg *msg = (struct twopc_msg *) &comm_msg_buffer[src][t];
//				struct twopc_txn_ops *txn_ops = (struct twopc_txn_ops *) msg->body;
//				struct hash_op *hashop = (struct hash_op *) txn_ops->hash_ops;
////				printf("Sending key %ld\n", hashop[i].key);
//				assert((hashop[i].key % (g_nremote_exec_srv + 1)) == t);
//			}
			pthread_mutex_lock(&snd_ops_mtx[t]);
			msg_ops->ltxn_pos = ltxn_pos;
			if (!twopc_has_srv_exited(t)) {
				send(server_ops_sock_map[t], &msg_size, sizeof(ssize_t), 0);
				send(server_ops_sock_map[t], msg, msg_size, 0);
				msg_ops->ops_cnt = 0;
			} else {
				msg_ops->ops_cnt = 0;
				pthread_mutex_unlock(&snd_ops_mtx[t]);
				return -1;
			}
			pthread_mutex_unlock(&snd_ops_mtx[t]);
		}
	}

	return 0;
}

//int twopc_cli_send_final_result(int src, uint64_t txn_id, int result) {
//	ssize_t msg_size = sizeof(enum twopc_proto_codes) + sizeof(struct txn_result_data);
//	struct twopc_msg *final_res_msg = (struct twopc_msg *) malloc(msg_size);
//	final_res_msg->twopc_code = TWOPC_FINAL_RESULT;
////	final_res_msg->body = malloc(sizeof(struct txn_result_data));
//	struct txn_result_data *res_data = (struct txn_result_data *) final_res_msg->body;
//	res_data->result = result;
//	res_data->txn_id = txn_id;
//	res_data->src_exec_srv = src;
//	res_data->src_srv = g_twopc_cur_srv;
//	int t;
//	for (t = 0; t < g_nremote_exec_srv + 1; t++) {
//		struct twopc_msg *msg = &comm_msg_buffer[src][t];
//		struct twopc_txn_ops *msg_ops = (struct twopc_txn_ops *) msg->body;
//		if (msg_ops->ops_cnt > 0) {
//			pthread_mutex_lock(&snd_mtx[t]);
//			if (!twopc_has_srv_exited(t)) {
//				res_data->dst_exec_srv = msg_ops->dst_exec_src;
//				send(server_sock_map[t], &msg_size, sizeof(ssize_t), 0);
//				send(server_sock_map[t], final_res_msg, msg_size, 0);
//			} else {
//				msg_ops->ops_cnt = 0;
//				pthread_mutex_unlock(&snd_mtx[t]);
//				return -1;
//			}
//			msg_ops->ops_cnt = 0;
//			pthread_mutex_unlock(&snd_mtx[t]);
//		}
//	}
//
//	return 0;
//}

int twopc_cli_send_final_result(int src, uint64_t txn_id, int result, int *recipients, int rtxn_pos) {
//	printf("srv %d sending final result\n", src);
	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct txn_result_data);
//	struct twopc_msg *final_res_msg = (struct twopc_msg *) malloc(msg_size);
	struct twopc_msg *final_res_msg = txn_result_msg[src];
	final_res_msg->twopc_code = TWOPC_FINAL_RESULT;
//	final_res_msg->body = malloc(sizeof(struct txn_result_data));
	struct txn_result_data *res_data = (struct txn_result_data *) final_res_msg->body;
	res_data->result = result;
	res_data->txn_id = txn_id;
	res_data->src_exec_srv = src;
	res_data->src_srv = g_twopc_cur_srv;
	res_data->rtxn_pos = rtxn_pos;
	int t;
	for (t = 0; t < g_nremote_exec_srv + 1; t++) {
		if (t != g_twopc_cur_srv) {
			if (recipients[t] != -1) {
				pthread_mutex_lock(&snd_final_res_mtx[t]);
				if (!twopc_has_srv_exited(t)) {
					res_data->dst_exec_srv = recipients[t];
	//				if (g_twopc_cur_srv == 1) {
	//					printf("Sending final result of txn %ld to node %d worker %d from node %d worker %d\n",
	//							res_data->txn_id, t, res_data->dst_exec_srv, res_data->src_srv, res_data->src_exec_srv);
	//				}

					send(server_final_res_sock_map[t], &msg_size, sizeof(ssize_t), 0);
					send(server_final_res_sock_map[t], final_res_msg, msg_size, 0);

	//				if (g_twopc_cur_srv == 1) {
	//					printf("Sent final result of txn %ld to node %d worker %d from node %d worker %d\n",
	//										res_data->txn_id, t, res_data->dst_exec_srv, res_data->src_srv, res_data->src_exec_srv);
	//				}
				} else {
	//				free(final_res_msg);
					pthread_mutex_unlock(&snd_final_res_mtx[t]);
					return -1;
				}
				pthread_mutex_unlock(&snd_final_res_mtx[t]);
			}
		}

	}
//	free(final_res_msg);
	return 0;
}


int twopc_cli_send_txn_result(int dst, uint64_t txn_id, int result, int cur_exec_srv, int txn_issued_srv, int ltxn_pos, int rtxn_pos) {
//	printf("srv %d sending result\n", cur_exec_srv);
	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct txn_result_data);
//	printf("Message size is %ld\n", msg_size);
//	struct twopc_msg *res_msg = (struct twopc_msg *) malloc(msg_size);
	struct twopc_msg *res_msg = txn_result_msg[cur_exec_srv];
	res_msg->twopc_code = TWOPC_OPS_RESULT;
//	res_msg.body = malloc(sizeof(struct txn_result_data));
	struct txn_result_data *res_data = (struct txn_result_data *) res_msg->body;
	res_data->result = result;
	res_data->txn_id = txn_id;
	res_data->dst_exec_srv = cur_exec_srv;
	res_data->src_exec_srv = txn_issued_srv;
	res_data->src_srv = g_twopc_cur_srv;
	res_data->ltxn_pos = ltxn_pos;
	res_data->rtxn_pos = rtxn_pos;
//	if (g_twopc_cur_srv == 0) {
//		printf("Sending result of txn %ld from node %d srv %d to node %d srv %d\n", txn_id, g_twopc_cur_srv, cur_exec_srv, dst, txn_issued_srv);
//	}
//	double tstart = now();
//	pthread_mutex_lock(&snd_res_mtx[dst]);
	pthread_spin_lock(&ssnd_res_mtx[dst]);
//	double tend = now();
//	printf("Time to acquire mtx %.9f\n", (tend - tstart) * 1000);
//	if (!twopc_has_srv_exited(dst)) {
		send(server_res_sock_map[dst], &msg_size, sizeof(ssize_t), 0);
		send(server_res_sock_map[dst], res_msg, msg_size, 0);
//		printf("Sent result to srv %d\n", dst);
//	} else {
//		pthread_mutex_unlock(&snd_res_mtx[dst]);
//		free(res_msg);
//		return -1;
//	}
	pthread_spin_unlock(&ssnd_res_mtx[dst]);
//	pthread_mutex_unlock(&snd_res_mtx[dst]);
//	if (g_twopc_cur_srv == 0) {
//		printf("Sent result of txn %ld from node %d srv %d to node %d srv %d\n", res_data->txn_id, res_data->src_srv, res_data->dst_exec_srv, dst, res_data->src_exec_srv);
//	}
//	free(res_msg);

	return 0;
}

//int twopc_cli_register(int cli, int srv, int sockfd) {
//	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct registration_data);
//	struct twopc_msg *msg = (struct twopc_msg *) malloc(msg_size);
//	msg->twopc_code = TWOPC_REGISTER;
//	struct registration_data *reg_data = (struct registration_data *) msg->body;
//	reg_data->src = cli;
//	pthread_mutex_lock(&snd_mtx[srv]);
//	send(sockfd, &msg_size, sizeof(ssize_t), 0);
//	ssize_t bytes_sent = send(sockfd, msg, msg_size, 0);
//	pthread_mutex_unlock(&snd_mtx[srv]);
//	server_sock_map[srv] = sockfd;
//	printf("TWOPC_REGISTER (%d --> %d) %ld bytes\n", cli, srv, bytes_sent);
////	printf("Msg size %ld and sent %ld bytes\n", sizeof(msg), bytes_sent);
//	free(msg);
//	return 0;
//}

int twopc_cli_ops_register(int cli, int srv, int sockfd) {
	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct registration_data);
	struct twopc_msg *msg = (struct twopc_msg *) malloc(msg_size);
	msg->twopc_code = TWOPC_REGISTER_OPS;
	struct registration_data *reg_data = (struct registration_data *) msg->body;
	reg_data->src = cli;
	pthread_mutex_lock(&snd_ops_mtx[srv]);
	send(sockfd, &msg_size, sizeof(ssize_t), 0);
	ssize_t bytes_sent = send(sockfd, msg, msg_size, 0);
	pthread_mutex_unlock(&snd_ops_mtx[srv]);
	server_ops_sock_map[srv] = sockfd;
	printf("TWOPC_REGISTER_OPS (%d --> %d) %ld bytes\n", cli, srv, bytes_sent);
//	printf("Msg size %ld and sent %ld bytes\n", sizeof(msg), bytes_sent);
//	free(msg);
	return 0;
}


int twopc_cli_res_register(int cli, int srv, int sockfd) {
	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct registration_data);
	struct twopc_msg *msg = (struct twopc_msg *) malloc(msg_size);
	msg->twopc_code = TWOPC_REGISTER_RES;
	struct registration_data *reg_data = (struct registration_data *) msg->body;
	reg_data->src = cli;
	pthread_mutex_lock(&snd_res_mtx[srv]);
	send(sockfd, &msg_size, sizeof(ssize_t), 0);
	ssize_t bytes_sent = send(sockfd, msg, msg_size, 0);
	pthread_mutex_unlock(&snd_res_mtx[srv]);
	server_res_sock_map[srv] = sockfd;
	printf("TWOPC_REGISTER_RES (%d --> %d) %ld bytes\n", cli, srv, bytes_sent);
//	printf("Msg size %ld and sent %ld bytes\n", sizeof(msg), bytes_sent);
//	free(msg);
	return 0;
}

int twopc_cli_final_res_register(int cli, int srv, int sockfd) {
	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct registration_data);
	struct twopc_msg *msg = (struct twopc_msg *) malloc(msg_size);
	msg->twopc_code = TWOPC_REGISTER_FINAL_RES;
	struct registration_data *reg_data = (struct registration_data *) msg->body;
	reg_data->src = cli;
	pthread_mutex_lock(&snd_final_res_mtx[srv]);
	send(sockfd, &msg_size, sizeof(ssize_t), 0);
	ssize_t bytes_sent = send(sockfd, msg, msg_size, 0);
	pthread_mutex_unlock(&snd_final_res_mtx[srv]);
	server_final_res_sock_map[srv] = sockfd;
	printf("TWOPC_REGISTER_FINAL_RES (%d --> %d) %ld bytes\n", cli, srv, bytes_sent);
//	printf("Msg size %ld and sent %ld bytes\n", sizeof(msg), bytes_sent);
//	free(msg);
	return 0;
}


// register remote client locally
void twopc_register_remote_client_ops(struct registration_data *reg_data) {
	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct registration_data);
	int remote_client = reg_data->src;
	struct twopc_msg *msg = (struct twopc_msg *) malloc(msg_size);
	msg->twopc_code = TWOPC_REGISTER_ACK;
	reg_data->src = g_twopc_cur_srv;
	memcpy(msg->body, reg_data, sizeof(struct registration_data));
	while (server_ops_sock_map[remote_client] == -1) ;
	pthread_mutex_lock(&snd_ops_mtx[remote_client]);
//	send(server_sock_map[remote_client], &msg_size, sizeof(ssize_t), 0);
	send(server_ops_sock_map[remote_client], &msg_size, sizeof(ssize_t), 0);
	ssize_t bytes_sent = send(server_ops_sock_map[remote_client], msg, msg_size, 0);
//	printf("Register ops ACK bytes %ld\n", bytes_sent);
	pthread_mutex_unlock(&snd_ops_mtx[remote_client]);
	printf("TWOPC_REGISTER_ACK (%d --> %d) %ld bytes\n", g_twopc_cur_srv, remote_client, bytes_sent);
//	free(msg);
}

void twopc_register_remote_client_res(struct registration_data *reg_data) {
	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct registration_data);
	int remote_client = reg_data->src;
	struct twopc_msg *msg = (struct twopc_msg *) malloc(msg_size);
	msg->twopc_code = TWOPC_REGISTER_ACK;
	reg_data->src = g_twopc_cur_srv;
	memcpy(msg->body, reg_data, sizeof(struct registration_data));
	while (server_res_sock_map[remote_client] == -1) ;
	pthread_mutex_lock(&snd_res_mtx[remote_client]);
	//	send(server_sock_map[remote_client], &msg_size, sizeof(ssize_t), 0);
	send(server_res_sock_map[remote_client], &msg_size, sizeof(ssize_t), 0);
	ssize_t bytes_sent = send(server_res_sock_map[remote_client], msg, msg_size, 0);
//	printf("Register res ACK bytes %ld\n", bytes_sent);
//		send(server_final_res_sock_map[remote_client], &msg_size, sizeof(ssize_t), 0);
//		bytes_sent = send(server_final_res_sock_map[remote_client], msg, msg_size, 0);
//		printf("Register final res ACK bytes %ld\n", bytes_sent);
	//	ssize_t bytes_sent = send(server_sock_map[remote_client], msg, msg_size, 0);
	pthread_mutex_unlock(&snd_res_mtx[remote_client]);
	printf("TWOPC_REGISTER_ACK (%d --> %d) %ld bytes\n", g_twopc_cur_srv, remote_client, bytes_sent);
//	free(msg);
}


void twopc_register_remote_client_final_res(struct registration_data *reg_data) {
	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct registration_data);
	int remote_client = reg_data->src;
	struct twopc_msg *msg = (struct twopc_msg *) malloc(msg_size);
	msg->twopc_code = TWOPC_REGISTER_ACK;
	reg_data->src = g_twopc_cur_srv;
	memcpy(msg->body, reg_data, sizeof(struct registration_data));
	while (server_final_res_sock_map[remote_client] == -1) ;
	pthread_mutex_lock(&snd_final_res_mtx[remote_client]);
	//	send(server_sock_map[remote_client], &msg_size, sizeof(ssize_t), 0);
	send(server_final_res_sock_map[remote_client], &msg_size, sizeof(ssize_t), 0);
	ssize_t bytes_sent = send(server_final_res_sock_map[remote_client], msg, msg_size, 0);
//		printf("Register final res ACK bytes %ld\n", bytes_sent);
	//	ssize_t bytes_sent = send(server_sock_map[remote_client], msg, msg_size, 0);

	pthread_mutex_unlock(&snd_final_res_mtx[remote_client]);
//	printf("TWOPC_REGISTER_ACK (%d --> %d) %ld bytes\n", g_twopc_cur_srv, remote_client, bytes_sent);
	free(msg);
}


int twopc_cli_send_txn_exit() {
	printf("Server %d sending exit message\n", g_twopc_cur_srv);
	// send the message to all the possible servers
	ssize_t msg_size = sizeof(struct twopc_msg) + sizeof(struct twopc_exit_data);
	struct twopc_msg *msg = (struct twopc_msg *) malloc(msg_size);
	msg->twopc_code = TWOPC_EXIT;
	struct twopc_exit_data *exit_data = (struct twopc_exit_data *) msg->body;
	exit_data->src = g_twopc_cur_srv;

	int t;
	for (t = 0; t < g_nremote_exec_srv + 1; t++) {
		pthread_mutex_lock(&snd_mtx[t]);
		send(server_ops_sock_map[t], &msg_size, sizeof(ssize_t), 0);
		send(server_ops_sock_map[t], msg, msg_size, 0);
		send(server_res_sock_map[t], &msg_size, sizeof(ssize_t), 0);
		send(server_res_sock_map[t], msg, msg_size, 0);
		send(server_final_res_sock_map[t], &msg_size, sizeof(ssize_t), 0);
		send(server_final_res_sock_map[t], msg, msg_size, 0);
		pthread_mutex_unlock(&snd_mtx[t]);
	}

	return 0;
}

//int twopc_cli_register(int cli, int srv, int sockfd) {
//	struct msghdr msg;
//	struct iovec iov[1];
//	memset(&msg, 0, sizeof(msg));
//	memset(iov, 0, sizeof(iov));
//
//	struct registration_data *reg_data = (struct registration_data *) malloc(sizeof(struct registration_data));
//	reg_data->src = cli;
//	iov[0].iov_base = reg_data;
//	iov[0].iov_len = sizeof(struct registration_data);
//
//	enum twopc_proto_codes code = TWOPC_REGISTER;
//	msg.msg_name = (void *) &code;
//	msg.msg_namelen = sizeof(enum twopc_proto_codes);
//
//	msg.msg_iov = iov;
//	msg.msg_iovlen = 1;
//
//	ssize_t bytes_sent = sendmsg(sockfd, &msg, 0);
//	printf("Sent REGISTER with %ld bytes to %d\n", bytes_sent, reg_data->src);
//	if (bytes_sent == -1) {
//		perror("sendmsg");
//	}
//	server_sock_map[srv] = sockfd;
//	return 0;
//}
//
//void twopc_register_remote_client(struct registration_data *reg_data) {
//	struct msghdr msg;
//	struct iovec iov[1];
//	memset(&msg, 0, sizeof(msg));
//	memset(iov, 0, sizeof(iov));
//
//	iov[0].iov_base = reg_data;
//	iov[0].iov_len = sizeof(struct registration_data);
//
//	enum twopc_proto_codes code = TWOPC_REGISTER_ACK;
//	msg.msg_name = (void *) &code;
//	msg.msg_namelen = sizeof(enum twopc_proto_codes);
//	msg.msg_iov = iov;
//	msg.msg_iovlen = 1;
//
//	while (server_sock_map[reg_data->src] == -1) ;
//	ssize_t bytes_sent = sendmsg(server_sock_map[reg_data->src], &msg, 0);
//	printf("Sent REGISTER ACK with %ld bytes to %d\n", bytes_sent, reg_data->src);
//}


//int twopc_cli_get_result(uint64_t txn_id, int srv) {
//	struct txn_result_data result_data;
//	recv(server_sock_map[srv], &result_data, sizeof(struct txn_result_data), 0);
//	assert(result_data.txn_id == txn_id);
//
//	return result_data.result;
//}

int twopc_cli_connect(int cli, int srv, char *ip_addr, short port) {
	printf("Trying to connect to server %s:%d\n", ip_addr, port);
	struct sockaddr_in srv_addr;
	int sockfd = 0;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		return -1;
	}

//	fcntl(sockfd, F_SETFL, fcntl(sockfd, F_GETFL, 0) | O_NONBLOCK);

	memset(&srv_addr, 0, sizeof(struct sockaddr_in));
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(port);

	if (inet_pton(AF_INET, ip_addr, &srv_addr.sin_addr) < 0) {
		perror("inet_pton");
		return -1;
	}


	while (connect(sockfd, (struct sockaddr *) &srv_addr, sizeof(srv_addr)) < 0) {
//		perror("connect");
//		return -1;
		usleep(100000);
	}
	printf("Connected to server %s:%d\n", ip_addr, port);

	// first send the registration message
//	struct twopc_msg msg;
//	msg.twopc_code = TWOPC_REGISTER;
//	msg.body = malloc(sizeof(struct registration_data));
//	struct registration_data *reg_data = (struct registration_data *) msg.body;
//	reg_data->src = cli;
//	send(sockfd, &msg, sizeof(msg), 0);
	// and then wait for the acknowledgement
//	int resp = 0;
//	int valread = recv(sockfd, &resp, sizeof(int), 0);
//	if (resp != 1) {
//		return -1;
//	}

//	server_sock_map[srv] = sockfd;

	return sockfd;
}

