/*
 * twopc_util.c
 *
 *  Created on: 24 Sep 2018
 *      Author: anadioti
 */

#include "twopc.h"
#include "../headers.h"

// List for incoming txn operations
//LIST_HEAD(rtxn_ops, rtxn_ops_entry) rtxn_ops_list =
//		LIST_HEAD_INITIALIZER(rtxn_ops_list);
//struct rtxn_ops_entry {
//	LIST_ENTRY(rtxn_ops_entry) rtxn_ops_entries;
//	struct twopc_txn_ops remote_ops;
//};
TAILQ_HEAD(rtxn_ops, rtxn_ops_entry) rtxn_ops_list =
		TAILQ_HEAD_INITIALIZER(rtxn_ops_list);
struct rtxn_ops_entry {
	TAILQ_ENTRY(rtxn_ops_entry) rtxn_ops_entries;
	struct twopc_txn_ops remote_ops;
};

pthread_mutex_t rtxn_ops_list_mtx;

// List for incoming txn results
LIST_HEAD(rtxn_results, rtxn_results_entry) rtxn_results_list =
		LIST_HEAD_INITIALIZER(rtxn_results_list);
struct rtxn_results_entry {
	LIST_ENTRY(rtxn_results_entry) rtxn_results_entries;
	struct txn_result_data remote_result;
};
pthread_mutex_t rtxn_results_list_mtx;

// List for incoming final txn results
LIST_HEAD(rtxn_fin_results, rtxn_fin_results_entry) rtxn_fin_results_list =
		LIST_HEAD_INITIALIZER(rtxn_fin_results_list);
struct rtxn_fin_results_entry {
	LIST_ENTRY(rtxn_fin_results_entry) rtxn_fin_results_entries;
	struct txn_result_data fin_remote_result;
};
pthread_mutex_t rtxn_fin_results_list_mtx;

int *twopc_connected_srvs;
volatile int *twopc_finished_srvs;
int rtxn_ops_count;
struct rtxn_context **remote_txns_queue_l_issued;
struct rtxn_context **remote_txns_queue_r_issued;
int *rtxns_queue_lissued_cnt;
int *rtxns_queue_rissued_cnt;

int *rtxns_lq_ins_idx;
int *rtxns_lq_rmv_idx;
int *rtxns_rq_ins_idx;
int *rtxns_rq_rmv_idx;

int *idx_success;
int *idx_failure;

int **recent_l_updates;
int *recent_l_updates_rcnt;
int *recent_l_updates_wcnt;
int *recent_l_updates_rcnt_rounds;
int *recent_l_updates_wcnt_rounds;

pthread_mutex_t *rtxns_queue_l_issued_mtx;
pthread_mutex_t *rtxns_queue_r_issued_mtx;

pthread_mutex_t ack_mtx;

ssize_t twopc_txn_ops_struct_size() {
//	ssize_t txn_ops_msg_size = sizeof(int) +
//			sizeof(int) +
//			sizeof(int) +
//			sizeof(uint64_t) +
//			sizeof(int) +
//			g_nremote_exec_ops * sizeof(struct hash_op);

	ssize_t txn_ops_msg_size = sizeof(struct twopc_txn_ops) + g_nremote_exec_ops * sizeof(struct hash_op);

	return txn_ops_msg_size;
}

void twopc_util_init() {
	pthread_mutex_init(&rtxn_ops_list_mtx, NULL);
	pthread_mutex_init(&rtxn_results_list_mtx, NULL);
	pthread_mutex_init(&rtxn_fin_results_list_mtx, NULL);
	pthread_mutex_init(&ack_mtx, NULL);
//	LIST_INIT(&rtxn_ops_list);
	TAILQ_INIT(&rtxn_ops_list);
	LIST_INIT(&rtxn_results_list);
	LIST_INIT(&rtxn_fin_results_list);
	twopc_connected_srvs = (int *) calloc(g_nremote_exec_srv + 1, sizeof(int));
	twopc_finished_srvs = (volatile int *) calloc(g_nremote_exec_srv + 1, sizeof(int));
	rtxn_ops_count = 0;
	remote_txns_queue_l_issued = (struct rtxn_context **) calloc(g_nservers, sizeof(struct rtxn_context *));
	remote_txns_queue_r_issued = (struct rtxn_context **) calloc(g_nservers, sizeof(struct rtxn_context *));
	rtxns_queue_l_issued_mtx = (pthread_mutex_t *) calloc(g_nservers, sizeof(pthread_mutex_t));
	rtxns_queue_r_issued_mtx = (pthread_mutex_t *) calloc(g_nservers, sizeof(pthread_mutex_t));
	for (int i = 0; i < g_nservers; i++) {
		remote_txns_queue_l_issued[i] = (struct rtxn_context *) calloc(MAX_REMOTE_OUT_REQS_PER_SRV, sizeof(struct rtxn_context));
		remote_txns_queue_r_issued[i] = (struct rtxn_context *) calloc(MAX_REMOTE_IN_REQS_PER_SRV, sizeof(struct rtxn_context));
		pthread_mutex_init(&rtxns_queue_l_issued_mtx[i], NULL);
		pthread_mutex_init(&rtxns_queue_r_issued_mtx[i], NULL);
		for (int j = 0; j < MAX_REMOTE_OUT_REQS_PER_SRV; j++) {
			remote_txns_queue_l_issued[i][j].remote_reqs = -1;
			remote_txns_queue_l_issued[i][j].remote_srvs = (int *) calloc(g_nremote_exec_srv + 1, sizeof(int));
			for (int k = 0; k < g_nremote_exec_srv + 1; k++) {
				remote_txns_queue_l_issued[i][j].remote_srvs[k] = -1;
			}
		}
		for (int j = 0; j < MAX_REMOTE_IN_REQS_PER_SRV; j++) {
			remote_txns_queue_r_issued[i][j].remote_reqs = -1;
		}
	}
	rtxns_queue_lissued_cnt = (int *) calloc(g_nservers, sizeof(int));
	rtxns_queue_rissued_cnt = (int *) calloc(g_nservers, sizeof(int));

	rtxns_lq_ins_idx = (int *) calloc(g_nservers, sizeof(int));
	rtxns_lq_rmv_idx = (int *) calloc(g_nservers, sizeof(int));
	rtxns_rq_ins_idx = (int *) calloc(g_nservers, sizeof(int));
	rtxns_rq_rmv_idx = (int *) calloc(g_nservers, sizeof(int));

	idx_success = (int *) calloc(g_nservers, sizeof(int));
	idx_failure = (int *) calloc(g_nservers, sizeof(int));

	recent_l_updates_rcnt = (int *) calloc(g_nservers, sizeof(int));
	recent_l_updates_wcnt = (int *) calloc(g_nservers, sizeof(int));
	recent_l_updates_rcnt_rounds = (int *) calloc(g_nservers, sizeof(int));
	recent_l_updates_wcnt_rounds = (int *) calloc(g_nservers, sizeof(int));
	recent_l_updates = (int **) calloc(g_nservers, sizeof(int *));
	for (int i = 0; i < g_nservers; i++) {
		recent_l_updates[i] = (int *) calloc(MAX_REMOTE_OUT_REQS_PER_SRV, sizeof(int));
		for (int j = 0; j < MAX_REMOTE_OUT_REQS_PER_SRV; j++) {
			recent_l_updates[i][j] = -1;
		}
	}
}

int twopc_store_locally_issued_txn_ctx(int local_srv, int total_remote_reqs, struct txn_ctx *cur_ctx, int *remote_srvs) {
//	struct rtxn_context *rctx = &remote_txns_queue_l_issued[local_srv][rtxns_queue_lissued_idx[local_srv]];

	int ret = -1;
	int idx = rtxns_lq_ins_idx[local_srv] % MAX_REMOTE_OUT_REQS_PER_SRV;
//	int idx = (rtxns_queue_lissued_cnt[local_srv] + 1) % MAX_REMOTE_OUT_REQS_PER_SRV;
	for (int i = 0; i < MAX_REMOTE_OUT_REQS_PER_SRV; i++) {
		struct rtxn_context *rctx = &remote_txns_queue_l_issued[local_srv][idx];
		if (rctx->remote_reqs < 0) {
//			printf("Srv %d storing local txn %ld\n", local_srv, cur_ctx->ts);
			rctx->remote_reqs = total_remote_reqs;
//			for (int i = 0; i < g_nremote_exec_srv + 1; i++) {
//				rctx->remote_srvs[i] = remote_srvs[i];
//			}
			memcpy(rctx->remote_srvs, remote_srvs, (g_nremote_exec_srv + 1) * sizeof(int));
			rctx->result = TXN_COMMIT;
			memcpy(&rctx->ctx ,cur_ctx, sizeof(struct txn_ctx));
//			printf("Srv %d stored local txn %ld\n", local_srv, rctx->ctx.ts);
			ret = idx;
			rtxns_lq_ins_idx[local_srv] = idx + 1;
			rtxns_queue_lissued_cnt[local_srv]++;
			break;
		} else {
//			printf("Failed\n");
			idx = (idx + 1) % MAX_REMOTE_OUT_REQS_PER_SRV;
		}
	}

//	memcpy(rctx->ctx.op_ctx, cur_ctx->op_ctx, MAX_OPS_PER_QUERY * sizeof(struct op_ctx));
//	rtxns_queue_lissued_idx[local_srv] = (rtxns_queue_lissued_idx[local_srv] + 1) % MAX_REMOTE_REQS_PER_SRV;
	return ret;
}

void twopc_load_locally_issued_txn_ctx(int local_srv, uint64_t txn_id, struct txn_ctx *cur_ctx) {
//	int cnt = rtxns_queue_lissued_idx[local_srv] - 1;
	for (int i = 0; i < rtxns_queue_lissued_cnt[local_srv]; i++) {
		struct rtxn_context *rctx = &remote_txns_queue_l_issued[local_srv][i];
		if (rctx->ctx.ts == txn_id) {
			cur_ctx = &rctx->ctx;
			break;
		}
	}
}

int twopc_store_remotely_issued_txn_ctx(int local_srv, struct txn_ctx *cur_ctx) {
//	struct rtxn_context *rctx = &remote_txns_queue_r_issued[local_srv][rtxns_queue_rissued_idx[local_srv]];
//	if (rctx->remote_reqs >= 0) {
//		return -1;
//	}
	int ret = -1;
	if (rtxns_queue_rissued_cnt[local_srv] < MAX_REMOTE_IN_REQS_PER_SRV) {
		int idx = rtxns_rq_ins_idx[local_srv] % MAX_REMOTE_IN_REQS_PER_SRV;
		for (int i = 0; i < MAX_REMOTE_IN_REQS_PER_SRV; i++) {
			struct rtxn_context *rctx = &remote_txns_queue_r_issued[local_srv][idx];
			if (rctx->remote_reqs < 0) {
				rctx->remote_reqs = 1;
				memcpy(&rctx->ctx, cur_ctx, sizeof(struct txn_ctx));
			//	printf("Stored size %ld\n", sizeof(struct txn_ctx));
	//			for (int i = 0; i < cur_ctx->nops; i++) {
	//				struct op_ctx *opctx = &cur_ctx->op_ctx[i];
	//				assert(opctx->e);
	//			}
				ret = idx;
				rtxns_queue_rissued_cnt[local_srv]++;
				rtxns_rq_ins_idx[local_srv] = idx + 1;
				break;
			} else {
//				printf("Failed to locate index\n");
				idx = (idx + 1) % MAX_REMOTE_IN_REQS_PER_SRV;
			}
		}
	}


//	rtxns_queue_rissued_idx[local_srv] = (rtxns_queue_rissued_idx[local_srv] + 1) % MAX_REMOTE_REQS_PER_SRV;
	return ret;
}

int twopc_rtxn_have_space(int local_srv) {
	if (rtxns_queue_rissued_cnt[local_srv] < MAX_REMOTE_IN_REQS_PER_SRV) {
		return 0;
	}

	return -1;
}

int twopc_ltxn_have_space(int local_srv) {
	if (rtxns_queue_lissued_cnt[local_srv] < MAX_REMOTE_OUT_REQS_PER_SRV) {
		return 0;
	}

	return -1;
}


void twopc_load_remotely_issued_txn_ctx(int local_srv, uint64_t txn_id, struct txn_ctx *cur_ctx) {
//	int cnt = rtxns_queue_rissued_idx[local_srv] - 1;
	for (int i = 0; i < rtxns_queue_rissued_cnt[local_srv]; i++) {
		struct rtxn_context *rctx = &remote_txns_queue_r_issued[local_srv][i];
		if (rctx->ctx.ts == txn_id) {
			cur_ctx = &rctx->ctx;
			break;
		}
	}
}

void twopc_upd_result_locally_issued_txn_ctx(struct txn_result_data *result_data) {
//	if (g_twopc_cur_srv == 1) {
//		printf("Received result for txn %ld issued by worker %d from srv %d worker %d\n",
//			result_data->txn_id, result_data->src_exec_srv, result_data->src_srv, result_data->dst_exec_srv);
//	}

//	int cnt = rtxns_queue_lissued_idx[result_data->src_exec_srv] - 1;
	int found = 0;
	int i = result_data->ltxn_pos;
//	for (int i = 0; i < MAX_REMOTE_REQS_PER_SRV; i++) {
//		struct rtxn_context *rctx = &remote_txns_queue_l_issued[result_data->src_exec_srv][i];
		if (remote_txns_queue_l_issued[result_data->src_exec_srv][i].ctx.ts == result_data->txn_id) {

			pthread_mutex_lock(&rtxns_queue_l_issued_mtx[result_data->src_exec_srv]);

			found = 1;
			if (result_data->result == TXN_ABORT) {
				remote_txns_queue_l_issued[result_data->src_exec_srv][i].result = TXN_ABORT;
			}


			remote_txns_queue_l_issued[result_data->src_exec_srv][i].remote_srvs[result_data->src_srv] = result_data->dst_exec_srv;
//			if (g_twopc_cur_srv == 1) {
//				printf("TXN %ld was executed on node %d by srv %d\n", result_data->txn_id, result_data->src_srv, result_data->dst_exec_srv);
//			}
			remote_txns_queue_l_issued[result_data->src_exec_srv][i].remote_reqs--;
			remote_txns_queue_l_issued[result_data->src_exec_srv][i].pos = result_data->rtxn_pos;
//			assert(remote_txns_queue_l_issued[result_data->src_exec_srv][i].remote_reqs >= 0);
//			printf("Now the remote reqs for worker %d are %d\n", result_data->src_exec_srv, rctx->remote_reqs);
//			printf("Worker %d in remote srv %d\n", result_data->dst_exec_srv, result_data->src_srv);
			rtxns_lq_rmv_idx[result_data->src_exec_srv] = i;
			recent_l_updates[result_data->src_exec_srv][recent_l_updates_wcnt[result_data->src_exec_srv]] = i;
			recent_l_updates_wcnt[result_data->src_exec_srv] = (recent_l_updates_wcnt[result_data->src_exec_srv] + 1) % MAX_REMOTE_OUT_REQS_PER_SRV;
			if (recent_l_updates_wcnt[result_data->src_exec_srv] == 0) {
				recent_l_updates_wcnt_rounds[result_data->src_exec_srv]++;
			}
			pthread_mutex_unlock(&rtxns_queue_l_issued_mtx[result_data->src_exec_srv]);
//			twopc_cli_upd_remote_op(result_data->src_exec_srv, result_data->src_srv, result_data->txn_id, result_data->dst_exec_srv);
//			break;
		}
//	}
	if (!found) {
		for (int j = 0; j < g_nservers; j++) {
			for (int i = 0; i < MAX_REMOTE_OUT_REQS_PER_SRV; i++) {
//				struct rtxn_context *rctx = &remote_txns_queue_l_issued[j][i];
				if (remote_txns_queue_l_issued[j][i].ctx.ts == result_data->txn_id) {
					printf("Found it on srv %d instead of %d\n", j, result_data->src_exec_srv);
					break;
				}
			}
		}
	}
	assert(found == 1);
}

void twopc_upd_result_remotely_issued_txn_ctx(struct txn_result_data *result_data) {
//	int cnt = rtxns_queue_rissued_idx[result_data->dst_exec_srv] - 1;
//	if (g_twopc_cur_srv == 0) {
//		printf("Received final result for txn %ld executed here by srv %d\n", result_data->txn_id, result_data->dst_exec_srv);
//	}
	int found = 0;
	int i = result_data->rtxn_pos;
//	for (int i = 0; i < MAX_REMOTE_REQS_PER_SRV; i++) {
//		struct rtxn_context *rctx = &remote_txns_queue_r_issued[result_data->dst_exec_srv][i];
		uint64_t txn_id;

#if ENABLE_2PC
		txn_id = remote_txns_queue_r_issued[result_data->dst_exec_srv][i].ctx.rtxn_id;
#else
		txn_id = remote_txns_queue_r_issued[result_data->dst_exec_srv][i].ctx.ts;
#endif
		if (txn_id == result_data->txn_id) {
			if (result_data->result == TXN_ABORT) {
				remote_txns_queue_r_issued[result_data->dst_exec_srv][i].result = TXN_ABORT;
			}
			found = 1;
			pthread_mutex_lock(&rtxns_queue_r_issued_mtx[result_data->dst_exec_srv]);
			remote_txns_queue_r_issued[result_data->dst_exec_srv][i].remote_reqs = 0;
			pthread_mutex_unlock(&rtxns_queue_r_issued_mtx[result_data->dst_exec_srv]);
//			break;
		}
//	}
	if (!found) {
		uint64_t txn_id;
			for (int j = 0; j < g_nservers; j++) {
				for (int i = 0; i < MAX_REMOTE_IN_REQS_PER_SRV; i++) {
	//				struct rtxn_context *rctx = &remote_txns_queue_l_issued[j][i];
#if ENABLE_2PC
		txn_id = remote_txns_queue_r_issued[j][i].ctx.rtxn_id;
#else
		txn_id = remote_txns_queue_r_issued[j][i].ctx.ts;
#endif
					if (txn_id == result_data->txn_id) {
						printf("Found it on srv %d instead of %d\n", j, result_data->dst_exec_srv);
						break;
					}
				}
			}
		}
	if (g_twopc_cur_srv == 0) {
		assert(found == 1);
	}

}

int get_idx_success(int worker) {
	return idx_success[worker];
}

int get_idx_failure(int worker) {
	return idx_failure[worker];
}

int twopc_load_ltxn_results(int cur_worker_srv, int *result, struct txn_ctx **ctx, int **remote_srvs, int pos, int *rtxn_pos) {
	int ret = -1;
//	int idx = rtxns_lq_rmv_idx[cur_worker_srv] % MAX_REMOTE_OUT_REQS_PER_SRV;

	int all_reads = recent_l_updates_rcnt_rounds[cur_worker_srv] * MAX_REMOTE_OUT_REQS_PER_SRV + recent_l_updates_rcnt[cur_worker_srv];
	int all_writes = recent_l_updates_wcnt_rounds[cur_worker_srv] * MAX_REMOTE_OUT_REQS_PER_SRV + recent_l_updates_wcnt[cur_worker_srv];
	if (all_reads < all_writes) {
		int idx = recent_l_updates[cur_worker_srv][recent_l_updates_rcnt[cur_worker_srv]];
		int found = 0;
		for (int i = 0; i < MAX_REMOTE_OUT_REQS_PER_SRV; i++) {
			struct rtxn_context *rctx = &remote_txns_queue_l_issued[cur_worker_srv][idx];

			if (rctx->remote_reqs == 0) {
				if (idx == recent_l_updates[cur_worker_srv][recent_l_updates_rcnt[cur_worker_srv]]) {
					found = 1;
					idx_success[cur_worker_srv]++;
					recent_l_updates_rcnt[cur_worker_srv] = recent_l_updates_rcnt[cur_worker_srv] + 1;
					if (recent_l_updates_rcnt[cur_worker_srv] == MAX_REMOTE_OUT_REQS_PER_SRV) {
						recent_l_updates_rcnt[cur_worker_srv] = 0;
						recent_l_updates_rcnt_rounds[cur_worker_srv]++;
					}
	//				printf("Got it this time\n");
				} else {
					idx_failure[cur_worker_srv]++;
				}

				pthread_mutex_lock(&rtxns_queue_l_issued_mtx[cur_worker_srv]);
				ret = idx;
				*result = rctx->result;
				*ctx = &rctx->ctx;
	//			assert(ctx);
	//			printf("ctx addr %p\n", (void *) ctx);
				*remote_srvs = rctx->remote_srvs;
				*rtxn_pos = rctx->pos;
	//			memcpy(*ctx, &rctx->ctx, sizeof(struct txn_ctx));
	//			memcpy(*remote_srvs, rctx->remote_srvs, (g_nremote_exec_srv + 1) * sizeof(int));
	//			for (int j = 0; j < g_nremote_exec_srv + 1; j++) {
	//				if ((*remote_srvs)[j] != -1) {
	//					printf("Loading: TXN %ld executed on node %d worker %d\n", (*ctx)->ts, j, (*remote_srvs)[j]);
	//				}
	////				assert(rctx->remote_srvs[j] == remote_srvs[j]);
	//			}
				assert(ctx);
				rtxns_lq_rmv_idx[cur_worker_srv] = idx + 1;
	//			printf("Breaking\n");
				pthread_mutex_unlock(&rtxns_queue_l_issued_mtx[cur_worker_srv]);
				break;
			} else {
	//			printf("Had to increase the index\n");
				if (found) {
					printf("Should not be here\n");
				}
				idx = (idx + 1) % MAX_REMOTE_OUT_REQS_PER_SRV;
			}
		}
	}


//	printf("Exiting\n");
	return ret;
}

void twopc_invalidate_ltxn(int cur_worker_srv, int pos) {
	struct rtxn_context *rctx = &remote_txns_queue_l_issued[cur_worker_srv][pos];
	rctx->remote_reqs = -1;
	rtxns_queue_lissued_cnt[cur_worker_srv]--;
//	printf("Invalidated local at pos %d\n", pos);
}

int twopc_load_rtxn_final_results(int cur_worker_srv, int *result, struct txn_ctx **ctx, int pos) {
	int ret = -1;
	int idx = rtxns_rq_rmv_idx[cur_worker_srv] % MAX_REMOTE_IN_REQS_PER_SRV;
	for (int i = 0; i < MAX_REMOTE_IN_REQS_PER_SRV; i++) {
		struct rtxn_context *rctx = &remote_txns_queue_r_issued[cur_worker_srv][idx];
		if (rctx->remote_reqs == 0) {
			*result = rctx->result;
			*ctx = &rctx->ctx;
//			memcpy(ctx, &rctx->ctx, sizeof(struct txn_ctx));
			ret = idx;
			rtxns_rq_rmv_idx[cur_worker_srv] = idx + 1;
			break;
		} else {
			idx = (idx + 1) % MAX_REMOTE_IN_REQS_PER_SRV;
		}
	}

	return ret;
}

void twopc_invalidate_rtxn(int cur_worker_srv, int pos) {
	struct rtxn_context *rctx = &remote_txns_queue_r_issued[cur_worker_srv][pos];
	rctx->remote_reqs = -1;
	rtxns_queue_rissued_cnt[cur_worker_srv]--;
}


void twopc_push_remote_ops(struct twopc_txn_ops *rops) {
//	printf("Pushed %d remote ops\n", rops->ops_cnt);

	pthread_mutex_lock(&rtxn_ops_list_mtx);
//	printf("Node %d received remote op for txn %ld\n", g_twopc_cur_srv, rops->txn_id);
//	struct rtxn_ops_entry *in_rtxn_op =
//			(struct rtxn_ops_entry *) malloc(sizeof (struct rtxn_ops_entry));
	ssize_t entry_size = sizeof(struct rtxn_ops_entry) + g_nremote_exec_ops * sizeof(struct hash_op);
	struct rtxn_ops_entry *in_rtxn_op = (struct rtxn_ops_entry *) malloc(entry_size);
	memcpy(&in_rtxn_op->remote_ops, rops, twopc_txn_ops_struct_size());
//	LIST_INSERT_HEAD(&rtxn_ops_list, in_rtxn_op, rtxn_ops_entries);
	TAILQ_INSERT_TAIL(&rtxn_ops_list, in_rtxn_op, rtxn_ops_entries);
//	printf("Created record with src srv %d txn id %ld ops count %d\n", in_rtxn_op->remote_ops.src_srv, in_rtxn_op->remote_ops.src_srv, in_rtxn_op->remote_ops.ops_cnt);
//	struct hash_op *hashop = (struct hash_op *) rops->hash_ops;
//	struct hash_op *hashop_stored = (struct hash_op *) in_rtxn_op->remote_ops.hash_ops;
//	for (int i = 0; i < rops->ops_cnt; i++) {
//
////		printf("Inserting key %ld\n", hashop[i].key);
//		assert((hashop[i].key % (g_nremote_exec_srv + 1)) == g_twopc_cur_srv);
//		assert(hashop[i].key == hashop_stored[i].key);
//
//	}
//
//	for (int i = 0; i < in_rtxn_op->remote_ops.ops_cnt; i++) {
//		struct hash_op *hashop = (struct hash_op *) in_rtxn_op->remote_ops.hash_ops;
//		printf("Inserted key %ld\n", hashop[i].key);
//	}
	rtxn_ops_count++;
	pthread_mutex_unlock(&rtxn_ops_list_mtx);
//	printf("Pushed ops of txn %ld\n", rops->txn_id);
}

int twopc_pop_remote_ops(struct twopc_txn_ops *rops) {
	int count = 0;

		pthread_mutex_lock(&rtxn_ops_list_mtx);
	if (rtxn_ops_count > 0) {

//		struct rtxn_ops_entry *in_rtxn_op = LIST_FIRST(&rtxn_ops_list);
		struct rtxn_ops_entry *in_rtxn_op = TAILQ_FIRST(&rtxn_ops_list);
		if (in_rtxn_op) {
//			struct hash_op *hashop_stored = (struct hash_op *) in_rtxn_op->remote_ops.hash_ops;
//			for (int i = 0; i < in_rtxn_op->remote_ops.ops_cnt; i++) {
//				printf("Popping key %ld\n", hashop_stored[i].key);
//			}
			memcpy(rops, &in_rtxn_op->remote_ops, twopc_txn_ops_struct_size());
			memcpy(rops->hash_ops, in_rtxn_op->remote_ops.hash_ops, g_nremote_exec_ops * sizeof(struct hash_op));
//			for (int i = 0; i < in_rtxn_op->remote_ops.ops_cnt; i++) {
//				printf("Popped key %ld\n", hashop_stored[i].key);
//			}
//			struct hash_op *hashop = (struct hash_op *) rops->hash_ops;
//			for (int i = 0; i < rops->ops_cnt; i++) {
//				printf("Now have key %ld\n", hashop[i].key);
//			}
//			LIST_REMOVE(in_rtxn_op, rtxn_ops_entries);
			TAILQ_REMOVE(&rtxn_ops_list, in_rtxn_op, rtxn_ops_entries);
//			for (int i = 0; i < rops->ops_cnt; i++) {
//				printf("After remove have key %ld\n", hashop[i].key);
//			}

			free(in_rtxn_op);
//			for (int i = 0; i < rops->ops_cnt; i++) {
//				printf("After free have key %ld\n", hashop[i].key);
//			}

			count++;
			rtxn_ops_count--;
	//		printf("Retrieved %d ops\n", rops->ops_cnt);
	//		for (int i = 0; i < rops->ops_cnt; i++) {
	//			struct hash_op *hashop = (struct hash_op *) rops->hash_ops;
	//			printf("Retrieving key %ld\n", hashop[i].key);
	//		}
		}


	}
	pthread_mutex_unlock(&rtxn_ops_list_mtx);
	return count;
}

void twopc_push_result(struct txn_result_data *result_data) {
	pthread_mutex_lock(&rtxn_results_list_mtx);
	struct rtxn_results_entry *in_result =
			(struct rtxn_results_entry *) malloc(sizeof (struct rtxn_results_entry));
	memcpy(&in_result->remote_result, result_data, sizeof(struct txn_result_data));
	LIST_INSERT_HEAD(&rtxn_results_list, in_result, rtxn_results_entries);
	pthread_mutex_unlock(&rtxn_results_list_mtx);
//	printf("Pushed result of txn %ld\n", result_data->txn_id);
}

int twopc_pop_result(struct txn_result_data *result_data) {
	int count = 0;
	pthread_mutex_lock(&rtxn_results_list_mtx);
	struct rtxn_results_entry *in_result = LIST_FIRST(&rtxn_results_list);
	if (in_result) {
		memcpy(result_data, &in_result->remote_result, sizeof(struct txn_result_data));
		LIST_REMOVE(in_result, rtxn_results_entries);
		free(in_result);
		count++;
	}
	pthread_mutex_unlock(&rtxn_results_list_mtx);

	return count;
}

void twopc_push_fin_result(struct txn_result_data *result_data) {
	pthread_mutex_lock(&rtxn_fin_results_list_mtx);
	struct rtxn_fin_results_entry *in_result =
			(struct rtxn_fin_results_entry *) malloc(sizeof (struct rtxn_fin_results_entry));
	memcpy(&in_result->fin_remote_result, result_data, sizeof(struct txn_result_data));
	LIST_INSERT_HEAD(&rtxn_fin_results_list, in_result, rtxn_fin_results_entries);
	pthread_mutex_unlock(&rtxn_fin_results_list_mtx);
//	printf("Pushed final result of txn %ld\n", result_data->txn_id);
}

int twopc_pop_fin_result(struct txn_result_data *result_data) {
	int count = 0;
	pthread_mutex_lock(&rtxn_fin_results_list_mtx);
	struct rtxn_fin_results_entry *in_result = LIST_FIRST(&rtxn_fin_results_list);
	if (in_result) {
//		printf("Found a final result\n");
		memcpy(result_data, &in_result->fin_remote_result, sizeof(struct txn_result_data));
		LIST_REMOVE(in_result, rtxn_fin_results_entries);
		free(in_result);
		count++;
	}
	pthread_mutex_unlock(&rtxn_fin_results_list_mtx);

	return count;
}

void twopc_ack_rcv(int remote_srv) {
	pthread_mutex_lock(&ack_mtx);
	twopc_connected_srvs[remote_srv]++;
	pthread_mutex_unlock(&ack_mtx);
}

int twopc_connected_srvs_count() {
	pthread_mutex_lock(&ack_mtx);
	int i, count = 0;
	for (i = 0; i < g_nremote_exec_srv + 1; i++) {
		count += twopc_connected_srvs[i];
	}
	pthread_mutex_unlock(&ack_mtx);
	return count;
}


void twopc_exit_rcv(int remote_srv) {
	twopc_finished_srvs[remote_srv] = 1;
}

int twopc_finished_srvs_count() {
	volatile int i, count = 0;
	for (i = 0; i < g_nremote_exec_srv + 1; i++) {
		count += twopc_finished_srvs[i];
	}

	return count;
}

int twopc_has_srv_exited(int remote_srv) {
	return twopc_finished_srvs[remote_srv];
}
