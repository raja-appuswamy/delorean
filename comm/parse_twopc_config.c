/*
 * parse_twopc_config.c
 *
 *  Created on: 18 Sep 2018
 *      Author: anadioti
 */

#include "../headers.h"
#include "twopc.h"

struct twopc_configuration *config;

void parse_configuration_file(const char *file, struct twopc_configuration *config) {
	int cnt = 0;
	FILE *fp;
	fp = fopen(file, "r");

	if (!fp) {
		printf("Error opening file\n");
		exit(-1);
	}

	while (fscanf(fp, "%d %s %d\n", &config[cnt].srv, config[cnt].ip_addr, &config[cnt].port) != EOF) {
		cnt++;
	}

	assert(cnt == g_nremote_exec_srv + 1);
}

void twopc_config_init() {
	const char *file = "twopc_servers.conf";
	config = (struct twopc_configuration *) calloc(g_nremote_exec_srv + 1, sizeof(struct twopc_configuration));
	parse_configuration_file(file, config);
}


void *start_daemon(void *args) {
	short port = *(short *) args;
	twopc_server_daemon(port);

	return NULL;
}

void *twopc_daemon_start(void *args) {
	int cur_srv = *(int *) args;


	// get the local server index in configuration file
	int i, cur_idx;
	for (i = 0; i < g_nremote_exec_srv + 1; i++) {
		if (config[i].srv == cur_srv) {
			cur_idx = i;
			break;
		}
	}

	// start the 2-phase commit server daemon
//	pthread_t srv_daemon;
//	pthread_create(&srv_daemon, NULL, start_daemon, (short *) &config[cur_idx].port);
	// now try to connect to the rest of the servers

//	pthread_join(srv_daemon, NULL);
	twopc_server_daemon(config[cur_idx].port);
	return NULL;
}

void twopc_clients_start(int cur_srv) {
	int i, cur_idx;
	for (i = 0; i < g_nremote_exec_srv + 1; i++) {
		if (config[i].srv == cur_srv) {
			cur_idx = i;
			break;
		}
	}
	for (i = 0; i < g_nremote_exec_srv + 1; i++) {
		if (i != cur_idx) {
			int sock1 = twopc_cli_connect(config[cur_idx].srv, config[i].srv, config[i].ip_addr, config[i].port);
			int sock2 = twopc_cli_connect(config[cur_idx].srv, config[i].srv, config[i].ip_addr, config[i].port);
			int sock3 = twopc_cli_connect(config[cur_idx].srv, config[i].srv, config[i].ip_addr, config[i].port);
			usleep(1000000);
			printf("Connections: %d %d %d\n", sock1, sock2, sock3);
			twopc_cli_ops_register(config[cur_idx].srv, config[i].srv, sock1);
			twopc_cli_res_register(config[cur_idx].srv, config[i].srv, sock2);
			twopc_cli_final_res_register(config[cur_idx].srv, config[i].srv, sock3);
		}
	}
}
