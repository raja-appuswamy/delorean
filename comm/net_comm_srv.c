#if 0
/*
 * net_comm_srv.c
 *
 *  Created on: 19 Aug 2018
 *      Author: anadioti
 */

#include "headers.h"
#include "net_comm.h"

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define SRV_BUF_SIZE 1024
#define SRV_BACKLOG 16
#define SRV_WORKER_THREADS 4

pthread_mutex_t client_mtx;
int running_clients = 0;

void *process_clients(void *args) {
	int accepted_sockfd = *((int *) args);
	struct comm_msg in;
	ssize_t bytes_recv = recv(accepted_sockfd, &in, sizeof(struct comm_msg), 0);
	if (bytes_recv != -1) {
		if (in.ops_cnt != -1) {
			remote_ops_q_insert(&in.txn_id, in.ops, &in.ops_cnt, &accepted_sockfd);
		} else {
			/* Do nothing for now; we assume that all ops
			* are sent together with the PREPARE msg */
		}
	}
	pthread_mutex_lock(&client_mtx);
	running_clients--;
	pthread_mutex_unlock(&client_mtx);

	return NULL;
}

int start_server(struct hash_table *hash_table) {
	remote_ops_q_init();
	pthread_t workers[SRV_WORKER_THREADS];
	pthread_mutex_init(&client_mtx, NULL);

    int server_sock, client_sock;
    struct sockaddr_in server_sockaddr, client_sockaddr;
    memset(&server_sockaddr, 0, sizeof(struct sockaddr_in));
    memset(&client_sockaddr, 0, sizeof(struct sockaddr_in));
    int opt = 1;
    int len = sizeof(server_sockaddr);


    // Creating socket file descriptor
    if ((server_sock = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        return -1;
    }

    server_sockaddr.sin_family = AF_INET;
    server_sockaddr.sin_port = COMM_PORT;
    server_sockaddr.sin_addr.s_addr = INADDR_ANY;

    if (bind(server_sock, (struct sockaddr *) &server_sockaddr, len) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_sock, MAX_SERVERS) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("Socket listening for connections\n");

    int pid;


    while(!hash_table->quitting) {
		if ((client_sock = accept(server_sock, (struct sockaddr *) &client_sockaddr,  &len))<0) {
			perror("accept");
			exit(EXIT_FAILURE);
		}

		printf("Accepted new connection\n");

		int r = pthread_create(&workers[running_clients], NULL, process_clients, (void *) &client_sock);
		assert(r == 0);
		pthread_mutex_lock(&client_mtx);
		running_clients++;
		pthread_mutex_unlock(&client_mtx);
	}

    close(server_sock);



	return 0;
}


#endif	//0
