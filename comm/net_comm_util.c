#if 0
/*
 * net_comm_util.c
 *
 *  Created on: 20 Aug 2018
 *      Author: anadioti
 */

#include "net_comm.h"
#include <sys/queue.h>
#include <pthread.h>
#include <stdint.h>

struct remote_txn_op {
	struct hash_op op;
	uint64_t txn_id;
	uint8_t result;
	struct remote_txn_op *nxt;
};

pthread_mutex_t rop_waitq_mtx, rop_execq_mtx;
struct remote_txn_op *rtxn_waitq;
struct remote_txn_op *rtxn_execq;

void remote_opq_init() {
	pthread_mutex_init(&rop_waitq_mtx, NULL);
	pthread_mutex_init(&rop_execq_mtx, NULL);
	rtxn_waitq = (struct remote_txn_op *) malloc(sizeof(struct remote_txn_op));
	rtxn_waitq->nxt = NULL;
	rtxn_execq = (struct remote_txn_op *) malloc(sizeof(struct remote_txn_op));
	rtxn_execq->nxt = NULL;
}

void remote_op_waitq_push(const struct hash_op *remote_op, const uint64_t *txn_id) {
	struct remote_txn_op *elem = (struct remote_txn_op *) malloc(sizeof(struct remote_txn_op));
	memcpy(&elem->op, remote_op, sizeof(struct hash_op));
	elem->txn_id = *txn_id;
	pthread_mutex_lock(&rop_waitq_mtx);
	elem->nxt = rtxn_waitq->nxt;
	rtxn_waitq->nxt = elem;
	pthread_mutex_unlock(&rop_waitq_mtx);
}

// take the first waiter and move it to the executing queue
int remote_op_exec(struct remote_txn_op *remote_op, uint8_t *txn_result) {
	pthread_mutex_lock(&rop_waitq_mtx);
	struct remote_txn_op *elem = rtxn_waitq->nxt;
	if (elem != NULL) {
		rtxn_waitq->nxt = elem->nxt;
		pthread_mutex_unlock(&rop_waitq_mtx);
		pthread_mutex_lock(&rop_execq_mtx);
		elem->nxt = rtxn_execq->nxt;
		rtxn_execq->nxt = elem;
		pthread_mutex_unlock(&rop_execq_mtx);
		return 1;
	} else {
		pthread_mutex_unlock(&rop_waitq_mtx);
		return 0;
	}
}



uint8_t remote_txn_commit(const uint64_t *txn_id) {
	uint8_t res = TXN_COMMIT;
	pthread_mutex_lock(&rop_execq_mtx);
	struct remote_txn_op *e = rtxn_execq->nxt;
	struct remote_txn_op *p = rtxn_execq;
	while (e != NULL) {
		if (e->txn_id == *txn_id) {
			if (res != e->result) {
				res = TXN_ABORT;
			}
			// now remove the element from the list
			p->nxt = e->nxt;
			free(e);
		}
		p = p->nxt;
		if (p != NULL) {
			e = p->nxt;
		}
	}
	pthread_mutex_unlock(&rop_execq_mtx);

	return res;
}
#endif	//0
