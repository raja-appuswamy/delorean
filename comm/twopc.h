/*
 * twopc.h
 *
 *  Created on: 17 Sep 2018
 *      Author: anadioti
 */

#ifndef COMM_TWOPC_H_
#define COMM_TWOPC_H_

#include "../headers.h"

#define MAX_REMOTE_IN_REQS_PER_SRV	1000
#define MAX_REMOTE_OUT_REQS_PER_SRV	100

enum twopc_proto_codes {
	TWOPC_REGISTER,
	TWOPC_REGISTER_OPS,
	TWOPC_REGISTER_RES,
	TWOPC_REGISTER_FINAL_RES,
	TWOPC_REGISTER_ACK,
	TWOPC_OPS,
	TWOPC_OPS_RESULT,
	TWOPC_FINAL_RESULT,
	TWOPC_EXIT
};

struct twopc_msg {
	enum twopc_proto_codes twopc_code;
	char body[];
};

struct twopc_configuration {
	int srv;
	char ip_addr[16];
	int port;
};

struct registration_data {
	int src;
	int cli_socket;
};

struct twopc_txn_ops {
	int src_srv;	// the 2pc node
	int src_issue_srv;	// the srv issuing txns
	int dst_exec_src;		// the srv executing txns
	uint64_t txn_id;
	int ops_cnt;	// -1: PREPARE for 2PC
	int ltxn_pos;
	char hash_ops[];
};

struct txn_result_data {
	int src_srv;	// the 2pc node
	int src_exec_srv;
	int dst_exec_srv;
	uint64_t txn_id;
	int result;
	int ltxn_pos;
	int rtxn_pos;
};

struct twopc_exit_data {
	int src;
};

struct rtxn_op {
	uint64_t txn_id;
	struct hash_op *ops;
	uint8_t ops_cnt;
	uint8_t res;
	int client_sock;
};

struct rtxn_context {
	struct txn_ctx ctx;
	volatile int remote_reqs;
	int result;
	int pos;
	int *remote_srvs;
};

extern volatile int *server_sock_map;

void twopc_config_init();
void *twopc_daemon_start(void *args);
void twopc_clients_start(int cur_srv);

// server functions definitions
int twopc_server_daemon(short port);

// client functions definitions
void twopc_cli_init();
int twopc_cli_connect(int cli, int srv, char *ip_addr, short port);
int twopc_cli_ops_register(int cli, int srv, int sockfd);
int twopc_cli_res_register(int cli, int srv, int sockfd);
int twopc_cli_final_res_register(int cli, int srv, int sockfd);
int twopc_cli_register(int cli, int srv, int sockfd);
void twopc_register_remote_client_ops(struct registration_data *reg_data);
void twopc_register_remote_client_res(struct registration_data *reg_data);
void twopc_register_remote_client_final_res(struct registration_data *reg_data);
void twopc_cli_insert_remote_op(int src, int trg, uint64_t txn_id, struct hash_op *hash_op);
void twopc_cli_upd_remote_op(int src, int trg, uint64_t txn_id, int remote_exec_srv);
int twopc_cli_flush_rops(int src, int ltxn_pos);
//int twopc_cli_send_txn_result(int src, int dst, uint64_t txn_id, int result);
int twopc_cli_send_txn_result(int dst, uint64_t txn_id, int result, int cur_exec_srv, int txn_issued_srv, int ltxn_pos, int rtxn_pos);
//int twopc_cli_send_final_result(int src, uint64_t txn_id, int result);
int twopc_cli_send_final_result(int src, uint64_t txn_id, int result, int *recipients, int rtxn_pos);
int twopc_cli_send_txn_exit();

// util functions definitions
void twopc_util_init();
void twopc_push_remote_ops(struct twopc_txn_ops *rops);
int twopc_pop_remote_ops(struct twopc_txn_ops *rops);
void twopc_push_result(struct txn_result_data *result_data);
int twopc_pop_result(struct txn_result_data *result_data);
void twopc_push_fin_result(struct txn_result_data *result_data);
int twopc_pop_fin_result(struct txn_result_data *result_data);
void twopc_ack_rcv(int remote_srv);
int twopc_connected_srvs_count();
ssize_t twopc_txn_ops_struct_size();
void twopc_exit_rcv(int remote_srv);
int twopc_finished_srvs_count();
int twopc_has_srv_exited(int remote_srv);

int twopc_store_locally_issued_txn_ctx(int local_srv, int total_remote_reqs, struct txn_ctx *cur_ctx, int *remote_srv);
void twopc_load_locally_issued_txn_ctx(int local_srv, uint64_t txn_id, struct txn_ctx *cur_ctx);
int twopc_store_remotely_issued_txn_ctx(int local_srv, struct txn_ctx *cur_ctx);
void twopc_load_remotely_issued_txn_ctx(int local_srv, uint64_t txn_id, struct txn_ctx *cur_ctx);
void twopc_upd_result_locally_issued_txn_ctx(struct txn_result_data *result_data);
void twopc_upd_result_remotely_issued_txn_ctx(struct txn_result_data *result_data);
int twopc_load_ltxn_results(int cur_worker_srv, int *result, struct txn_ctx **ctx, int **remote_srv, int pos, int *rtxn_pos);
void twopc_invalidate_ltxn(int cur_worker_srv, int pos);
int twopc_load_rtxn_final_results(int cur_worker_srv, int *result, struct txn_ctx **ctx, int pos);
void twopc_invalidate_rtxn(int cur_worker_srv, int pos);
int twopc_rtxn_have_space(int local_srv);
int twopc_ltxn_have_space(int local_srv);
int get_idx_success(int worker);
int get_idx_failure(int worker);

#endif /* COMM_TWOPC_H_ */
