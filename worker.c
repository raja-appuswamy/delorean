/*
 * worker.c
 *
 *  Created on: Nov 29, 2017
 *      Author: anadioti
 */

#include "headers.h"
#include "benchmark.h"
#include "worker.h"
#include "partition.h"
#include "smphashtable.h"

#if ENABLE_CPU_HOTPLUG
#include "hotplug/hotplug.h"
#endif

#if DIASSRV8

static int coreids[] = {
		1,2,3,4,5,6,7,8,9,10,
		11,12,13,14,15,16,17,18,19,20,
		21,22,23,24,25,26,27,28,29,30,
		31,32,33,34,35,36,37,38,39,40,
		0,81,82,83,84,85,86,87,88,89,
		90,91,92,93,94,95,96,97,98,99,
		100,101,102,103,104,105,106,107,108,109,
		110,111,112,113,114,115,116,117,118,119
};

#elif DIASCLD33
#if HT_ENABLED

static int coreids[] = {
		0,4,8,12,16,20,24,28,32,36,40,44,48,52,56,60,64,68,72,76,80,84,88,92,96,100,104,108,112,116,120,124,128,132,136,140,
		1,5,9,13,17,21,25,29,33,37,41,45,49,53,57,61,65,69,73,77,81,85,89,93,97,101,105,109,113,117,121,125,129,133,137,141,
		2,6,10,14,18,22,26,30,34,38,42,46,50,54,58,62,66,70,74,78,82,86,90,94,98,102,106,110,114,118,122,126,130,134,138,142,
		3,7,11,15,19,23,27,31,35,39,43,47,51,55,59,63,67,71,75,79,83,87,91,95,99,103,107,111,115,119,123,127,131,135,139,143,
};

#else

static int coreids[] = {
		0,4,8,12,16,20,24,28,32,36,40,44,48,52,56,60,64,68,
		1,5,9,13,17,21,25,29,33,37,41,45,49,53,57,61,65,69,
		2,6,10,14,18,22,26,30,34,38,42,46,50,54,58,62,66,70,
		3,7,11,15,19,23,27,31,35,39,43,47,51,55,59,63,67,71
};
#endif

#elif DIASCLD31
static int coreids[] = {
    0,2,4,6,8,10,12,14,16,18,20,22,
    1,3,5,7,9,11,13,15,17,19,21,23
};
#elif DIASCLD34
#if HT_ENABLED

static int coreids[] = {
    0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,
    1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47
};

#else

static int coreids[] = {
    0,2,4,6,8,10,12,14,16,18,20,22,
    1,3,5,7,9,11,13,15,17,19,21,23
};

#endif

#elif DIASCLD29
static int coreids[] = {
		0,2,4,6,8,10,12,14,
		1,3,5,7,9,11,13,15
};

#else
// for now, in default case, set it to 4 cores
static int coreids[] = {
    0,1,2,3
};

#endif

int *worker_id_socket_map;

void init_worker_id_socket_map() {
	worker_id_socket_map = (int *) calloc(g_nservers, sizeof(int));
	int i;
	for (i = 0; i < g_nservers; i++) {
		worker_id_socket_map[i] = -1;
	}
	worker_id_socket_map[0] = g_master_socket;
}




#if ENABLE_CPU_HOTPLUG

double measure_current_throughput(struct hash_table *hash_table)
{
//	int active_servers = 0;
	double tps = 0;
	for (int i = 0; i < g_nservers; i++) {
		struct partition *p = &hash_table->partitions[i];

//		if ((p->q_idx > p->prev_sum_tps) && (p->thread_termination_flag != 1)) {
		if (p->q_idx > p->prev_sum_tps) {

			if (p->prev_time_tps == 0) {
				p->prev_time_tps = p->txn_start_time;
			}
			double cur_time = now();
//			if (cur_time - p->prev_sum_tps > 800000) {
			double cur_idx = p->q_idx;
				tps += ((double) (cur_idx - p->prev_sum_tps)) / (cur_time - p->prev_time_tps);
				p->prev_sum_tps = cur_idx;
				p->prev_time_tps = cur_time;
//			}
		}
	}

	return tps / 1000000;
}


void load_data(struct hash_table *hash_table, int s, int id)
{
	struct partition *p = &hash_table->partitions[id];
	double tstart = now();

#if SHARED_EVERYTHING
	/* load only one partition in case of shared everything */
	g_benchmark->load_data(hash_table, id);
#else

#if ENABLE_ASYMMETRIC_MESSAGING
	/* load only few partitions in case of asym msg. */
	if (s < g_nhot_servers)
		g_benchmark->load_data(hash_table, id);
#else
	/* always load for sn/trireme */
	g_benchmark->load_data(hash_table, id);

#endif //ENABLE_ASYMMETRIC_MESSAGING

#endif //SHARED_EVERYTHING

	double tend = now();

	pthread_mutex_lock(&hash_table->create_client_lock);
	lready++;
	pthread_mutex_unlock(&hash_table->create_client_lock);
//	printf("srv %d load time %.3f\n", s, tend - tstart);
//	printf("srv %d rec count: %d partition sz %lu-KB "
//			"tx count: %d, per_txn_op cnt: %d\n", s, p->ninserts, p->size / 1024,
//			g_niters, g_ops_per_txn);
//	printf("lready = %d\n", lready);

}

int execute_txns(struct hash_table *hash_table, int s) {
	struct partition *p = &hash_table->partitions[s];
	void *query;
	p->q_idx = 0;

	pthread_mutex_lock(&hash_table->create_client_lock);

	nready++;
//	printf("nready is now %d\n", nready);

	pthread_mutex_unlock(&hash_table->create_client_lock);

//	while (nready != g_active_servers) ;
	while (nready != g_nservers_to_begin_txns) ;

	printf("srv %d starting txns\n", s);
	fflush(stdout);

	query = g_benchmark->alloc_query();
	assert(query);

	double tstart = now();
	p->txn_start_time = tstart;

#if ENABLE_ASYMMETRIC_MESSAGING

#if defined(SHARED_EVERYTHING) || defined(SHARED_NOTHING)
#error "Asymmetric messaging valid only in msgpassing mode\n"
#endif

	if (s >= g_nhot_servers)
		task_libinit(s);
#else
	task_libinit(s);
#endif

	double tend = now();
	p->txn_stop_time = tend;

//	printf("srv %d query time %.3f\n", s, tend - tstart);
//	printf("srv %d total txns %d \n", s, p->q_idx);
//	printf("srv %d commited txns %d aborted %d\n", s, p->ncommits, p->naborts);

	fflush(stdout);

#if ENABLE_ASYMMETRIC_MESSAGING
	if (s < g_nhot_servers)
		p->tps = 0;
	else
		p->tps = p->q_idx / (tend - tstart);
#else
	p->tps = p->q_idx / (tend - tstart);
#endif

	pthread_mutex_lock(&hash_table->create_client_lock);

	nready--;

	pthread_mutex_unlock(&hash_table->create_client_lock);

//	while(!p->thread_termination_flag);

	if (s < g_startup_servers) {
		while (nready != 0)
		#if !defined (SHARED_EVERYTHING) && !defined (SHARED_NOTHING)
				process_requests(hash_table, s);
		#else
			;
		#endif

			printf("srv %d quitting \n", s);
			fflush(stdout);

			if (g_benchmark->verify_txn)
				g_benchmark->verify_txn(hash_table, s);

			// destory out dses
		#if !defined (SHARED_EVERYTHING)
			/* in shared nothing and trireme case, we can safely delete
			 * the partition now as no one else will be directly accessing
			 * data from the partition. In shared everything case, some other
			 * thread might still be doing verification. So we don't destroy now.
			 */
			destroy_hash_partition(p);
		#endif
	}

	printf("srv %d done quitting \n", s);

	return 1;
}

void *start_worker_thread(void *args)
{
	int i, r;
	const int s = ((struct thread_args *) args)->id;
	const int c = ((struct thread_args *) args)->core;
	struct hash_table *hash_table = ((struct thread_args *) args)->hash_table;
	struct partition *p = &hash_table->partitions[s];
	void *query;
	__attribute__((unused)) int pct = 10;
	set_affinity(c);
#if defined(MIGRATION)
	fix_affinity(s);
#endif

	pthread_mutex_lock(&hash_table->create_client_lock);
	sready++;
	pthread_mutex_unlock(&hash_table->create_client_lock);
	int partition = -1;
	int loop_flag = 1;
	while (loop_flag) {
		for (int i = 0; (i < WORKER_ACTIONS_NUMBER) && (loop_flag); i++) {
			if (p->actions[i]) {
				switch(i) {
				case WORKER_ACTION_LOAD_DATA:
					p->actions[i] = 0;
					load_data(hash_table, s, p->signal_id);
					break;
				case WORKER_ACTION_START_TXNS:
					p->actions[i] = 0;
					r = execute_txns(hash_table, s);
					if (r == 1) {
						loop_flag = 0;
					}
					break;

				case WORKER_ACTION_STOP_TXNS:
					p->actions[i] = 0;
					loop_flag = 0;
					p->thread_termination_flag = 1;
					break;
				default:
					printf("Worker %d received msg %d\n", s, i);
				}
			}
		}
	}

	return NULL;
}

void stop_worker(int id, struct hash_table *hash_table)
{
	struct partition *p = &hash_table->partitions[id];
//	pthread_mutex_lock(&p->cv_mtx);
//	p->actions[WORKER_ACTION_STOP_TXNS] = 1;
//	pthread_mutex_unlock(&p->cv_mtx);

	g_active_servers--;
	void *value;
	int r = pthread_join(hash_table->threads[id], &value);
	assert(r == 0);
//	printf("Worker %d thread joined\n", id);

}

void notify_worker(int s, int id, int action, struct hash_table *hash_table)
{
	struct partition *p = &hash_table->partitions[s];
	p->signal_id = id;
	p->actions[action] = 1;
}


int hotplug_worker_to_socket(int worker_id, int socket_id, struct hash_table *hash_table, int *started_srv)
{
	int ret = 0;
//	int cpus_before = get_nprocs();
	uint64_t h = hotplug_cpu_add_socket(g_vm_id, socket_id);
//	printf("Hypervisor returned %d\n", h);
	if (h == 0) {
		ret = -1;
	} else if (h == 2) {
		ret = 1;
		g_active_servers++;
		g_max_started_servers++;
	} else {
		g_active_servers++;
		g_max_started_servers++;
	}
	ret = h - 1;

	int srv_to_start = -1;
	int i;
	for (i = 0; i < g_nservers; i++) {
		if (worker_id_socket_map[i] == -1) {
			srv_to_start = i;
			worker_id_socket_map[i] = socket_id;
			break;
		}
	}
	*started_srv = srv_to_start;
	struct partition *p = &hash_table->partitions[srv_to_start];
	hash_table->thread_data[srv_to_start].id = srv_to_start;
#if ENABLE_VIRTUALIZATION
	hash_table->thread_data[srv_to_start].core = srv_to_start;
#else
	hash_table->thread_data[srv_to_start].core = coreids[srv_to_start];
#endif
	hash_table->thread_data[srv_to_start].hash_table = hash_table;

	worker_id_socket_map[srv_to_start] = socket_id;

//	printf("Assinging core %d to srv %d\n", hash_table->thread_data[id].core, id);
	pthread_mutex_init(&p->cv_mtx, NULL);
	p->signal_id = 0;
	p->action_id = 0;
	p->actions = (int *) calloc(WORKER_ACTIONS_NUMBER, sizeof(int));
	p->thread_termination_flag = 0;

	if (ret >= 0) {
		int r = pthread_create(&hash_table->threads[srv_to_start], NULL, start_worker_thread, (void *) (&hash_table->thread_data[srv_to_start]));
		assert(r == 0);
	}

	return ret;
}

void stop_hotplugged_server_socket(int socket_id, struct hash_table *hash_table)
{
	int srv_to_stop = -1;
	int i;
	for (i = g_nservers - 1; i >= 0; i--) {
		if (worker_id_socket_map[i] == socket_id) {
			srv_to_stop = i;
			worker_id_socket_map[i] = -1;
			break;
		}
	}

	assert(srv_to_stop > -1);

	struct partition *p = &hash_table->partitions[srv_to_stop];
	pthread_mutex_lock(&p->cv_mtx);
	p->actions[WORKER_ACTION_STOP_TXNS] = 1;
	pthread_mutex_unlock(&p->cv_mtx);
	p->thread_termination_flag = 1;
	p->prev_sum_tps = 0;
	p->prev_time_tps = 0;
	g_active_servers--;
	void *value;
	int r = pthread_join(hash_table->threads[srv_to_stop], &value);
	assert(r == 0);

		// In this case, we are in the VM and we need to release the core back to the hypervisor
	int h;
	h = hotplug_cpu_del_socket(g_vm_id, socket_id);

}

void stop_all_hotplugged_servers(struct hash_table *hash_table)
{
	int socket_id;
	int srv_to_stop = -1;
	int i;
	for (i = g_nservers - 1; i >= g_startup_servers; i--) {
		socket_id = worker_id_socket_map[i];
		srv_to_stop = i;
		worker_id_socket_map[i] = -1;


		// In this case, we are in the VM and we need to release the core back to the hypervisor
		int h;
		h = hotplug_cpu_del_socket(g_vm_id, socket_id);

		assert(srv_to_stop > -1);
		struct partition *p = &hash_table->partitions[srv_to_stop];
		pthread_mutex_lock(&p->cv_mtx);
		p->actions[WORKER_ACTION_STOP_TXNS] = 1;
		pthread_mutex_unlock(&p->cv_mtx);
		p->thread_termination_flag = 1;
		g_active_servers--;
		void *value;
		int r = pthread_join(hash_table->threads[srv_to_stop], &value);
		assert(r == 0);
//		printf("Worker %d thread joined\n", srv_to_stop);
	}
}
#else

void load_data(struct hash_table *hash_table, int s)
{
	struct partition *p = &hash_table->partitions[s];
	double tstart = now();

#if SHARED_EVERYTHING
	/* load only one partition in case of shared everything */
	g_benchmark->load_data(hash_table, s);
#else

#if ENABLE_ASYMMETRIC_MESSAGING
	/* load only few partitions in case of asym msg. */
	if (s < g_nhot_servers)
		g_benchmark->load_data(hash_table, s);
#else
	/* always load for sn/trireme */
	g_benchmark->load_data(hash_table, s);

#endif //ENABLE_ASYMMETRIC_MESSAGING

#endif //SHARED_EVERYTHING

	double tend = now();

	printf("srv %d load time %.3f\n", s, tend - tstart);
	printf("srv %d rec count: %d partition sz %lu-KB "
			"tx count: %d, per_txn_op cnt: %d\n", s, p->ninserts, p->size / 1024,
			g_niters, g_ops_per_txn);

}

void execute_txns(struct hash_table *hash_table, int s)
{
	struct partition *p = &hash_table->partitions[s];
	void *query;

	pthread_mutex_lock(&hash_table->create_client_lock);

	nready++;

	pthread_mutex_unlock(&hash_table->create_client_lock);

	while (nready != g_active_servers) ;

	printf("srv %d starting txns\n", s);
	fflush(stdout);

	query = g_benchmark->alloc_query();
	assert(query);
	double tstart = now();

#if ENABLE_ASYMMETRIC_MESSAGING

#if defined(SHARED_EVERYTHING) || defined(SHARED_NOTHING)
#error "Asymmetric messaging valid only in msgpassing mode\n"
#endif

	if (s >= g_nhot_servers)
		task_libinit(s);
#else
	task_libinit(s);
#endif

	double tend = now();

	printf("srv %d query time %.3f\n", s, tend - tstart);
	printf("srv %d total txns %ld \n", s, p->q_idx);
	printf("srv %d commited txns %d aborted %d\n", s, p->ncommits, p->naborts);

	fflush(stdout);

#if ENABLE_ASYMMETRIC_MESSAGING
	if (s < g_nhot_servers)
		p->tps = 0;
	else
		p->tps = p->q_idx / (tend - tstart);
#elif ENABLE_2PC
	p->tps = p->ncommits / (tend - tstart);
#else
	p->tps = p->q_idx / (tend - tstart);
#endif

	pthread_mutex_lock(&hash_table->create_client_lock);

	nready--;

	pthread_mutex_unlock(&hash_table->create_client_lock);

	while (nready != 0)
#if !defined (SHARED_EVERYTHING) && !defined (SHARED_NOTHING)
		process_requests(hash_table, s);
#else
	;
#endif

	printf("srv %d quitting \n", s);
	fflush(stdout);

	if (g_benchmark->verify_txn)
		g_benchmark->verify_txn(hash_table, s);

	// destory out dses
#if !defined (SHARED_EVERYTHING)
	/* in shared nothing and trireme case, we can safely delete
	 * the partition now as no one else will be directly accessing
	 * data from the partition. In shared everything case, some other
	 * thread might still be doing verification. So we don't destroy now.
	 */
	destroy_hash_partition(p);
#endif
	printf("srv %d done quitting \n", s);
}

void *start_worker_thread(void *args)
{
	int i, r;
	const int s = ((struct thread_args *) args)->id;
	const int c = ((struct thread_args *) args)->core;
	struct hash_table *hash_table = ((struct thread_args *) args)->hash_table;
	struct partition *p = &hash_table->partitions[s];
	void *query;
	__attribute__((unused)) int pct = 10;

	set_affinity(c);
#if defined(MIGRATION)
	fix_affinity(s);
#endif

	printf("Worker %d waiting for signal\n", s);
	pthread_mutex_lock(&p->cv_mtx);
	while (p->signal_id == 0) {
		r = pthread_cond_wait(&p->cv, &p->cv_mtx);
	}
	p->actions[p->signal_id] = 1;
	pthread_mutex_unlock(&p->cv_mtx);
	printf("Worker %d received signal %d\n", s, p->signal_id);

	int loop_flag = 1;
	while (loop_flag) {
		for (int i = 0; i < WORKER_ACTIONS_NUMBER; i++) {
			pthread_mutex_lock(&p->cv_mtx);
			if (p->actions[i]) {
				switch(i) {
				case WORKER_ACTION_LOAD_DATA:
					printf("Worker %d loading data\n", s);
					p->actions[i] = 0;
					pthread_mutex_unlock(&p->cv_mtx);
					// load the data
					load_data(hash_table, s);
					break;
				case WORKER_ACTION_START_TXNS:
					printf("Worker %d executing txns\n", s);
					p->actions[i] = 0;
					pthread_mutex_unlock(&p->cv_mtx);
					execute_txns(hash_table, s);
					break;

				case WORKER_ACTION_STOP_TXNS:
					printf("Worker %d stopping\n", s);
					p->actions[i] = 0;
					pthread_mutex_unlock(&p->cv_mtx);
					loop_flag = 0;
					p->thread_termination_flag = 1;
					break;
				default:
					printf("Worker %d received msg %d\n", s, i);
				}
			} else {
				pthread_mutex_unlock(&p->cv_mtx);
			}
		}
	}

	return NULL;
}

void stop_worker(int id, struct hash_table *hash_table)
{
	struct partition *p = &hash_table->partitions[id];
	pthread_mutex_lock(&p->cv_mtx);
	p->actions[WORKER_ACTION_STOP_TXNS] = 1;
	pthread_mutex_unlock(&p->cv_mtx);
	void *value;
	int r = pthread_join(hash_table->threads[id], NULL);
	assert(r == 0);
}



void notify_worker(int id, int action, struct hash_table *hash_table)
{
	struct partition *p = &hash_table->partitions[id];
	pthread_mutex_lock(&p->cv_mtx);
	p->signal_id = action;
	p->actions[action] = 1;
	pthread_cond_signal(&p->cv);
	pthread_mutex_unlock(&p->cv_mtx);
	printf("Worker %d notified with signal %d\n", id, action);
}

#endif // ENABLE_CPU_HOTPLUG




void start_worker(int id, struct hash_table *hash_table)
{
	struct partition *p = &hash_table->partitions[id];
	hash_table->thread_data[id].id = id;
#if ENABLE_VIRTUALIZATION
	hash_table->thread_data[id].core = id;
#else
	hash_table->thread_data[id].core = coreids[id];
#if defined(ENABLE_2PC) && !defined(ENALBLE_VIRTUALIZATION)
	hash_table->thread_data[id].core = coreids[id] + g_twopc_cur_srv;
#endif // ENABLE_2PC
#endif
	hash_table->thread_data[id].hash_table = hash_table;

	printf("Assinging core %d to srv %d\n", hash_table->thread_data[id].core, id);
	pthread_mutex_init(&p->cv_mtx, NULL);
	p->signal_id = 0;
	p->action_id = 0;
	p->actions = (int *) calloc(WORKER_ACTIONS_NUMBER, sizeof(int));
	p->thread_termination_flag = 0;

	int r = pthread_create(&hash_table->threads[id], NULL, start_worker_thread, (void *) (&hash_table->thread_data[id]));
	assert(r == 0);
}

void start_worker_hotplug(int id, struct hash_table *hash_table)
{
	struct partition *p = &hash_table->partitions[id];
	hash_table->thread_data[id].id = id;
#if ENABLE_VIRTUALIZATION
	hash_table->thread_data[id].core = id;
#else
	hash_table->thread_data[id].core = coreids[id];
#endif
	hash_table->thread_data[id].hash_table = hash_table;

	printf("Assinging core %d to srv %d\n", hash_table->thread_data[id].core, id);
	pthread_mutex_init(&p->cv_mtx, NULL);
	p->signal_id = 0;
	p->action_id = 0;
	p->actions = (int *) calloc(WORKER_ACTIONS_NUMBER, sizeof(int));
	p->thread_termination_flag = 0;

	g_active_servers++;

	int r = pthread_create(&hash_table->threads[id], NULL, start_worker_thread, (void *) (&hash_table->thread_data[id]));
	assert(r == 0);
}





